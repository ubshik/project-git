package com.gmail.ubshik.repository.util;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public class HibernateUtil {
    private static final Logger logger = Logger.getLogger(HibernateUtil.class);
    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            return new Configuration().configure().buildSessionFactory();
        } catch (Throwable e) {
            logger.error("Initial SessionFactory creation failed." + e);
            throw new ExceptionInInitializerError(e);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static Session getOpenSession(){
        return getSessionFactory().openSession();
    }

    public static Session getCurrentSession(){
        return getSessionFactory().getCurrentSession();
    }

    public static void shutdown() {
        getSessionFactory().close();
    }
}
