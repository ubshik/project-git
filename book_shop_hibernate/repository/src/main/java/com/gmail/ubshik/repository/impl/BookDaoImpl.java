package com.gmail.ubshik.repository.impl;

import com.gmail.ubshik.repository.BookDao;
import com.gmail.ubshik.repository.model.Book;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public class BookDaoImpl extends GenericDaoImpl<Book, Integer> implements BookDao {
}
