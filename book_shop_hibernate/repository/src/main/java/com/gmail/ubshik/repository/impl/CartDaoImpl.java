package com.gmail.ubshik.repository.impl;

import com.gmail.ubshik.repository.CartDao;
import com.gmail.ubshik.repository.model.Book;
import com.gmail.ubshik.repository.model.Cart;
import com.gmail.ubshik.repository.model.User;
import com.gmail.ubshik.repository.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public class CartDaoImpl extends GenericDaoImpl<Cart, Integer> implements CartDao {
    private static final Logger logger = Logger.getLogger(CartDaoImpl.class);

    @Override
    public Cart getCartByUserAndBook(User user, Book book) {
        logger.info("Trying to get cart by user and book");
        Session getSession = HibernateUtil.getCurrentSession();
        Cart cart = null;
        Query query = getSession.createQuery(resourceBundle.getString("getCartRecordByUserAndBook"));
        query.setParameter("user", user);
        query.setParameter("book", book);
        cart = (Cart)query.uniqueResult();
        if(cart == null){
            return null;
        }
        return cart;
    }

    @Override
    public List<Cart> findListCartByUserId(User user) {
        logger.info("Trying to get list of cart by user id");
        Session getSession = HibernateUtil.getCurrentSession();
        List<Cart> listCart = new ArrayList<>();
        Query query = getSession.createQuery(resourceBundle.getString("getCartRecordByUserId"));
        query.setParameter("user", user);
        listCart = query.list();
        if(listCart == null || listCart.size() == 0){
            logger.warn("User " + user + " haven't record in cart.");
            return null;
        }
        return listCart;
    }
}
