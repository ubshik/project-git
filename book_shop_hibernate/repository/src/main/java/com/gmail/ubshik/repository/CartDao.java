package com.gmail.ubshik.repository;

import com.gmail.ubshik.repository.model.Book;
import com.gmail.ubshik.repository.model.Cart;
import com.gmail.ubshik.repository.model.User;

import java.util.List;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public interface CartDao extends GenericDao<Cart, Integer> {
    List<Cart> findListCartByUserId(User user);
    Cart getCartByUserAndBook(User user, Book book);
}
