package com.gmail.ubshik;

import com.gmail.ubshik.repository.model.Cart;
import com.gmail.ubshik.repository.model.User;
import com.gmail.ubshik.repository.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class AppRepo{

    public static void main( String[] args ){
        Session session = null;
        session = HibernateUtil.getCurrentSession();
        List<User> userList = new ArrayList<>();
        List<Cart> cartList = new ArrayList<>();
        session.beginTransaction();
        Query query =
                session.createQuery
                        ("SELECT o FROM Order o WHERE o.user = :user");
        userList = (List<User>)query.list();
        session.getTransaction().commit();
        System.out.println(userList.toString());
    }
}
