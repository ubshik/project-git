package com.gmail.ubshik.repository.impl;

import com.gmail.ubshik.repository.NewsDao;
import com.gmail.ubshik.repository.model.News;
import com.gmail.ubshik.repository.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public class NewsDaoImpl extends GenericDaoImpl<News, Integer> implements NewsDao {
    private static final Logger logger = Logger.getLogger(NewsDaoImpl.class);

    @Override
    public List<News> getListNews10Item() {
        logger.info("Trying to get 10 news.");
        List<News> listNews = new ArrayList<>();
        Session getSession = HibernateUtil.getCurrentSession();
        Query query = getSession.createQuery(resourceBundle.getString("getNews10Item"));
        query.setMaxResults(10);
        listNews = (List<News>) query.list();
        if(listNews == null || listNews.size() == 0){
            logger.warn("Don't find any news.");
        }
        return listNews;
    }
}
