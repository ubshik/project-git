package com.gmail.ubshik.repository.impl;

import com.gmail.ubshik.repository.GenericDao;
import com.gmail.ubshik.repository.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public abstract class GenericDaoImpl<T extends Serializable, ID extends Serializable> implements GenericDao<T, ID> {
    private static final Logger logger = Logger.getLogger(GenericDaoImpl.class);
    ResourceBundle resourceBundle = ResourceBundle.getBundle("hql");
    private final Class<T> entityClass;

    public GenericDaoImpl(){
        this.entityClass = (Class<T>) ((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public ID save(T entity) {
        Session getSession = HibernateUtil.getCurrentSession();
        return (ID) getSession.save(entity);
    }

    @Override
    public void saveOrUpdate(T entity) {
        Session getSession = HibernateUtil.getCurrentSession();
        getSession.saveOrUpdate(entity);
    }

    @Override
    public T findById(ID id) {
        Session getSession = HibernateUtil.getCurrentSession();
        return (T) getSession.get(this.entityClass, id);
    }

    @Override
    public List<T> findAll() {
        Session getSession = HibernateUtil.getCurrentSession();
        return getSession.createCriteria(this.entityClass).list();
    }

    @Override
    public void delete(T entity) {
        Session getSession = HibernateUtil.getCurrentSession();
        getSession.delete(entity);
    }

    @Override
    public void deleteAll() {
        Session getSession = HibernateUtil.getCurrentSession();
        List<T> entities = findAll();
        for (T entity : entities){
            getSession.delete(entity);
        }
    }
}
