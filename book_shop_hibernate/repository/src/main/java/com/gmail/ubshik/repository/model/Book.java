package com.gmail.ubshik.repository.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Acer5740 on 02.08.2017.
 */
@Entity
@Table(name = "T_BOOKS")
public class Book implements Serializable {
    private static final long serialVersionUID = -3341200147446642750L;

    @Id
    @Column(name = "F_BOOK_ID", nullable = false, unique = true, precision=5)
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Integer bookId;
    @Column(name = "F_TITLE", nullable = false, length = 50)
    private String title;
    @Column(name = "F_AUTHOR", nullable = false, length = 50)
    private String author;
    @Column(name = "F_INVENTORY_NUMBER", nullable = false, length = 8)
    private String inventoryNumber;
    @Column(name = "F_DESCRIPTION", nullable = false, columnDefinition="text")
    private String description;
    @Column(name = "F_PRICE", nullable = false, precision=5, scale=2)
    private Double price;
    @Column(name = "F_DATE_CREATION", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dateCreation;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.book")
    private Set<Cart> carts = new HashSet<>();

    private Book(Builder builder){
        bookId = builder.bookId;
        title = builder.title;
        author = builder.author;
        inventoryNumber = builder.inventoryNumber;
        description = builder.description;
        price = builder.price;
        dateCreation = builder.dateCreation;
        carts = builder.carts;
    }

    public Book() {
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getInventoryNumber() {
        return inventoryNumber;
    }

    public void setInventoryNumber(String inventoryNumber) {
        this.inventoryNumber = inventoryNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Set<Cart> getCarts() {
        return carts;
    }

    public void setCarts(Set<Cart> carts) {
        this.carts = carts;
    }

    public static final class Builder{
        private Integer bookId;
        private String title;
        private String author;
        private String inventoryNumber;
        private String description;
        private Double price;
        private Date dateCreation;
        private Set<Cart> carts  = new HashSet<>();

        private Builder(){}

        public Builder bookId(Integer val){
            bookId = val;
            return this;
        }

        public Builder title(String val){
            title = val;
            return this;
        }

        public Builder author(String val){
            author = val;
            return this;
        }

        public Builder inventoryNumber(String val){
            inventoryNumber = val;
            return this;
        }

        public Builder description(String val){
            description = val;
            return this;
        }

        public Builder price(Double val){
            price = val;
            return this;
        }

        public Builder dateCreation(Date val){
            dateCreation = val;
            return this;
        }

        public Builder carts(Set<Cart> val){
            carts = val;
            return this;
        }

        public Book build(){
            return new Book(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(title, book.title) &&
                Objects.equals(author, book.author) &&
                Objects.equals(price, book.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, price);
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookId=" + bookId +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", inventoryNumber='" + inventoryNumber + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", dateCreation=" + dateCreation +
                ", carts=" + carts +
                '}';
    }

    public String toStringShort() {
        return "Book{" +
                "bookId=" + bookId +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", inventoryNumber='" + inventoryNumber + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", dateCreation=" + dateCreation +
                '}';
    }
}
