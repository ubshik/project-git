package com.gmail.ubshik.repository.model.enums;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public enum Role {
    SUPERADMIN,
    ADMIN,
    USER
}
