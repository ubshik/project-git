package com.gmail.ubshik.repository;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public interface GenericDao<T extends Serializable, ID extends Serializable> {
    ID save (T entity);
    void saveOrUpdate(T entity);
    T findById(ID id);
    List<T> findAll();
    void delete(T entity);
    void deleteAll();
}
