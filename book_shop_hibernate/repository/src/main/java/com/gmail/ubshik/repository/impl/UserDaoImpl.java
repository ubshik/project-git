package com.gmail.ubshik.repository.impl;

import com.gmail.ubshik.repository.UserDao;
import com.gmail.ubshik.repository.model.User;
import com.gmail.ubshik.repository.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public class UserDaoImpl extends GenericDaoImpl<User, Integer> implements UserDao {
    private static final Logger logger = Logger.getLogger(UserDaoImpl.class);

    @Override
    public User getUserByEmail(String email) {
        logger.info("Trying to get user by email");
        Session getSession = HibernateUtil.getCurrentSession();
        User user = null;
        Query query = getSession.createQuery(resourceBundle.getString("getUserByEmail"));
        query.setParameter("email", email);
        user = (User) query.uniqueResult();
        if(user == null){
            logger.warn("Don't find user with email " + email);
        }
        return user;
    }

    @Override
    public User getUserByEmailAndPassword(String email, String password) {
        logger.info("Trying to get user by email and password");
        Session getSession = HibernateUtil.getCurrentSession();
        User user = null;
        Query query = getSession.createQuery(resourceBundle.getString("getUserByEmailAndPassword"));
        query.setParameter("email", email);
        query.setParameter("password", password);
        user = (User) query.uniqueResult();
        if(user == null){
            logger.warn("Don't find user with email " + email + " and password " + password);
        }
        return user;
    }
}
