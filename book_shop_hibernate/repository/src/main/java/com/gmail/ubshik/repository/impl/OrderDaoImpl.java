package com.gmail.ubshik.repository.impl;

import com.gmail.ubshik.repository.OrderDao;
import com.gmail.ubshik.repository.model.Cart;
import com.gmail.ubshik.repository.model.Order;
import com.gmail.ubshik.repository.model.User;
import com.gmail.ubshik.repository.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public class OrderDaoImpl extends GenericDaoImpl<Order, Integer> implements OrderDao {
    private static final Logger logger = Logger.getLogger(OrderDaoImpl.class);

    @Override
    public List<Order> findListOrderByUserId(User user) {
        logger.info("Trying to get list of cart by user id");
        Session getSession = HibernateUtil.getCurrentSession();
        List<Order> listOrder = new ArrayList<>();
        Query query = getSession.createQuery(resourceBundle.getString("getOrderByUserId"));
        query.setParameter("user", user);
        listOrder = query.list();
        if(listOrder == null || listOrder.size() == 0){
            logger.warn("User " + user + " haven't order in cart.");
            return null;
        }
        return listOrder;
    }
}
