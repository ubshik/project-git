package com.gmail.ubshik.repository;

import com.gmail.ubshik.repository.model.Book;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public interface BookDao extends GenericDao<Book, Integer> {
}
