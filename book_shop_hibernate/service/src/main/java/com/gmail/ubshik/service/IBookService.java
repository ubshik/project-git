package com.gmail.ubshik.service;

import com.gmail.ubshik.service.model.BookDTO;

import java.util.List;

/**
 * Created by Acer5740 on 12.08.2017.
 */
public interface IBookService {
    public Integer createBookDTO(BookDTO bookDTO);
    public List<BookDTO> getAllBook();
    public BookDTO getBookById(Integer id);
}
