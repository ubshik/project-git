package com.gmail.ubshik.service.impl;

import com.gmail.ubshik.repository.UserDao;
import com.gmail.ubshik.repository.impl.UserDaoImpl;
import com.gmail.ubshik.repository.model.User;
import com.gmail.ubshik.repository.model.enums.Role;
import com.gmail.ubshik.repository.model.enums.UserStatus;
import com.gmail.ubshik.repository.util.HibernateUtil;
import com.gmail.ubshik.service.IUserService;
import com.gmail.ubshik.service.model.UserDTO;
import com.gmail.ubshik.service.model.util.Converter;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.Date;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public class UserServiceImpl implements IUserService {
    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);
    private static UserDao userDao = new UserDaoImpl();
    private static UserServiceImpl instance = null;

    public static UserServiceImpl getInstance(){
        if(instance == null){
            instance = new UserServiceImpl();
        }
        return instance;
    }

    public UserDTO getUserDTOByEmailAndPassword(String email, String password) {
        logger.info("Trying to get user by email and password.");
        Session getSession = HibernateUtil.getCurrentSession();
        User user = null;
        UserDTO userDTO = null;
        try{
            getSession.beginTransaction();
            user = userDao.getUserByEmailAndPassword(email, password);
            if(user!=null){
                userDTO = new UserDTO(user);
            }
            getSession.getTransaction().commit();
        }catch (HibernateException e){
            logger.error("User with email " + email + " and password " + password + " don't found. " + e);
            getSession.getTransaction().rollback();
        }
        if (user == null){
            return null;
        }
        return userDTO;
    }

    public UserDTO getUserDTOByEmail(String email) {
        logger.info("Trying to get user by email.");
        Session getSession = null;
        User user = null;
        UserDTO userDTO = null;
        getSession = HibernateUtil.getCurrentSession();
        try{
            getSession.beginTransaction();
            user = userDao.getUserByEmail(email);
            if(user!=null){
                userDTO = new UserDTO(user);
            }
            getSession.getTransaction().commit();
        }catch (HibernateException e){
            logger.error("User with email " + email + " don't found. " + e);
            getSession.getTransaction().rollback();
        }
        if (user == null) {
            return null;
        }
        return userDTO;
    }

    public Integer createUserDTO(UserDTO userDTO){
        logger.info("Trying to create user from UserServiceImpl.");
        Session getSession = null;
        User user = null;
        Date date = new Date();
        Integer count;
        getSession = HibernateUtil.getCurrentSession();
        try {
            getSession.beginTransaction();
            userDTO.setDateRegistration(date);
            userDTO.setRole(Role.USER);
            userDTO.setUserStatus(UserStatus.ACTIVE);
            user = Converter.convertToUser(userDTO);
            count = userDao.save(user);
            logger.info("Count saving user " + count);
            getSession.getTransaction().commit();
        } catch (HibernateException e) {
            logger.error("It is not possible to save user " + userDTO.getFirstName()+ " " + userDTO.getLastName() + ". " + e);
            getSession.getTransaction().rollback();
            return null;
            }
        return count;
    }



}
