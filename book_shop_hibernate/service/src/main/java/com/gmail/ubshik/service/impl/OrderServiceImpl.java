package com.gmail.ubshik.service.impl;

import com.gmail.ubshik.repository.CartDao;
import com.gmail.ubshik.repository.OrderDao;
import com.gmail.ubshik.repository.UserDao;
import com.gmail.ubshik.repository.impl.CartDaoImpl;
import com.gmail.ubshik.repository.impl.OrderDaoImpl;
import com.gmail.ubshik.repository.impl.UserDaoImpl;
import com.gmail.ubshik.repository.model.Cart;
import com.gmail.ubshik.repository.model.Order;
import com.gmail.ubshik.repository.model.User;
import com.gmail.ubshik.repository.model.enums.OrderStatus;
import com.gmail.ubshik.repository.util.HibernateUtil;
import com.gmail.ubshik.service.IOrderService;
import com.gmail.ubshik.service.model.CartDTO;
import com.gmail.ubshik.service.model.OrderDTO;
import com.gmail.ubshik.service.model.UserDTO;
import com.gmail.ubshik.service.model.util.Converter;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Acer5740 on 15.08.2017.
 */
public class OrderServiceImpl implements IOrderService {
    private static final Logger logger = Logger.getLogger(OrderServiceImpl.class);
    private static OrderDao orderDao = new OrderDaoImpl();
    private static UserDao userDao = new UserDaoImpl();
    private static CartDao cartDao = new CartDaoImpl();
    private static OrderServiceImpl instance = null;

    public static OrderServiceImpl getInstance(){
        if(instance == null){
            instance = new OrderServiceImpl();
        }
        return instance;
    }

    @Override
    public List<OrderDTO> getAllOrderByUserId(UserDTO userDTO) {
        logger.info("Trying to get all orders by user id from OrderService.");
        List<OrderDTO> listOrderDTO = new ArrayList<>();
        List<Order> listOrder = null;
        Session getSession = null;
        getSession = HibernateUtil.getCurrentSession();
        try {
            getSession.beginTransaction();
            listOrder = orderDao.findListOrderByUserId(Converter.convertToUser(userDTO));
            if(listOrder != null){
                for (Order order : listOrder){
                    OrderDTO orderDTO = new OrderDTO(order);
                    listOrderDTO.add(orderDTO);
                }
            }
            getSession.getTransaction().commit();
        } catch (HibernateException e) {
            logger.error("It is not possible to get orders by user. " + e);
            getSession.getTransaction().rollback();
            return null;
        }
        if(listOrder == null){
            return null;
        }
        return listOrderDTO;
    }

    @Override
    public Integer createOrder(OrderDTO orderDTO, List<CartDTO> cartDTOList) {
        logger.info("Trying to create new order from OrderServiceImpl.");
        Session getSession = null;
        Order order = null;
        User user = null;
        Set<Cart> cartSet = new HashSet<>();
        Integer count = null;
        getSession = HibernateUtil.getCurrentSession();
        try {
            getSession.beginTransaction();
            orderDTO.setOrderStatus(OrderStatus.NEW);
            order = Converter.convertToOrder(orderDTO);
            user = order.getUser();
            for (CartDTO cartDTO : cartDTOList){
                Cart cart = Converter.convertToCart(cartDTO);
                cart.setOrder(order);
                cartSet.add(cart);
            }
            order.setCarts(cartSet);
            user.getOrders().add(order);
            count = orderDao.save(order);
            logger.info("Saving order");
            User user2 = userDao.findById(orderDTO.getUserId());//todo
            Order order2 = orderDao.findById(count);//todo
            getSession.getTransaction().commit();
        } catch (HibernateException e) {
                Writer writer = new StringWriter();
            e.printStackTrace(new PrintWriter(writer));
                    String s = writer.toString();
                    logger.warn(s);
            logger.error("It is not possible to save order where user with email: " + user.getEmail()  + ". " + e);
            getSession.getTransaction().rollback();
        }
        return count;
    }
}
