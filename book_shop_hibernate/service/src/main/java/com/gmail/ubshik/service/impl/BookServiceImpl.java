package com.gmail.ubshik.service.impl;

import com.gmail.ubshik.repository.BookDao;
import com.gmail.ubshik.repository.impl.BookDaoImpl;
import com.gmail.ubshik.repository.model.Book;
import com.gmail.ubshik.repository.util.HibernateUtil;
import com.gmail.ubshik.service.IBookService;
import com.gmail.ubshik.service.model.BookDTO;
import com.gmail.ubshik.service.model.util.Converter;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Acer5740 on 12.08.2017.
 */
public class BookServiceImpl implements IBookService {
    private static final Logger logger = Logger.getLogger(BookServiceImpl.class);
    private static BookDao bookDao = new BookDaoImpl();
    private static BookServiceImpl instance = null;

    public static BookServiceImpl getInstance(){
        if(instance == null){
            instance = new BookServiceImpl();
        }
        return instance;
    }

    @Override
    public List<BookDTO> getAllBook() {
        logger.info("Trying to get all book from BookService.");
        List<BookDTO> listBookDTO = new ArrayList<>();
        List<Book> listBook = null;
        Session getSession = null;
        getSession = HibernateUtil.getCurrentSession();
        try {
            getSession.beginTransaction();
            listBook = bookDao.findAll();
            if(listBook != null){
                for (Book book : listBook){
                    BookDTO bookDTO = new BookDTO(book);
                    listBookDTO.add(bookDTO);
                }
            }
            getSession.getTransaction().commit();
        } catch (HibernateException e) {
            Writer writer = new StringWriter();
            e.printStackTrace(new PrintWriter(writer));
            String s = writer.toString();
            logger.warn(s);
            logger.error("It is not possible to get all book. " + e);
            getSession.getTransaction().rollback();
            return null;
        }
        if(listBook == null){
            return null;
        }
        return listBookDTO;
    }

    @Override
    public Integer createBookDTO(BookDTO bookDTO) {
        logger.info("Trying to create book from BookService.");
        Session getSession = null;
        Book book = null;
        Date date = new Date();
        Integer count;
        getSession = HibernateUtil.getCurrentSession();
        try {
            getSession.beginTransaction();
            bookDTO.setDateCreation(date);
            book = Converter.convertToBook(bookDTO);
            count = bookDao.save(book);
            logger.info("Count saving book " + count);
            getSession.getTransaction().commit();
        } catch (HibernateException e) {
            logger.error("It is not possible to save book " + bookDTO.getTitle()+ " " + bookDTO.getAuthor() + ". " + e);
            getSession.getTransaction().rollback();
            return null;
        }
        return count;
    }

    @Override
    public BookDTO getBookById(Integer id) {
        logger.info("Trying to get book by ID from BookService.");
        Session getSession = null;
        Book book = null;
        BookDTO bookDTO = null;
        getSession = HibernateUtil.getCurrentSession();
        try {
            getSession.beginTransaction();
            book = bookDao.findById(id);
            if(book != null){
                bookDTO = new BookDTO(book);
            }
            getSession.getTransaction().commit();
        } catch (HibernateException e) {
            logger.error("It is not possible to get book by id " + id + ". " + e);
            getSession.getTransaction().rollback();
            return null;
        }
        if(book == null){
            return null;
        }
        return bookDTO;
    }
}
