package com.gmail.ubshik.service.model;

import com.gmail.ubshik.repository.model.Book;
import com.gmail.ubshik.repository.model.Cart;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public class BookDTO {
    private Integer bookId;
    private String title;
    private String author;
    private String inventoryNumber;
    private String description;
    private Double price;
    private Date dateCreation;

    public BookDTO(Book book){
        this(
                newBuilder()
                        .bookId(book.getBookId())
                        .title(book.getTitle())
                        .author(book.getAuthor())
                        .inventoryNumber(book.getInventoryNumber())
                        .description(book.getDescription())
                        .price(book.getPrice())
                        .dateCreation(book.getDateCreation())
        );
    }

    private BookDTO(Builder builder){
        setBookId(builder.bookId);
        setTitle(builder.title);
        setAuthor(builder.author);
        setInventoryNumber(builder.inventoryNumber);
        setDescription(builder.description);
        setPrice(builder.price);
        setDateCreation(builder.dateCreation);
    }

    public static Builder newBuilder(){
        return new Builder();
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getInventoryNumber() {
        return inventoryNumber;
    }

    public void setInventoryNumber(String inventoryNumber) {
        this.inventoryNumber = inventoryNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public static final class Builder{
        private Integer bookId;
        private String title;
        private String author;
        private String inventoryNumber;
        private String description;
        private Double price;
        private Date dateCreation;

        private Builder(){}

        public Builder bookId(Integer val){
            bookId = val;
            return this;
        }

        public Builder title(String val){
            title = val;
            return this;
        }

        public Builder author(String val){
            author = val;
            return this;
        }

        public Builder inventoryNumber(String val){
            inventoryNumber = val;
            return this;
        }

        public Builder description(String val){
            description = val;
            return this;
        }

        public Builder price(Double val){
            price = val;
            return this;
        }

        public Builder dateCreation(Date val){
            dateCreation = val;
            return this;
        }

        public BookDTO build(){
            return new BookDTO(this);
        }
    }
}
