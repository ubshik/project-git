package com.gmail.ubshik.service.impl;

import com.gmail.ubshik.repository.NewsDao;
import com.gmail.ubshik.repository.impl.NewsDaoImpl;
import com.gmail.ubshik.repository.model.News;
import com.gmail.ubshik.repository.model.User;
import com.gmail.ubshik.repository.util.HibernateUtil;
import com.gmail.ubshik.service.INewsService;
import com.gmail.ubshik.service.model.NewsDTO;
import com.gmail.ubshik.service.model.util.Converter;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Acer5740 on 16.08.2017.
 */
public class NewsServiceImpl implements INewsService {
    private static final Logger logger = Logger.getLogger(NewsServiceImpl.class);
    private static NewsDao newsDao = new NewsDaoImpl();
    private static NewsServiceImpl instance = null;

    public static NewsServiceImpl getInstance(){
        if(instance == null){
            instance = new NewsServiceImpl();
        }
        return instance;
    }

    @Override
    public List<NewsDTO> get10News() {
        logger.info("Trying to get all news from NewsService.");
        List<NewsDTO> listNewsDTO = new ArrayList<>();
        List<News> listNews = null;
        Session getSession = null;
        getSession = HibernateUtil.getCurrentSession();
        try {
            getSession.beginTransaction();
            listNews = newsDao.getListNews10Item();
            if(listNews != null){
                for (News news : listNews){
                    NewsDTO newsDTO = new NewsDTO(news);
                    listNewsDTO.add(newsDTO);
                }
            }
            getSession.getTransaction().commit();
        } catch (HibernateException e) {
            logger.error("It is not possible to get all news. " + e);
            getSession.getTransaction().rollback();
            return null;
        }
        if(listNews == null){
            return null;
        }
        return listNewsDTO;
    }

    @Override
    public Integer createNews(NewsDTO newsDTO) {
        logger.info("Trying to create book from BookService.");
        Session getSession = null;
        User user = null;
        News news = null;
        Date date = new Date();
        Integer count;
        getSession = HibernateUtil.getCurrentSession();
        try {
            getSession.beginTransaction();
            newsDTO.setDatePublication(date);
            news = Converter.convertToNews(newsDTO);
            user = news.getUser();
            user.getNewsSet().add(news);
            count = newsDao.save(news);
            logger.info("Count saving news " + count);
            getSession.getTransaction().commit();
        } catch (HibernateException e) {
            logger.error("It is not possible to save news " + newsDTO.getTopic() + ". " + e);
            getSession.getTransaction().rollback();
            return null;
        }
        return count;
    }
}
