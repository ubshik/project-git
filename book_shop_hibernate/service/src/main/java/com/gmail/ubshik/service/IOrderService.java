package com.gmail.ubshik.service;

import com.gmail.ubshik.repository.model.Order;
import com.gmail.ubshik.service.model.CartDTO;
import com.gmail.ubshik.service.model.OrderDTO;
import com.gmail.ubshik.service.model.UserDTO;

import java.util.List;

/**
 * Created by Acer5740 on 15.08.2017.
 */
public interface IOrderService {
    public List<OrderDTO> getAllOrderByUserId(UserDTO userDTO);
    public Integer createOrder(OrderDTO orderDTO, List<CartDTO> cartDTOList);
}
