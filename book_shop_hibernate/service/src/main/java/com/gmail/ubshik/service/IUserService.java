package com.gmail.ubshik.service;

import com.gmail.ubshik.service.model.UserDTO;

/**
 * Created by Acer5740 on 08.08.2017.
 */
public interface IUserService {
    public UserDTO getUserDTOByEmailAndPassword(String email, String password);
    public UserDTO getUserDTOByEmail(String email);
    public Integer createUserDTO(UserDTO userDTO);
}
