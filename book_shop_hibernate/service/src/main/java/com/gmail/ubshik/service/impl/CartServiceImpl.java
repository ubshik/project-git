package com.gmail.ubshik.service.impl;

import com.gmail.ubshik.repository.BookDao;
import com.gmail.ubshik.repository.CartDao;
import com.gmail.ubshik.repository.UserDao;
import com.gmail.ubshik.repository.impl.BookDaoImpl;
import com.gmail.ubshik.repository.impl.CartDaoImpl;
import com.gmail.ubshik.repository.impl.UserDaoImpl;
import com.gmail.ubshik.repository.model.Book;
import com.gmail.ubshik.repository.model.Cart;
import com.gmail.ubshik.repository.model.CartId;
import com.gmail.ubshik.repository.model.User;
import com.gmail.ubshik.repository.util.HibernateUtil;
import com.gmail.ubshik.service.ICartService;
import com.gmail.ubshik.service.model.BookDTO;
import com.gmail.ubshik.service.model.CartDTO;
import com.gmail.ubshik.service.model.UserDTO;
import com.gmail.ubshik.service.model.util.Converter;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Acer5740 on 12.08.2017.
 */
public class CartServiceImpl implements ICartService {
    private static final Logger logger = Logger.getLogger(CartServiceImpl.class);
    private static CartDao cartDao = new CartDaoImpl();
    private static CartServiceImpl instance = null;

    public static CartServiceImpl getInstance(){
        if(instance == null){
            instance = new CartServiceImpl();
        }
        return instance;
    }

    @Override
    public void createCartRecord(CartDTO cartDTO, Double price) {
        logger.info("Trying to create new record in cart from CartServiceImpl.");
        Session getSession = null;
        User user = null;
        Book book = null;
        Cart cart = null;
        CartId cartId = null;
        Integer count;
        getSession = HibernateUtil.getCurrentSession();
        try {
            getSession.beginTransaction();
            Integer quantity = 1;
            cartDTO.setPrice(price);
            cartDTO.setQuantity(quantity);
            cartDTO.setSum(price * quantity);
            cart = Converter.convertToCart(cartDTO);
            user = cart.getUser();
            user.getCarts().add(cart);
            book = cart.getBook();
            book.getCarts().add(cart);
            cartDao.save(cart);
            logger.info("Saving record in cart ");
            getSession.getTransaction().commit();
        } catch (HibernateException e) {
            logger.error("It is not possible to save record in cart. User with email: " + user.getEmail() + " and book with id: " + book.getBookId() + ". " + e);
            getSession.getTransaction().rollback();
        }
    }

    @Override
    public CartDTO getCardDTOByUserAndBook(UserDTO userDTO, BookDTO bookDTO) {
        logger.info("Trying to get cartDTO by user and book.");
        Session getSession = HibernateUtil.getCurrentSession();
        Cart cart = null;
        CartDTO cartDTO = null;
        try{
            getSession.beginTransaction();
            User user = Converter.convertToUser(userDTO);
            Book book = Converter.convertToBook(bookDTO);
            cart = cartDao.getCartByUserAndBook(user, book);
            if(cart != null){
                cartDTO = new CartDTO(cart);
            }
            getSession.getTransaction().commit();
        }catch (HibernateException e){
            logger.error("Cart with user " + userDTO.getEmail() + " and book " + bookDTO.getBookId() + " don't found. " + e);
            getSession.getTransaction().rollback();
        }
        if (cartDTO == null){
            return null;
        }
        return cartDTO;
    }

    @Override
    public List<CartDTO> getAllCartRecordByUserId(UserDTO userDTO) {
        logger.info("Trying to get all record by user id from CartService.");
        List<CartDTO> listCartDTO = new ArrayList<>();
        List<Cart> listCart = null;
        Session getSession = null;
        getSession = HibernateUtil.getCurrentSession();
        try {
            getSession.beginTransaction();
            listCart = cartDao.findListCartByUserId(Converter.convertToUser(userDTO));
            if(listCart != null){
                for (Cart cart : listCart){
                    CartDTO cartDTO = new CartDTO(cart);
                    listCartDTO.add(cartDTO);
                }
            }
            getSession.getTransaction().commit();
        } catch (HibernateException e) {
            logger.error("It is not possible to get records from cart by user. " + e);
            getSession.getTransaction().rollback();
            return null;
        }
        if(listCart == null){
            return null;
        }
        return listCartDTO;
    }

    @Override
    public CartDTO getCartDTOById(Integer id) {
        logger.info("Trying to get cartDTO by cartId.");
        Session getSession = HibernateUtil.getCurrentSession();
        Cart cart = null;
        CartDTO cartDTO = null;
        try{
            getSession.beginTransaction();
            cart = cartDao.findById(id);
            if(cart != null){
                cartDTO = new CartDTO(cart);
            }
            getSession.getTransaction().commit();
        }catch (HibernateException e){
            logger.error("Cart with id " + id + " don't found. " + e);
            getSession.getTransaction().rollback();
        }
        if (cartDTO == null){
            return null;
        }
        return cartDTO;
    }
}



//    Writer writer = new StringWriter();
//            e.printStackTrace(new PrintWriter(writer));
//                    String s = writer.toString();
//                    logger.warn(s);
