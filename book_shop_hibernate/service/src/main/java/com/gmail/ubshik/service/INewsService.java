package com.gmail.ubshik.service;

import com.gmail.ubshik.service.model.NewsDTO;

import java.util.List;

/**
 * Created by Acer5740 on 16.08.2017.
 */
public interface INewsService {
    public List<NewsDTO> get10News();
    public Integer createNews(NewsDTO newsDTO);
}
