package com.gmail.ubshik.service;

import com.gmail.ubshik.service.model.BookDTO;
import com.gmail.ubshik.service.model.CartDTO;
import com.gmail.ubshik.service.model.UserDTO;

import java.util.List;

/**
 * Created by Acer5740 on 12.08.2017.
 */
public interface ICartService {
    public void createCartRecord(CartDTO cartDTO, Double price);
    public List<CartDTO> getAllCartRecordByUserId(UserDTO userDTO);
    public CartDTO getCardDTOByUserAndBook(UserDTO userDTO, BookDTO bookDTO);
    public CartDTO getCartDTOById(Integer id);

}
