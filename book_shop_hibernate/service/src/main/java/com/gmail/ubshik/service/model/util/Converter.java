package com.gmail.ubshik.service.model.util;

import com.gmail.ubshik.repository.BookDao;
import com.gmail.ubshik.repository.OrderDao;
import com.gmail.ubshik.repository.UserDao;
import com.gmail.ubshik.repository.impl.BookDaoImpl;
import com.gmail.ubshik.repository.impl.OrderDaoImpl;
import com.gmail.ubshik.repository.impl.UserDaoImpl;
import com.gmail.ubshik.repository.model.*;
import com.gmail.ubshik.service.model.*;

/**
 * Created by Acer5740 on 08.08.2017.
 */
public class Converter {
    private static UserDao userDao = new UserDaoImpl();
    private static BookDao bookDao = new BookDaoImpl();
    private static OrderDao orderDao = new OrderDaoImpl();

    public static User convertToUser(UserDTO userDTO){
        User user = new User();
        user.setUserId(userDTO.getUserId());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setPhone(userDTO.getPhone());
        user.setAddress(userDTO.getAddress());
        user.setAddInfo(userDTO.getAddInfo());
        user.setDateRegistration(userDTO.getDateRegistration());
        user.setRole(userDTO.getRole());
        user.setUserStatus(userDTO.getUserStatus());
        return user;
    }

    public static Book convertToBook(BookDTO bookDTO){
        Book book = new Book();
        book.setBookId(bookDTO.getBookId());
        book.setTitle(bookDTO.getTitle());
        book.setAuthor(bookDTO.getAuthor());
        book.setInventoryNumber(bookDTO.getInventoryNumber());
        book.setDescription(bookDTO.getDescription());
        book.setPrice(bookDTO.getPrice());
        book.setDateCreation(bookDTO.getDateCreation());
        return book;
    }

    public static Order convertToOrder(OrderDTO orderDTO){
        if(orderDTO != null){
            Order order = new Order();
            order.setOrderId(orderDTO.getOrderId());
            order.setUser(userDao.findById(orderDTO.getUserId()));
            order.setTotalPrice(orderDTO.getTotalPrice());
            order.setOrderStatus(orderDTO.getOrderStatus());
            return order;
        }else {
            return null;
        }
    };

    public static News convertToNews(NewsDTO newsDTO){
        News news = new News();
        news.setNewsId(newsDTO.getNewsId());
        news.setUser(userDao.findById(newsDTO.getUserId()));
        news.setTopic(newsDTO.getTopic());
        news.setContent(newsDTO.getContent());
        news.setPathPhoto(newsDTO.getPathPhoto());
        news.setDatePublication(newsDTO.getDatePublication());
        return news;
    }

    private static CartId convertToCartId(CartDTO cartDTO){
        CartId cartId = new CartId();
        cartId.setUser(userDao.findById(cartDTO.getUserId()));
        cartId.setBook(bookDao.findById(cartDTO.getBookId()));
        return cartId;
    }

    public static Cart convertToCart(CartDTO cartDTO){
        Cart cart = new Cart();
        cart.setCartId(cartDTO.getCartId());
        cart.setPk(convertToCartId(cartDTO));
        cart.setPrice(cartDTO.getPrice());
        cart.setQuantity(cartDTO.getQuantity());
        cart.setSum(cartDTO.getSum());
        cart.setOrder(cartDTO.getOrderId() != null ? orderDao.findById(cartDTO.getOrderId()) : null);
        return cart;
    }
}
