<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <meta charset="utf-8">
    <title>Cart list</title>
</head>
<body>
<h2>YOUR CART</h2>
<h1>${message}</h1>
<table>
    <form name="cartList" method="post" action="${pageContext.request.contextPath}/user/orders/add">
    <tr>
        <th>Serial number</th>
        <th>Title</th>
        <th>Author</th>
        <th>Price</th>
        <th>Quantity</th>
        <th>Sum</th>
        <th>Add to order</th>
    </tr>
    <c:forEach var="carts" items="${listCartDTO}" varStatus="loop">
        <tr>
            <td>${loop.index + 1}</td>
            <td><c:out value="${carts.title}"/></td>
            <td><c:out value="${carts.author}"/></td>
            <td><c:out value="${carts.price}"/></td>
            <td><c:out value="${carts.quantity}"/></td>
            <td><c:out value="${carts.sum}"/></td>
            <td><input type="checkbox"  name="id" value="${carts.cartId}"></td>
                <%--<td><a href="${pageContext.request.contextPath}/user/cars/order?id=${books.bookId}">Order</a></td>--%>
        </tr>
    </c:forEach><br>
        <p><input type="submit" value="Order" ></p>
    </form>
</table>
<button><a href ="${pageContext.request.contextPath}/logout">Log out</a></button>
</body>
</html>