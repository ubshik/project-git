<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <meta charset="utf-8">
    <title>Book list</title>
</head>
<body>
<h2>LIST OF BOOKS</h2>
<h1>${message}</h1>
<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Author</th>
        <th>Description</th>
        <th>Price</th>
        <th>Add to cart</th>
    </tr>
    <c:forEach var="books" items="${listBookDTO}">
        <tr>
            <td><c:out value="${books.bookId}"/></td>
            <td><c:out value="${books.title}"/></td>
            <td><c:out value="${books.author}"/></td>
            <td><c:out value="${books.description}"/></td>
            <td><c:out value="${books.price}"/></td>
            <td><a href="${pageContext.request.contextPath}/user/cart/add?id=${books.bookId}"> Add</a></td>
            <%--<td><a href="${pageContext.request.contextPath}/user/cars/order?id=${books.bookId}">Order</a></td>--%>
        </tr>
    </c:forEach>
</table>
<button><a href ="${pageContext.request.contextPath}/logout">Log out</a></button>
</body>
</html>