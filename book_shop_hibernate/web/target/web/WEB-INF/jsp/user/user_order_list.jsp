<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <meta charset="utf-8">
    <title>Order list</title>
</head>
<body>
<h2>YOUR ORDERS</h2>
<table>
    <tr>
        <th>Order number</th>
        <th>Total price</th>
        <th>Status</th>
        <th>Details</th>


    </tr>
    <c:forEach var="orders" items="${listOrderDTO}">
        <tr>
            <td><c:out value="${orders.orderId}"/></td>
            <td><c:out value="${orders.totalPrice}"/></td>
            <td><c:out value="${orders.orderStatus}"/></td>
            <td><a href="${pageContext.request.contextPath}/user/order/detail?id=${orders.orderId}"> Look details</a></td>
                <%--<td><a href="${pageContext.request.contextPath}/user/cars/order?id=${books.bookId}">Order</a></td>--%>
        </tr>
    </c:forEach>
</table>
<button><a href ="${pageContext.request.contextPath}/logout">Log out</a></button>
</body>
</html>