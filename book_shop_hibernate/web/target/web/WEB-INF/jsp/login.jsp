<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <meta charset="utf-8">
    <title>Book shop</title>
</head>
<body>
<h2>Let's log in or register to work with site</h2>
<h1>${error}</h1>
<form name="LoginForm" method="post" action="${pageContext.request.contextPath}/login">
    <b>Enter your email:</b>
    <input name="email" type="text" size="50" required><br>
    <b>Enter your password :</b>
    <input name="password" type="password" size="15" required>
    <p><input type="submit" value="Enter"></p><br>
</form>
<a href ="${pageContext.request.contextPath}/registration">Registration</a>
</body>
</html>