<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <meta charset="utf-8">
    <title>Error</title>
</head>
<body>
    <h1>ERROR</h1>
    <h2>${error}</h2><br/><br/>
    <button><a href ="${pageContext.request.contextPath}/logout">Log out</a></button>
</body>
</html>