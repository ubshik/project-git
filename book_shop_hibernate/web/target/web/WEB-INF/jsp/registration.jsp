<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<h2>FORM TO REGISTRATION</h2>
<h1>${error}</h1>
<form name="registration" method="post" action="${pageContext.request.contextPath}/registration">
    <b>Enter your First name* : </b>
    <input name="firstname" type="text" size="50" value="${firstname}" required><br><br>
    <b>Enter your Last name* : </b>
    <input name="lastname" type="text" size="50" value="${lastname}" required><br><br>
    <b>Enter your email* : </b>
    <input name="email" type="text" size="50" value="${email}" required><br><br>
    <b>Enter your password (no more 15 symbols)* : </b>
    <input name="password" type="password" size="15" required><br><br>
    <b>Repeat your password* : </b>
    <input name="password_repeat" type="password" size="15" required><br><br>
    <b>Enter your phone (example: 80251234567)* : </b>
    <input name="phone" type="text" size="11" value="${phone}" required><br><br>
    <b>Enter your address* : </b>
    <input name="address" type="text" size="100" value="${address}" required><br><br>
    <b>Enter your additional information : </b>
    <input name="add_info" type="text" size="50" value="${addInfo}"><br><br>
    <input type="submit" value="Complete registration">
</form>
</body>
<a href ="${pageContext.request.contextPath}/login">Return to main page</a>
</html>