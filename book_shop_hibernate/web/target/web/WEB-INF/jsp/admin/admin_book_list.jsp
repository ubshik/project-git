<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <meta charset="utf-8">
    <title>Book list</title>
</head>
<body>
<h2>LIST OF BOOKS</h2>
<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Author</th>
        <th>Inventory number</th>
        <th>Description</th>
        <th>Price</th>
        <th>Date of creation</th>
        <th>Update</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="books" items="${listBookDTO}">
        <tr>
            <td><c:out value="${books.bookId}"/></td>
            <td><c:out value="${books.title}"/></td>
            <td><c:out value="${books.author}"/></td>
            <td><c:out value="${books.inventoryNumber}"/></td>
            <td><c:out value="${books.description}"/></td>
            <td><c:out value="${books.price}"/></td>
            <td><c:out value="${books.dateCreation}"/></td>
            <td><a href="${pageContext.request.contextPath}/admin/books/update?id=${books.bookId}">Update</a></td>
            <td><a href="${pageContext.request.contextPath}/admin/books/delete?id=${books.bookId}">Delete</a></td>
        </tr>
    </c:forEach>
</table>
<a href ="${pageContext.request.contextPath}/admin/books/add">Add new book</a><br><br>
<button><a href ="${pageContext.request.contextPath}/logout">Log out</a></button>
</body>
</html>