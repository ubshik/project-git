<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Add new book</title>
</head>
<body>
<h2>FORM TO ADD NEW BOOK</h2>
<h1>${error}</h1>
<form name="addBook" method="post" action="${pageContext.request.contextPath}/admin/books/add">
    <b>Enter Title* : </b>
    <input name="title" type="text" size="50" value="${title}" required><br><br>
    <b>Enter your Author* : </b>
    <input name="author" type="text" size="50" value="${author}" required><br><br>
    <b>Enter your Inventory number* : </b>
    <input name="inventoryNumber" type="text" size="8" value="${inventoryNumber}" required><br><br>
    <b>Enter your Description (no more 15 symbols)* : </b>
    <input name="description" type="text" required><br><br>
    <b>Enter your Price* : </b>
    <input name="price" type="text" value="${price}" required><br><br>
    <input type="submit" value="Complete add new book">
</form>
</body>
<a href ="${pageContext.request.contextPath}/admin/books">Return to list of books</a>
</html>