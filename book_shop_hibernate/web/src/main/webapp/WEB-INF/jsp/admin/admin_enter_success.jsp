<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <meta charset="utf-8">
    <title>Congratulation</title>
</head>
<body>
<h1>Hello, ${userDTO.firstName}!</h1>
<h2>Welcome to site BOOK SHOP!</h2>
<h2>You are admin!</h2>
<img src="${req.contextPath}/images/book_logo.jpg" alt="photo book shop"><br><br>
<button><a href ="${pageContext.request.contextPath}/admin/books">List of books</a></button><br>
<button><a href ="${pageContext.request.contextPath}/admin/news">List of news</a></button><br>
<%--<%@include file="/jsp/user_include.jsp"%>--%>
<button><a href ="${pageContext.request.contextPath}/logout">Log out</a></button>
</body>
</html>