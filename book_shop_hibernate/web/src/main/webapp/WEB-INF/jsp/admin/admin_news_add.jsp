<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Add new book</title>
</head>
<body>
<h2>FORM TO ADD NEW BOOK</h2>
<h1>${error}</h1>
<form name="addBook" enctype="multipart/form-data" method="post" action="${pageContext.request.contextPath}/admin/news/add">
    <b>Enter Topic* : </b>
    <input name="topic" type="text" size="50" value="${topic}" required><br><br>
    <b>Enter your Content* : </b>
    <input name="content" type="text" value="${content}" required><br><br>
    <b>Enter your Photo* : </b>
    <input name="photo" type="file"><br><br>
    <input type="submit" value="Complete add news.">
</form>
</body>
<a href ="${pageContext.request.contextPath}/admin/news">Return to list of news</a>
</html>