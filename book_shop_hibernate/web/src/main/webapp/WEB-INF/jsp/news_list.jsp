<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <meta charset="utf-8">
    <title>News list</title>
</head>
<body>
<h2>LIST OF NEWS</h2>
<h1>${message}</h1>
<table>
    <tr>
        <th>Topic</th>
        <th>Content</th>
        <th>Image</th>
        <th>Date</th>
    </tr>
    <c:forEach var="news" items="${listNewsDTO}">
        <tr>
            <td><c:out value="${news.topic}"/></td>
            <td><c:out value="${news.content}"/></td>
            <td><img src="${pageContext.request.contextPath}/images/${news.pathPhoto}"/></td>
            <td><c:out value="${news.datePublication}"/></td>
        </tr>
    </c:forEach>
    <c:set var="admin" value="ADMIN"/>
    <c:set var="superadmin" value="SUPERADMIN"/>
    <c:if test="${userDTO.role eq admin || userDTO.role eq superadmin}">
        <a href ="${pageContext.request.contextPath}/admin/news/add">Add new news</a><br><br>
    </c:if>
</table>
<button><a href ="${pageContext.request.contextPath}/logout">Log out</a></button>
</body>
</html>