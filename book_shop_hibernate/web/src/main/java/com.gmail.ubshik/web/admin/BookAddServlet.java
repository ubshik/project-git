package com.gmail.ubshik.web.admin;

import com.gmail.ubshik.service.IBookService;
import com.gmail.ubshik.service.impl.BookServiceImpl;
import com.gmail.ubshik.service.model.BookDTO;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Created by Acer5740 on 12.08.2017.
 */
public class BookAddServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(BookAddServlet.class);
    private final IBookService bookService = BookServiceImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/jsp/admin/admin_book_add.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Trying to add new book from servlet BookAddServlet");
        String title = req.getParameter("title");
        String author = req.getParameter("author");
        String inventoryNumber = req.getParameter("inventoryNumber");
        String description = req.getParameter("description");
        Double price = Double.parseDouble(req.getParameter("price"));
        BookDTO bookDTO = null;
        Integer count;
        req.getSession().setAttribute("title", title);
        req.getSession().setAttribute("author", author);
        req.getSession().setAttribute("inventoryNumber", inventoryNumber);
        req.getSession().setAttribute("description", description);
        req.getSession().setAttribute("price", price);
        if(!title.equals("") && !author.equals("")
                && !inventoryNumber.equals("") && !description.equals("")
                && !price.equals("")){
            try {
                bookDTO = BookDTO.newBuilder().title(title).author(author)
                        .inventoryNumber(inventoryNumber).description(description)
                        .price(price).build();
                count = bookService.createBookDTO(bookDTO);
                logger.info("Create book with id = " + count);
                if (bookDTO != null) {
                    resp.sendRedirect(req.getContextPath() + "/admin/books");
                } else {
                    req.getSession().setAttribute("error", "Site has a problem. Decision will be soon.");
                    resp.sendRedirect(req.getContextPath() + "/error");
                }
            }catch (Exception e) {
                logger.error("Mistake" + e.getMessage());
                throw new IllegalStateException("Unknown error");
            }
        }else{
            logger.warn("Your fields are free.");
            req.getSession().setAttribute("error", "Your fields are free. Please fill in the form.");
            resp.sendRedirect(req.getContextPath() + "/addBook");
        }
    }
}
