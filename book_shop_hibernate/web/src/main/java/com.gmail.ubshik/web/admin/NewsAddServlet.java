package com.gmail.ubshik.web.admin;

import com.gmail.ubshik.service.INewsService;
import com.gmail.ubshik.service.impl.NewsServiceImpl;
import com.gmail.ubshik.service.model.NewsDTO;
import com.gmail.ubshik.service.model.UserDTO;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Acer5740 on 12.08.2017.
 */
@WebServlet("/admin/news/add")
@MultipartConfig
public class NewsAddServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(NewsAddServlet.class);
    private final INewsService newsService = NewsServiceImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/jsp/admin/admin_news_add.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Trying to add new book from servlet NewsAddServlet");
        UserDTO userDTO = (UserDTO) req.getSession().getAttribute("userDTO");
        String topic = req.getParameter("topic");
        String content = req.getParameter("content");
        Part partPhoto = req.getPart("photo");
//        String part = partPhoto.getHeader("content-disposition");//todo
        DateFormat df = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
        Date today = Calendar.getInstance().getTime();
        String reportDate = df.format(today);
        String fileName= reportDate + "_" + String.valueOf(userDTO.getUserId()) + ".jpg";
        InputStream fileData = partPhoto.getInputStream();
        File pathToSave = new File("F:\\Java\\ПВТ 2 course\\My project with git\\book_shop_hibernate\\web\\src\\main\\webapp\\images");
//        File pathToSave = new File(getServletContext().getRealPath("/") + "/images/");
//        File file = new File(pathToSave, fileName);
        File file = new File(pathToSave, fileName);
        try{
            Files.copy(fileData, file.toPath());
        }catch (Exception e){
            logger.warn("Files don't get. " + e);
        }
        NewsDTO newsDTO = null;
        Integer count;
        req.getSession().setAttribute("topic", topic);
        req.getSession().setAttribute("content", content);
        if(!topic.equals("") && !content.equals("")){
            try {
                newsDTO = NewsDTO.newBuilder().userId(userDTO.getUserId())
                        .topic(topic).content(content).pathPhoto(fileName)
                        .build();
                count = newsService.createNews(newsDTO);
                logger.info("Create book with id = " + count);
                if (newsDTO != null) {
                    resp.sendRedirect(req.getContextPath() + "/admin/news");
                } else {
                    req.getSession().setAttribute("error", "Site has a problem. Decision will be soon.");
                    resp.sendRedirect(req.getContextPath() + "/error");
                }
            }catch (Exception e) {
                logger.error("Mistake" + e.getMessage());
                throw new IllegalStateException("Unknown error");
            }
        }else{
            logger.warn("Your fields are free.");
            req.getSession().setAttribute("error", "Your fields are free. Please fill in the form.");
            resp.sendRedirect(req.getContextPath() + "/addBook");
        }
    }
}
