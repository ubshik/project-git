package com.gmail.ubshik.web;

import com.gmail.ubshik.service.IUserService;
import com.gmail.ubshik.service.impl.UserServiceImpl;
import com.gmail.ubshik.service.model.UserDTO;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public class RegistrationServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(RegistrationServlet.class);
    private final IUserService userService = UserServiceImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/jsp/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Trying to register from servlet RegisterServlet");
        String firstname = req.getParameter("firstname");
        String lastname = req.getParameter("lastname");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String passwordRepeat = req.getParameter("password_repeat");
        String phone = req.getParameter("phone");
        String address = req.getParameter("address");
        String addInfo = req.getParameter("add_info");
        UserDTO userDTO = null;
        Integer count;
        req.getSession().setAttribute("firstname", firstname);
        req.getSession().setAttribute("lastname", lastname);
        req.getSession().setAttribute("email", email);
        req.getSession().setAttribute("phone", phone);
        req.getSession().setAttribute("address", address);
        req.getSession().setAttribute("addInfo", addInfo);
        if(!firstname.equals("") && !lastname.equals("")
                && !email.equals("") && !password.equals("")
                && !phone.equals("") && !address.equals("")){
            if(!password.equals(passwordRepeat)){
                logger.warn("Your fields of password are different.");
                req.getSession().setAttribute("error", "Your fields of password are different. Please fill them the same.");
                resp.sendRedirect(req.getContextPath() + "/registration");
            } else{
                try {
                    userDTO = userService.getUserDTOByEmail(email);
                } catch (HibernateException e) {
                    logger.warn("Something wrong with get user by email from RegistrationServlet");
                    e.printStackTrace();
                }
                if (userDTO != null){
                    logger.warn("User with your email already exist.");
                    req.getSession().setAttribute("error", "User with your email already exist. Please fill the form again.");
                    resp.sendRedirect(req.getContextPath() + "/registration");
                }else{
                    try{
                        userDTO = UserDTO.newBuilder().firstName(firstname).lastName(lastname)
                                .email(email).password(password).phone(phone).address(address)
                                .addInfo(addInfo).build();
                        count = userService.createUserDTO(userDTO);
                        logger.info("Create user with id = " + count);
                        if(userDTO != null){
                            req.getSession().setAttribute("userDTO", userDTO);
                            resp.sendRedirect(req.getContextPath() + "/user");
                        }else {
                            req.getSession().setAttribute("error", "Site has a problem. Decision will be soon.");
                            resp.sendRedirect(req.getContextPath() + "/error");
                        }
                    }catch (Exception e) {
                        logger.error("Mistake" + e.getMessage());
                        throw new IllegalStateException("Unknown error");
                    }
                }
            }
        }else{
            logger.warn("Your fields are free.");
            req.getSession().setAttribute("error", "Your fields are free. Please fill in the form.");
            resp.sendRedirect(req.getContextPath() + "/registration");
        }
    }
}
