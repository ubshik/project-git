package com.gmail.ubshik.web.common;

import com.gmail.ubshik.service.IBookService;
import com.gmail.ubshik.service.impl.BookServiceImpl;
import com.gmail.ubshik.service.model.BookDTO;
import com.gmail.ubshik.service.model.UserDTO;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Acer5740 on 12.08.2017.
 */
public class BookList extends HttpServlet {
    private static final Logger logger = Logger.getLogger(BookList.class);
    private final IBookService bookService = BookServiceImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Trying to get list of book from servlet BookList");
        List<BookDTO> listBook = null;
        UserDTO userDTO =(UserDTO) req.getSession().getAttribute("userDTO");
        try {
            listBook = bookService.getAllBook();
            req.setAttribute("listBookDTO", listBook);
            if(userDTO.getRole().name().equals("USER")){
                req.getRequestDispatcher("/WEB-INF/jsp/user/user_book_list.jsp").forward(req, resp);
            }else {
                req.getRequestDispatcher("/WEB-INF/jsp/admin/admin_book_list.jsp").forward(req, resp);
            }
        } catch (Exception e) {
            logger.warn("Mistake of trying to get list of book");
            req.setAttribute("error", e.getMessage());
            req.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, resp);
        }
    }
}
