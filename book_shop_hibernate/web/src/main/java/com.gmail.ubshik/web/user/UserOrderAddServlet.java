package com.gmail.ubshik.web.user;

import com.gmail.ubshik.service.ICartService;
import com.gmail.ubshik.service.IOrderService;
import com.gmail.ubshik.service.impl.CartServiceImpl;
import com.gmail.ubshik.service.impl.OrderServiceImpl;
import com.gmail.ubshik.service.model.CartDTO;
import com.gmail.ubshik.service.model.OrderDTO;
import com.gmail.ubshik.service.model.UserDTO;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acer5740 on 16.08.2017.
 */
public class UserOrderAddServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(UserOrderAddServlet.class);
    private final ICartService cartService = CartServiceImpl.getInstance();
    private final IOrderService orderService = OrderServiceImpl.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Trying to add order from servlet UserOrderAdd");
        List<CartDTO> listCartDTO = new ArrayList<>();
        UserDTO userDTO =(UserDTO) req.getSession().getAttribute("userDTO");
        Integer userId = userDTO.getUserId();
        Double totalPrice = 0D;
        OrderDTO orderDTO = null;
        for (String id : req.getParameterValues("id")){
            Integer cartId = Integer.parseInt(id);
            CartDTO cartDTO = cartService.getCartDTOById(cartId);
            totalPrice+=cartDTO.getSum();
            listCartDTO.add(cartDTO);
        }
        try{
            orderDTO = OrderDTO.newBuilder()
                    .userId(userId).totalPrice(totalPrice)
                    .build();
            orderService.createOrder(orderDTO, listCartDTO);
            logger.info("Order was created successful");
            resp.sendRedirect(req.getContextPath() + "/user/orders");
        }catch (HibernateException e){
            logger.error("Mistake " + e.getMessage());
            throw new IllegalStateException("Unknown error");
        }
    }
}
