package com.gmail.ubshik.web.user;

import com.gmail.ubshik.repository.impl.CartDaoImpl;
import com.gmail.ubshik.repository.impl.UserDaoImpl;
import com.gmail.ubshik.repository.model.Cart;
import com.gmail.ubshik.repository.model.User;
import com.gmail.ubshik.service.IBookService;
import com.gmail.ubshik.service.ICartService;
import com.gmail.ubshik.service.impl.BookServiceImpl;
import com.gmail.ubshik.service.impl.CartServiceImpl;
import com.gmail.ubshik.service.model.BookDTO;
import com.gmail.ubshik.service.model.CartDTO;
import com.gmail.ubshik.service.model.UserDTO;
import com.gmail.ubshik.service.model.util.Converter;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acer5740 on 12.08.2017.
 */
public class UserCartAddServlet extends HttpServlet{
    private static final Logger logger = Logger.getLogger(UserCartAddServlet.class);
    private final IBookService bookService = BookServiceImpl.getInstance();
    private final ICartService cartService = CartServiceImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().removeAttribute("error");
        req.getSession().removeAttribute("message");
        logger.info("Trying to add new record to cart from servlet UserCartAddServlet");
        UserDTO userDTO = (UserDTO) req.getSession().getAttribute("userDTO");
        Integer id = Integer.valueOf(req.getParameter("id"));
        BookDTO bookDTO = bookService.getBookById(id);
        CartDTO cartDTO = null;
        CartDTO cartDTOFromDB = null;
        if(userDTO != null && bookDTO != null){
            try{
                cartDTOFromDB = cartService.getCardDTOByUserAndBook(userDTO, bookDTO);
            }catch (HibernateException e){
                logger.error("Mistake" + e.getMessage());
                throw new IllegalStateException("Unknown error");
            }
            if(cartDTOFromDB == null){
                try {
                    cartDTO = CartDTO.newBuilder()
                            .userId(userDTO.getUserId())
                            .bookId(bookDTO.getBookId())
                            .build();
                    cartService.createCartRecord(cartDTO, bookDTO.getPrice());
                    logger.info("Create record in cart successful");
                    String message = "Book " + bookDTO.getTitle() + " author " + bookDTO.getAuthor() + " was added to your cart successful.";
                    req.getSession().setAttribute("message", message);
                    resp.sendRedirect(req.getContextPath() + "/user/books");
                }catch (Exception e) {
                    logger.error("Mistake " + e.getMessage());
                    throw new IllegalStateException("Unknown error");
                }
            }else {
                logger.warn("This book was added to your cart.");
                String message = "Book " + bookDTO.getTitle() + " author " + bookDTO.getAuthor() + " has already been added to your cart. Change quantity in your cart.";
                req.getSession().setAttribute("message", message);
                resp.sendRedirect(req.getContextPath() + "/user/books");
            }

        }else{
            logger.warn("Your fields are free.");
            req.getSession().setAttribute("error", "Your fields are free. Please fill in the form.");
            resp.sendRedirect(req.getContextPath() + "/user/books");
        }

    }
}
