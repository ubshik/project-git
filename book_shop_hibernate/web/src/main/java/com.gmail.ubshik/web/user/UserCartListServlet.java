package com.gmail.ubshik.web.user;

import com.gmail.ubshik.service.ICartService;
import com.gmail.ubshik.service.impl.CartServiceImpl;
import com.gmail.ubshik.service.model.CartDTO;
import com.gmail.ubshik.service.model.UserDTO;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Acer5740 on 13.08.2017.
 */
public class UserCartListServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(UserCartListServlet.class);
    private final ICartService cartService = CartServiceImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().removeAttribute("error");
        req.getSession().removeAttribute("message");
        logger.info("Trying to get list of cart by user from servlet UserCartList");
        List<CartDTO> listCartRecord = null;
        UserDTO userDTO =(UserDTO) req.getSession().getAttribute("userDTO");
        try {
            listCartRecord = cartService.getAllCartRecordByUserId(userDTO);
            req.setAttribute("listCartDTO", listCartRecord);
            req.getRequestDispatcher("/WEB-INF/jsp/user/user_cart_list.jsp").forward(req, resp);
        } catch (Exception e) {
            logger.warn("Mistake of trying to get list of cart by user");
            req.setAttribute("error", e.getMessage());
            req.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, resp);
        }

    }
}
