package com.gmail.ubshik.web.user;

import com.gmail.ubshik.service.ICartService;
import com.gmail.ubshik.service.IOrderService;
import com.gmail.ubshik.service.impl.CartServiceImpl;
import com.gmail.ubshik.service.impl.OrderServiceImpl;
import com.gmail.ubshik.service.model.CartDTO;
import com.gmail.ubshik.service.model.OrderDTO;
import com.gmail.ubshik.service.model.UserDTO;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Acer5740 on 13.08.2017.
 */
public class UserOrderListServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(UserOrderListServlet.class);
    private final IOrderService orderService = OrderServiceImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().removeAttribute("error");
        req.getSession().removeAttribute("message");
        logger.info("Trying to get list of order by user from servlet UserOrderList");
        List<OrderDTO> listOrder = null;
        UserDTO userDTO =(UserDTO) req.getSession().getAttribute("userDTO");
        try {
            listOrder = orderService.getAllOrderByUserId(userDTO);
            req.setAttribute("listOrderDTO", listOrder);
            req.getRequestDispatcher("/WEB-INF/jsp/user/user_order_list.jsp").forward(req, resp);
        } catch (Exception e) {
            logger.warn("Mistake of trying to get list of cart by user");
            req.setAttribute("error", e.getMessage());
            req.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, resp);
        }
    }
}
