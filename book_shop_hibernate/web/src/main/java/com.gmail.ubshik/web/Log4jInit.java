package com.gmail.ubshik.web;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public class Log4jInit extends HttpServlet {
    @Override
    public void init() throws ServletException {
        String prefix =  getServletContext().getRealPath("/");
        String file = getInitParameter("log4j-init-file");
        // if the log4j-init-file context parameter is not set, then no point in trying
        if(file != null) {
            PropertyConfigurator.configure(prefix + file);
            Logger logger = Logger.getRootLogger();
            getServletContext().setAttribute(new String("log4"), logger);
            getServletContext().setAttribute("file", file);
            logger.info("Log4J Logging started: " + prefix + file);
        }
    }
}
