package com.gmail.ubshik.web;

import com.gmail.ubshik.service.IUserService;
import com.gmail.ubshik.service.impl.UserServiceImpl;
import com.gmail.ubshik.service.model.UserDTO;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Acer5740 on 02.08.2017.
 */
public class LoginServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(LoginServlet.class);
    private final IUserService userService = UserServiceImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().removeAttribute("error");
        logger.info("Login request submitted");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        if(email == "" || email==null || password=="" || password == null){
            logger.warn("Your date is empty");
            req.getSession().setAttribute("error", "Your date is empty");
            resp.sendRedirect(req.getContextPath() + "/login");
//            req.getRequestDispatcher(req.getContextPath() + "/login").forward(req, resp);
        }else {
            try {
                UserDTO userDTO = userService.getUserDTOByEmailAndPassword(email, password);
                if (userDTO != null) {
                    if(userDTO.getUserStatus().toString().equals("ACTIVE")){
                        req.getSession().setAttribute("userDTO", userDTO);
                        req.getSession().setAttribute("userName", userDTO.getFirstName());
                        switch (userDTO.getRole()) {
                            case SUPERADMIN:
                                logger.info("User is superadmin. Redirect to /admin");
                                resp.sendRedirect(req.getContextPath() + "/admin");
                                break;
                            case ADMIN:
                                logger.info("User is admin. Redirect to /admin");
                                resp.sendRedirect(req.getContextPath() + "/admin");
                                break;
                            case USER:
                                logger.info("Redirect to /user");
                                resp.sendRedirect(req.getContextPath() + "/user");
                                break;
                            default:
                                throw new IllegalArgumentException("Unknown role");
                        }
                    }else {
                        logger.warn("User is blocked");
                        req.getSession().setAttribute("error", "Sorry.. Your status is blocked. Contact with admins to decide your problem.");
                        resp.sendRedirect(req.getContextPath() + "/login");
                    }
                } else {
                    logger.warn("Credentials is not valid");
                    req.getSession().setAttribute("error", "Credentials is not valid");
                    resp.sendRedirect(req.getContextPath() + "/login");
                }
            } catch (IOException e) {
                logger.warn("ExceptionServlet during request");
                throw new IllegalStateException("Unknown error");
            }
        }
    }
}
