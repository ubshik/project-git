package com.gmail.ubshik.model;

public class Car implements Identifation {
	private Integer id;
	private String producer;
	private String model;
	private int numberDoors;
	private int priceOneDay;
		
	public Car(Integer id, String producer, String model, int numberDoors, int priceOneDay) {
		this.id = id;
		this.producer = producer;
		this.model = model;
		this.numberDoors = numberDoors;
		this.priceOneDay = priceOneDay;
	}
	
	public Car(String producer, String model, int numberDoors, int priceOneDay){
		this.producer = producer;
		this.model = model;
		this.numberDoors = numberDoors;
		this.priceOneDay = priceOneDay;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getProducer() {
		return producer;
	}
	public void setProducer(String producer) {
		this.producer = producer;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getNumberDoors() {
		return numberDoors;
	}
	public void setNumberDoors(int numberDoors) {
		this.numberDoors = numberDoors;
	}
	public int getPriceOneDay() {
		return priceOneDay;
	}
	public void setPriceOneDay(int priceOneDay) {
		this.priceOneDay = priceOneDay;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + numberDoors;
		result = prime * result + priceOneDay;
		result = prime * result + ((producer == null) ? 0 : producer.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (numberDoors != other.numberDoors)
			return false;
		if (priceOneDay != other.priceOneDay)
			return false;
		if (producer == null) {
			if (other.producer != null)
				return false;
		} else if (!producer.equals(other.producer))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Car:/n[id = " + id + ", producer = " + producer + ", model = " + model + ", numberDoors = " + numberDoors
				+ ", priceOneDay = " + priceOneDay + "]";
	}
	
	
	
}
