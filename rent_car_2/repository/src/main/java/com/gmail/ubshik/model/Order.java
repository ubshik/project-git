package com.gmail.ubshik.model;

public class Order implements Identifation{
	private Integer id;
	private User user;
	private Car car;
	private int numberDaysToRent;
	private int totalPrice;
	
	public Order(Integer id, User user, Car car, int numberDaysToRent, int totalPrice) {
		super();
		this.id = id;
		this.user = user;
		this.car = car;
		this.numberDaysToRent = numberDaysToRent;
		this.totalPrice = car.getPriceOneDay()*numberDaysToRent;
	}
	
	public Order (int numberDaysToRent, int totalPrice){
		this.numberDaysToRent = numberDaysToRent;
		this.totalPrice = totalPrice;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public int getNumberDaysToRent() {
		return numberDaysToRent;
	}

	public void setNumberDaysToRent(int numberDaysToRent) {
		this.numberDaysToRent = numberDaysToRent;
	}

	public int getTotalPrice() {return totalPrice;	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((car == null) ? 0 : car.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + numberDaysToRent;
		result = prime * result + totalPrice;
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (car == null) {
			if (other.car != null)
				return false;
		} else if (!car.equals(other.car))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (numberDaysToRent != other.numberDaysToRent)
			return false;
		if (totalPrice != other.totalPrice)
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Order:/n[id = " + id + ", user = " + user + ", car = " + car + ", number days to rent = " + numberDaysToRent
				+ ", total price = " + totalPrice + "]";
	}
}
