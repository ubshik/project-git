package com.gmail.ubshik.entity;

/**
 * Created by Acer5740 on 25.06.2017.
 */
public class User {
    private com.gmail.ubshik.model.User user;
    private User userEntity;

    public User(Integer id, String firstname, String lastname, String email, String password, String userType) {
        user = new com.gmail.ubshik.model.User(id, firstname, lastname, email, password, userType);
    }

    public User(String firstname, String lastname, String email, String password){
        user = new com.gmail.ubshik.model.User(firstname, lastname, email, password);
    }

    com.gmail.ubshik.model.User getUser(){
        return user;
    }

    public String getEmail() {
        return user.getEmail();
    }

    public String getPassword() {return user.getPassword();	}

    public String getFirstname() {
        return user.getFirstname();
    }

    public String getLastname() {
        return user.getLastname();
    }

    public Integer getId() {
        return user.getId();
    }

    public String getUserType() {
        return user.getUserType();
    }

    public User returnUserEntity(){
        return userEntity;
    }
}
