package com.gmail.ubshik.service;

import com.gmail.ubshik.entity.Car;
import com.gmail.ubshik.entity.Order;
import com.gmail.ubshik.entity.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Service {
	private final static Logger LOGGER = Logger.getLogger(Service.class);

	String url = "jdbc:mysql://localhost:3306/car_rent?autoReconnect=true&useSSL=false";

	private Properties JDBCproperties() {
		Properties properties = new Properties();
		properties.put("user", "root");
		properties.put("password", "123123");
		properties.put("autoReconnect", "true");
		properties.put("characterEncoding", "UTF-8");
		properties.put("useUnicode", "true");
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			LOGGER.error("Driver for MySQL databse not found: " + e);
		}
		return properties;
	}
	
	public User getUser(String email, String password) throws SQLException {
		LOGGER.info("Trying get user by email and password from service");
		List<User> usersList = getUsersList();
		User user = null;
		for (User element : usersList) {
			if (element.getEmail().equals(email)) {
				user = element;
				if(user != null && user.getPassword().equals(password)){
					LOGGER.info("User finded : " + user.getFirstname() + " " + user.getLastname());
					return user;
				}
				user = null;
			}
		}
		LOGGER.warn("User is not found");
		return user;
	}
	
	public User getUser(Integer id) throws SQLException {
		LOGGER.info("Trying to get user by ID from service");
		List<User> usersList = getUsersList();
		User user = null;
		for (User element : usersList) {
			if (element.getId() == (id)) {
				user = element;
				LOGGER.info("User is founded");
				break;
			}else{
				LOGGER.warn("Mistake to find user");
				user = null;
			}
		}
		return user;
	}
	
	public List<User> getUsersList() throws SQLException {
		LOGGER.info("Trying to get user's list from service");
		User user = null;
		List<User> userList = null;
		Connection connection = DriverManager.getConnection(url, JDBCproperties());
		try (Statement statement = connection.createStatement();ResultSet resultsSet = statement.executeQuery("SELECT * FROM Users")) {
			connection.setAutoCommit(Boolean.FALSE);
			try {
				userList = new ArrayList<User>();
				while (resultsSet.next()) {
					Integer id = resultsSet.getInt(1);
					String firstname = resultsSet.getString(2);
					String lastname = resultsSet.getString(3);
					String email = resultsSet.getString(4);
					String password = resultsSet.getString(5);
					String type = resultsSet.getString(6);
					user = new User(id, firstname, lastname, email, password, type);
					userList.add(user);
				}
			} catch (SQLException e) {
				LOGGER.warn("Mistake getUsersList");
				throw e;
			}
		}catch (SQLException e) {
			LOGGER.error("It is not connection with database.");
		}finally {
			connection.close();
		}
		return userList;
	}
	
	public Car getCar(Integer id) throws SQLException {
		LOGGER.info("Trying to get car by ID from service");
		List<Car> carsList = getCars();
		Car car = null;
		for (Car element : carsList) {
			if (element.getId().equals(id)) {
				car = element;
				LOGGER.info("Car by id is found");
				break;
			}
		}
		return car;
	}

	public List<Car> getCars() throws SQLException {
		LOGGER.info("Trying to get list of cars from service");
		List<Car> carList = new ArrayList<Car>();
		Connection connection = DriverManager.getConnection(url, JDBCproperties());
		try (Statement statement = connection.createStatement();ResultSet resultsSet = statement.executeQuery("SELECT * FROM cars")){
			connection.setAutoCommit(Boolean.FALSE);
			try {
				while (resultsSet.next()) {
					Integer id = Integer.parseInt(resultsSet.getString(1));
					String producer = resultsSet.getString(2);
					String model = resultsSet.getString(3);
					Integer numberDoors = Integer.parseInt(resultsSet.getString(4));
					Integer priceOneDay = Integer.parseInt(resultsSet.getString(5));
					Car car = new Car(id, producer, model, numberDoors, priceOneDay);
					carList.add(car);
				}
			} catch (SQLException e) {
				LOGGER.warn("Mistake getCars");
				throw e;
			}
		}catch (SQLException e) {
			LOGGER.error("It is not connection with database.");
		}finally {
			connection.close();
		}
			return carList;
	}

	public List<Order> getUserOrdersList(Integer id) throws SQLException {
		LOGGER.info("Trying to get list of user's orders for user from service");
		List<Order> orderList = new ArrayList<Order>();
		String query = "SELECT * FROM car_rent.order WHERE id_user = ?";
		Connection connection = DriverManager.getConnection(url, JDBCproperties());
		try (PreparedStatement ps = connection.prepareStatement(query)){
			ps.setInt(1, id);
			ResultSet resultsSet = ps.executeQuery();
			connection.setAutoCommit(Boolean.FALSE);
			while (resultsSet.next()) {
				Integer orderId = resultsSet.getInt(1);
				User user = getUser(resultsSet.getInt(2));
				Car car = getCar(resultsSet.getInt(3));
				Integer numberDaysToRent = resultsSet.getInt(4);
				Integer totalPrice = resultsSet.getInt(5);
				Order order = new Order(orderId, user, car, numberDaysToRent, totalPrice);
				orderList.add(order);
			}
		}catch (SQLException e) {
			LOGGER.warn("Mistake getUserOrdersList");
			throw e;
		}finally {
			connection.close();
		}
		return orderList;
	}

	public List<Order> geAdminOrdersList() throws SQLException {
		LOGGER.info("Trying to get list of all user's order for admin from service");
		List<Order> orderList = new ArrayList<Order>();
		String query = "SELECT * FROM car_rent.order";
		Connection connection = DriverManager.getConnection(url, JDBCproperties());
		try (PreparedStatement ps = connection.prepareStatement(query); ResultSet resultsSet = ps.executeQuery()){
			connection.setAutoCommit(Boolean.FALSE);
			while (resultsSet.next()) {
				Integer orderId = resultsSet.getInt(1);
				User user = getUser(resultsSet.getInt(2));
				Car car = getCar(resultsSet.getInt(3));
				Integer numberDaysToRent = resultsSet.getInt(4);
				Integer totalPrice = resultsSet.getInt(5);
				Order order = new Order(orderId, user, car, numberDaysToRent, totalPrice);
				orderList.add(order);
			}
		}catch (SQLException e) {
			LOGGER.warn("Mistake getAdminOrdersList");
			throw e;
		}finally {
			connection.close();
		}
		return orderList;
	}
	
	public int addUser(User user) throws SQLException {
		LOGGER.info("Trying to add user from service");
		int checkpoint = 1;
		List<User> usersList = getUsersList();
		for (User element : usersList) {
			if (element.getEmail().equals(user.getEmail()))
				checkpoint = 0;
		}
		if (checkpoint == 1) {
			Connection connection = DriverManager.getConnection(url, JDBCproperties());
			String query = "Insert into users(firstname, lastname, email, password) values(?,?,?,?)";
			try (PreparedStatement preparedStatement = connection.prepareStatement(query)){
				preparedStatement.setString(1, user.getFirstname());
				preparedStatement.setString(2, user.getLastname());
				preparedStatement.setString(3, user.getEmail());
				preparedStatement.setString(4, user.getPassword());
				checkpoint = preparedStatement.executeUpdate();
				LOGGER.info("Update finished with code: " + checkpoint);
				if (checkpoint !=1){
					LOGGER.warn("Add more than 1 user");
				}
			}catch (SQLException e) {
				LOGGER.warn("Mistake addUser");
			}finally {
				connection.close();
			}
		} else {
			LOGGER.error("User with this email already exists");
		}
		return checkpoint;
	}
	
	public User createUser(String firstname, String lastname, String email, String password){
		LOGGER.info("Trying to create user by 4 parameters from service");
		User user = new User(firstname, lastname, email, password);
		return user;
	}
	
	public int addCar(Car car) throws SQLException {
		LOGGER.info("Trying to add car from service");
		int checkpoint = 1;
		Connection connection = DriverManager.getConnection(url, JDBCproperties());
		String query = "INSERT INTO cars (producer, model, number_doors, price_one_day) VALUES (?, ?, ?, ?)";
		try (PreparedStatement ps = connection.prepareStatement(query);){
			ps.setString(1, car.getProducer());
			ps.setString(2, car.getModel());
			ps.setInt(3, car.getNumberDoors());
			ps.setInt(4, car.getPriceOneDay());
			checkpoint = ps.executeUpdate();	
			if(checkpoint!=1){
				LOGGER.warn("Add more than 1 car");
			}
		}catch (SQLException e) {
			LOGGER.error("Mistake addCar");
		}finally{
			connection.close();
		}
		return checkpoint;
	}
	
	public Car createCar(String producer, String model, int numberDoors, int priceOneDay){
		LOGGER.info("Trying to create car by 4 parameters from service");
		Car car = new Car(producer, model, numberDoors, priceOneDay);
		return car;
	}
	
	public Car createCar(Integer id, String producer, String model, Integer numberDoors, Integer priceOneDay) throws SQLException {
		LOGGER.info("Trying to create car by all parameters");
		Car car = new Car(id, producer, model, numberDoors, priceOneDay);
		return car;
	}
	
	public int addOrder(User user, Car car, Order order) throws SQLException {
		LOGGER.info("Trying to add order from service");
		int checkpoint = 1;
		Connection connection = DriverManager.getConnection(url, JDBCproperties());
		String query = "INSERT INTO car_rent.order (`id_user`, `id_car`, `number_day_rent`, `total_price`) values (?, ?, ?, ?)";
		try (PreparedStatement ps = connection.prepareStatement(query)){
			ps.setInt(1, user.getId());;
			ps.setInt(2, car.getId());
			ps.setInt(3, order.getNumberDaysToRent());
			ps.setInt(4, order.getTotalPrice());
			checkpoint = ps.executeUpdate();	
			if(checkpoint!=1){
				LOGGER.warn("Add more than 1 carr");;
			}
		}catch (SQLException e) {
			LOGGER.error("Mistake addCar");
			throw e;
		}finally{
			connection.close();
		}
		return checkpoint;
	}
	
	public Order createOrder (Integer numberDaysToRent, Integer totalPrice){
		LOGGER.info("Trying to create order by 2 parameters from service");
		Order order = new Order(numberDaysToRent, totalPrice);
		return order;
	}
	
	public int updateCar(Car car) throws SQLException {
		LOGGER.info("Trying to update car from service");
		int checkpoint = 1;
		Connection connection = DriverManager.getConnection(url, JDBCproperties());
		String query = "UPDATE cars SET producer = ?, model = ?, number_doors = ?, price_one_day = ? WHERE id_car = ?";
		try (PreparedStatement ps = connection.prepareStatement(query)){
			ps.setString(1, car.getProducer());
			ps.setString(2, car.getModel());
			ps.setInt(3, car.getNumberDoors());
			ps.setInt(4, car.getPriceOneDay());
			ps.setInt(5, car.getId());
			checkpoint = ps.executeUpdate();	
			if(checkpoint!=1){
				LOGGER.warn("Update more than 1 car");
			}
		}catch (SQLException e) {
			LOGGER.error("Mistake updateCar");
		}finally{
			connection.close();
		}
		return checkpoint;
	}
	
	public int carDelete(Integer id) throws SQLException {
		LOGGER.info("Trying to delete car from service");
		int checkpoint = 1;
		Connection connection = DriverManager.getConnection(url, JDBCproperties());
		String query = "DELETE FROM cars where id_car = ?";
		try(PreparedStatement preparedStmt = connection.prepareStatement(query)){
			preparedStmt.setInt(1, id);
			checkpoint = preparedStmt.executeUpdate();
			if(checkpoint!=1){
				LOGGER.warn("Delete more than 1 car");
			}
		}catch (SQLException e) {
			LOGGER.error("Incorrect trying to delete car");
		}finally{
			connection.close();
		}
		return checkpoint;
	}	

	public int orderDelete(Integer id) throws SQLException {
		LOGGER.info("Trying to delete order from service");
		int checkpoint = 1;
		Connection connection = DriverManager.getConnection(url, JDBCproperties());
		String query = "DELETE FROM car_rent.order where id_order = ?";
		try(PreparedStatement preparedStmt = connection.prepareStatement(query);){
			preparedStmt.setInt(1, id);
			checkpoint = preparedStmt.executeUpdate();
			if(checkpoint!=1){
				LOGGER.error("Delete more than 1 order");
				new SQLException();
			}
			preparedStmt.close();
		}catch (SQLException e) {
			LOGGER.error("Incorrect trying to delete order");
			throw e;
		}finally{
			connection.close();
		}
		return checkpoint;
	}
}
