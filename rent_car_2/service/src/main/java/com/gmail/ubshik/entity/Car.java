package com.gmail.ubshik.entity;

/**
 * Created by Acer5740 on 25.06.2017.
 */
public class Car {
    private com.gmail.ubshik.model.Car car;
    private Car carEntity;

    public Car(String producer, String model, Integer numberDoors, Integer priceOneDay){
        car = new com.gmail.ubshik.model.Car(producer, model, numberDoors, priceOneDay);
    }

    public Car(Integer id, String producer, String model, Integer numberDoors, Integer priceOneDay){
        car = new com.gmail.ubshik.model.Car(id, producer, model, numberDoors, priceOneDay);
    }

    com.gmail.ubshik.model.Car getCar(){
        return car;
    }

    public Integer getId(){
        return car.getId();
    }

    public String getProducer(){
       return car.getProducer();
    }

    public String getModel() {
        return car.getModel();
    }

    public Integer getNumberDoors(){
        return car.getNumberDoors();
    }

    public Integer getPriceOneDay(){
        return car.getPriceOneDay();
    }

    public Car getCarEntity(){
        return carEntity;
    }
}
