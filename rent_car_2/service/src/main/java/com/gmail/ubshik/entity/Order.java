package com.gmail.ubshik.entity;

/**
 * Created by Acer5740 on 25.06.2017.
 */
public class Order {
    private com.gmail.ubshik.model.Order order;

    public Order (int numberDaysToRent, int totalPrice){
        order = new com.gmail.ubshik.model.Order(numberDaysToRent, totalPrice);
    }

    public Order(Integer id, User user, Car car, int numberDaysToRent, int totalPrice) {
        order = new com.gmail.ubshik.model.Order(id, user.getUser(), car.getCar(), numberDaysToRent, totalPrice);
    }

    public int getNumberDaysToRent() {
        return order.getNumberDaysToRent();
    }

    public int getTotalPrice() {return order.getTotalPrice();	}

    public Integer getId(){
        return order.getId();
    }

    public String getCarProducer()
    {
        return order.getCar().getProducer();
    }

    public String getCarModel()
    {
        return order.getCar().getModel();
    }

    public String getUserFirstname()
    {
        return order.getUser().getFirstname();
    }

    public String getUserLastname()
    {
        return order.getUser().getLastname();
    }
}
