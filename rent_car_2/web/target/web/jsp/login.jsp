<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
 <head>
  <meta charset="utf-8">
  <title>Rent car office</title>
 </head>
 <body>
 <h2>Let's log in or register to work with site</h2>
 	<form name="LoginForm" method="post" action="${pageContext.request.contextPath}/login">
  		<b>Enter your email:</b>
   		<input name="email" type="text" size="50"><br>
   		<b>Enter your password :</b>
   		<input name="password" type="password" size="50">
  		<p><input type="submit" value="Enter"></p><br>
 	</form>
	<a href ="${pageContext.request.contextPath}/jsp/register.jsp">Registration</a>
 </body>
</html>