<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
 <head>
  <meta charset="utf-8">
  <title>Congratulation</title>
 </head>
 <body>
 	<h2><c:out value="${success}" /></h2>
	<img src="${pageContext.request.contextPath}/picture/user_success.jpg" alt="rent car"><br>	
	<%@include file="/jsp/user_include.jsp"%>
 </body>
</html>