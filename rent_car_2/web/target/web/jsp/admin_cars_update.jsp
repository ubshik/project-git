<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
 <head>
  <meta charset="utf-8">
  <title>Update a car</title>
 </head>
 <body>
 	<h2>UPDATE A CAR</h2> 
 	<p>All fields you should fill in again!</p> 
 	<form name="UpdateCarForm" method="post" action="${pageContext.request.contextPath}/admin/cars/update">
 		<input name="id" type="hidden" size="50" value="${id}"><br><br>
 		<b>Enter Producer : </b>
   		<input name="producer" type="text" size="50" placeholder="${producer}"><br><br>
   		<b>Enter Model : </b>
   		<input name="model" type="text" size="50" placeholder="${model}"><br><br>
  		<b>Enter Number of doors : </b>
   		<input name="numberDoors" type="text" size="50" placeholder="${numberDoors}"><br><br>
   		<b>Enter Price of one day : </b>
   		<input name="priceOneDay" type="text" size="50" placeholder="${priceOneDay}"><br><br>
  		<input type="submit" value="Update a car">	
 	</form> <br>
	<%@include file="/jsp/admin_include.jsp"%>
 </body>
</html>