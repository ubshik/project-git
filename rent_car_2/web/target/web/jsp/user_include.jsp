<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

 		<form name="CarList" method="post" action="${pageContext.request.contextPath}/user/cars">
  			<p><input type="submit" value="Car view" ></p>
 		</form>
 		<form name="OrderList" method="post" action="${pageContext.request.contextPath}/user/orders">
 		 	<p><input name="user_id" type="hidden" value="${user.id}" ></p>
  			<p><input type="submit" value="Order view"></p>
 		</form>
 		<form name="Logout" method="get" action="${pageContext.request.contextPath}/logout">
  			<p><input type="submit" value="Logout"></p><br>
 		</form>