<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
 <head>
  <meta charset="utf-8">
  <title>Menu of cars</title>
 </head>
 <body>
 <h2>RENT CARS</h2> 
 	<table>
    	<tr>
            <th>Id</th>
            <th>Producer</th>
            <th>Model</th>
            <th>Number doors</th>
            <th>Price One Day</th>
            <th>Action</th>
        </tr>
     <c:forEach var="car" items="${car}">
         <tr>
             <td><c:out value="${car.id}"/></td>
             <td><c:out value="${car.producer}"/></td>
             <td><c:out value="${car.model}"/></td>
             <td><c:out value="${car.numberDoors}"/></td>
             <td><c:out value="${car.priceOneDay}"/></td>
             <td><a href="${pageContext.request.contextPath}/user/cars/order?id=${car.id}">Order</a></td>
         </tr>
     </c:forEach>
    </table>
     <form name="OrderList" method="post" action="${pageContext.request.contextPath}/user/orders">
 		 <p><input name="user_id" type="hidden" value="${user.id}" ></p>
  		<p><input type="submit" value="Order view"></p>
 	</form>
    <form name="Logout" method="get" action="${pageContext.request.contextPath}/logout">
  		<p><input type="submit" value="Logout"></p><br>
 	</form>
 </body>
</html>