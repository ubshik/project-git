<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
 <head>
  <meta charset="utf-8">
  <title>Congratulation</title>
 </head>
 <body>
	<h1>Hello, ${firstname}!</h1>
	<h2>Welcome to site RENT CAR!</h2>
	<img src="${pageContext.request.contextPath}/picture/logo.jpg" alt="rent car"><br>	
	<%@include file="/jsp/user_include.jsp"%>
 </body>
</html>