<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
 <head>
  <meta charset="utf-8">
  <title>Order a car</title>
 </head>
 <body>
 	<h2>ORDER A CAR</h2>  
 	<form name="OrderCarForm" method="post" action="${pageContext.request.contextPath}/user/orders/add">
 		<input name="id_car" type="hidden" size="50" value="${id_car}"><br><br>
 		<b>Enter Number days for rent a car : </b>
   		<input name="numberDays" type="text" size="50" ><br><br>
  		<input type="submit" value="Order a car">	
 	</form> <br><br>
 	<%@include file="/jsp/user_include.jsp"%>
 </body>
</html>