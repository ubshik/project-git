<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
 <head>
  <meta charset="utf-8">
  <title>Error</title>
 </head>
 <body>
 	<h1>ERROR</h1>
 	<h2>${error}</h2>
 	<img src="${pageContext.request.contextPath}/picture/user_error.jpg" alt="rent car"><br>
    <br/>
	<a href ="${pageContext.request.contextPath}/index.jsp">Log in</a>
	
	
 </body>
</html>
