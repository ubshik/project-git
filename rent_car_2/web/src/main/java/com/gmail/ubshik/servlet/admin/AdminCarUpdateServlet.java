package com.gmail.ubshik.servlet.admin;

import com.gmail.ubshik.entity.Car;
import com.gmail.ubshik.service.Service;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AdminCarUpdateServlet extends HttpServlet {

	private final static Logger LOGGER = Logger.getLogger(AdminCarUpdateServlet.class);
	
	private final Service service = new Service();

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("Trying to update car by from servlet AdminCarUpdateServlet");
		Integer id = Integer.valueOf(request.getParameter("id"));
		try{
			Car car = service.getCar(id);
			request.setAttribute("id", car.getId()); 
			request.setAttribute("producer", car.getProducer()); 
			request.setAttribute("model", car.getModel()); 
			request.setAttribute("numberDoors", car.getNumberDoors()); 
			request.setAttribute("priceOneDay", car.getPriceOneDay()); 
			request.getRequestDispatcher("/jsp/admin_cars_update.jsp").forward(request, response);
		}catch (Exception e) {
			LOGGER.error("Mistake to update car by id");
			request.setAttribute("error", "Mistake to update car by id");
			request.getRequestDispatcher("/jsp/admin_error.jsp").forward(request, response);
		}		
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("Trying to get car by id from servlet AdminCarUpdateServlet");
		String producer = null;
		String model = null;
		Integer numberDoors = null;
		Integer priceOneDay = null;
		Integer id = null;
		try{
			id = Integer.valueOf(request.getParameter("id")); 
			producer = request.getParameter("producer"); 
			model = request.getParameter("model"); 
			numberDoors = Integer.parseInt(request.getParameter("numberDoors")); 
			priceOneDay = Integer.parseInt(request.getParameter("priceOneDay")); 
		}catch (Exception e) {
			LOGGER.warn("Your date is uncorrected.");
			request.setAttribute("error", "Your date is uncorrected. Please fill its right");
			request.getRequestDispatcher("/jsp/admin_error.jsp").forward(request, response);
			return;
		}
		Car car = null;
		int checkpoint = 1;
		if(!id.equals("") && !producer.equals("") && !model.equals("")
				&& !numberDoors.equals("") && !priceOneDay.equals("")){
			try{
				car = service.createCar(id, producer, model, numberDoors, priceOneDay); 
				checkpoint = service.updateCar(car); 
				if(checkpoint == 1){
					request.setAttribute("success", "Car is updated success");
					request.getRequestDispatcher("/jsp/admin_success_action.jsp").forward(request, response);
				}else{
					LOGGER.warn("Mistake in the car update");
					request.setAttribute("error", "Mistake in the car update");
					request.getRequestDispatcher("/jsp/admin_error.jsp").forward(request, response);
				}
			}catch (Exception e) {
				LOGGER.error("Mistake " + e.getMessage());
				throw new IllegalStateException("Unknown error");
			}
		}else{
			LOGGER.warn("Your fields are free.");
			request.setAttribute("error", "Your fields are free. Please fill in the form");
			request.getRequestDispatcher("/jsp/admin_error.jsp").forward(request, response);
		}
		
	}
}
