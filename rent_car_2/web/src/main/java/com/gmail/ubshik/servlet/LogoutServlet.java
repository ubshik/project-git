package com.gmail.ubshik.servlet;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name="logout",urlPatterns={"/logout"})
public class LogoutServlet extends HttpServlet {

	private final static Logger LOGGER = Logger.getLogger(LogoutServlet.class);

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("Trying to logout from servlet LogoutServlet");
		request.getSession().setAttribute("user", null);
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}
}
