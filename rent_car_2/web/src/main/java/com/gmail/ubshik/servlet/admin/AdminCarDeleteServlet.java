package com.gmail.ubshik.servlet.admin;

import com.gmail.ubshik.service.Service;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AdminCarDeleteServlet extends HttpServlet {

	private final static Logger LOGGER = Logger.getLogger(AdminCarDeleteServlet.class);
	
	private final Service service = new Service();

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("Trying to delete car by admin from servlet AdminCarDeleteServlet");
		Integer id = Integer.valueOf(request.getParameter("id"));
		int checkpoint = 1;			
			try{
				checkpoint = service.carDelete(id);
				if(checkpoint == 1){
					request.setAttribute("success", "Car is deleted success");
					request.getRequestDispatcher("/jsp/admin_success_action.jsp").forward(request, response);
				}else{
					LOGGER.warn("Mistake in the car delete");
					request.setAttribute("error", "Mistake in the car delete");
					request.getRequestDispatcher("/jsp/admin_error.jsp").forward(request, response);
				}
			}catch (Exception e) {
				LOGGER.error("Mistake " + e.getMessage());
				throw new IllegalStateException("Unknown error");
			}		
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
