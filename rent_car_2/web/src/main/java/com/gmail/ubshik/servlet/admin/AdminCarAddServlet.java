package com.gmail.ubshik.servlet.admin;

import com.gmail.ubshik.entity.Car;
import com.gmail.ubshik.service.Service;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AdminCarAddServlet extends HttpServlet {

	private final static Logger LOGGER = Logger.getLogger(AdminCarAddServlet.class);
	
	private final Service service = new Service();

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("Trying to add car from servlet AdminCarAddServlet");
		String producer = null;
		String model = null;
		Integer numberDoors = null;
		Integer priceOneDay = null;
		try{
			producer = request.getParameter("producer");
			model = request.getParameter("model");
			numberDoors = Integer.parseInt(request.getParameter("numderDoors"));
			priceOneDay = Integer.parseInt(request.getParameter("priceOneDay"));
		}catch (Exception e) {
			LOGGER.warn("Your date is uncorrect.");
			request.setAttribute("error", "Your date is uncorrect. Please fill its right");
			request.getRequestDispatcher("/jsp/admin_error.jsp").forward(request, response);
			return;
		}
		Car car = null;
		int checkpoint = 1;
		if(!producer.equals("") && !model.equals("")
				&& !numberDoors.equals("") && !priceOneDay.equals("")){
			
			try{
				car = service.createCar(producer, model, numberDoors, priceOneDay);
				checkpoint = service.addCar(car);
				if(checkpoint == 1){
					request.setAttribute("success", "Car is added success");
					request.getRequestDispatcher("/jsp/admin_success_action.jsp").forward(request, response);
				}else{
					LOGGER.warn("Mistake in the car add");
					request.setAttribute("error", "Mistake in the car add");
					request.getRequestDispatcher("/jsp/admin_error.jsp").forward(request, response);
				}
			}catch (Exception e) {
				LOGGER.error("Mistake " + e.getMessage());
				throw new IllegalStateException("Unknown error");
			}
		}else{
			LOGGER.warn("Your fields are free.");
			request.setAttribute("error", "Your fields are free. Please fill in the form");
			request.getRequestDispatcher("/jsp/admin_error.jsp").forward(request, response);
		}
		
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
