package com.gmail.ubshik.servlet.user;

import com.gmail.ubshik.entity.Order;
import com.gmail.ubshik.service.Service;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class UserOrderListServlet extends HttpServlet {

	private final static Logger LOGGER = Logger.getLogger(UserOrderListServlet.class);
	
	private final Service service = new Service();

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("Trying to get list of order by user from servlet UserOrderListServlet");
		List<Order> listOrder = null;
		Integer userId = Integer.parseInt(request.getParameter("user_id"));
		try{

			listOrder = service.getUserOrdersList(userId);
			request.setAttribute("order", listOrder);
			request.getRequestDispatcher("/jsp/user_orders.jsp").forward(request, response);
		}catch (Exception e) {
			LOGGER.warn("Mistake of trying to get list of order");
			request.setAttribute("error", e.getMessage());
			request.getRequestDispatcher("/jsp/user_error.jsp").forward(request, response);
		}		
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
