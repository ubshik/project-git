package com.gmail.ubshik.servlet;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

/**
 * Created by Acer5740 on 26.06.2017.
 */
@WebServlet(name="log4j-init",urlPatterns={"/Log4jInit"})
public class Log4jInit extends HttpServlet {
    public void init(){
        String prefix =  getServletContext().getRealPath("/");
        String file = getInitParameter("log4j-init-file");
        // if the log4j-init-file context parameter is not set, then no point in trying
        if(file != null){
            PropertyConfigurator.configure(prefix+file);
            Logger LOGGER = Logger.getRootLogger();
            getServletContext().setAttribute(new String("log4"), LOGGER);
            getServletContext().setAttribute("file", file);
            LOGGER.info("Log4J Logging started: " + prefix+file);
        }
    }
}
