package com.gmail.ubshik.servlet.user;

import com.gmail.ubshik.service.Service;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserOrderDeleteServlet extends HttpServlet {

	private final static Logger LOGGER = Logger.getLogger(UserOrderDeleteServlet.class);
	
	private final Service service = new Service();

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("Trying to delete order by user from servlet UserOrderDeleteServlet");
		Integer id = Integer.valueOf(request.getParameter("id"));
		int checkpoint = 1;			
			try{
				checkpoint = service.orderDelete(id);
				if(checkpoint == 1){
					request.setAttribute("success", "Order is deleted success");
					request.getRequestDispatcher("/jsp/user_success_action.jsp").forward(request, response);
				}else{
					LOGGER.warn("Mistake in the order delete");
					request.setAttribute("error", "Mistake in the order delete");
					request.getRequestDispatcher("/jsp/user_error.jsp").forward(request, response);
				}
			}catch (Exception e) {
				LOGGER.error("Mistake " + e.getMessage());
				throw new IllegalStateException("Unknown error");
			}		
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
