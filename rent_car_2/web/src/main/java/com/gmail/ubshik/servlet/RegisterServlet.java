package com.gmail.ubshik.servlet;

import com.gmail.ubshik.entity.User;
import com.gmail.ubshik.service.Service;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name="register",urlPatterns={"/register"})
public class RegisterServlet extends HttpServlet {

	private final static Logger LOGGER = Logger.getLogger(RegisterServlet.class);

	private final Service service = new Service();

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("Trying to register from servlet RegisterServlet");
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		User user = null;
		int checkpoint = 1;
		if(!firstname.equals("") && !lastname.equals("")
				&& !email.equals("") && !password.equals("")){
			try{
				user = service.createUser(firstname, lastname, email, password);
				checkpoint = service.addUser(user);
				if(checkpoint == 1){
					request.getSession().setAttribute("user", user);
					request.setAttribute("firstname", user.getFirstname());
					request.getRequestDispatcher("/jsp/user_success.jsp").forward(request, response);
				}else{
					LOGGER.info("User with this email already exists");
					request.setAttribute("error", "User with this email already exists");
					request.getRequestDispatcher("/jsp/error.jsp").forward(request, response);
				}
			}catch (Exception e) {
				LOGGER.error("Mistake" + e.getMessage());
				throw new IllegalStateException("Unknown error");
			}
		}else{
			LOGGER.warn("Your filds are free.");
			request.setAttribute("error", "Your filds are free. Please fill in the form");
			request.getRequestDispatcher("/jsp/error.jsp").forward(request, response);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
