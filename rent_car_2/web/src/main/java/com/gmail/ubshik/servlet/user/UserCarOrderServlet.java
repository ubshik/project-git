package com.gmail.ubshik.servlet.user;

import com.gmail.ubshik.entity.Car;
import com.gmail.ubshik.service.Service;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserCarOrderServlet extends HttpServlet {

	private final static Logger LOGGER = Logger.getLogger(UserCarOrderServlet.class);
	
	private final Service service = new Service();

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("Trying to get user's list of orders from servlet UserCarOrderServlet");
		Integer id = Integer.valueOf(request.getParameter("id"));
		try{
			Car car = service.getCar(id);
			request.setAttribute("id_car", car.getId()); 
			request.setAttribute("producer", car.getProducer());
			request.setAttribute("model", car.getModel());
			request.setAttribute("priceOneDay", car.getPriceOneDay());
			request.getRequestDispatcher("/jsp/user_cars_order_calculate.jsp").forward(request, response);
		}catch (Exception e) {
			LOGGER.warn("Mistake to get car by id");
			request.setAttribute("error", "Mistake to get car by id");
			request.getRequestDispatcher("/jsp/user_error.jsp").forward(request, response);
		}		
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
