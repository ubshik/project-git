package com.gmail.ubshik.servlet.user;

import com.gmail.ubshik.entity.Car;
import com.gmail.ubshik.entity.Order;
import com.gmail.ubshik.entity.User;
import com.gmail.ubshik.service.Service;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserOrderAddServlet extends HttpServlet {

	private final static Logger LOGGER = Logger.getLogger(UserOrderAddServlet.class);
	
	private final Service service = new Service();

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("Trying to add order by user from servlet UserOrderAddServlet");
		Integer id_user = Integer.parseInt(request.getParameter("id_user"));
		Integer id_car = Integer.parseInt(request.getParameter("id_car")); 
		Integer numberDays = Integer.parseInt(request.getParameter("numberDays")); 
		Integer totalPrice = Integer.parseInt(request.getParameter("totalPrice")); 
		Car car = null;
		User user = null;
		int checkpoint = 1;		
		try{
			user = service.getUser(id_user);
			car = service.getCar(id_car);
			Order order = service.createOrder(numberDays, totalPrice);
			checkpoint = service.addOrder(user, car, order);
			if(checkpoint == 1){
				LOGGER.info("Checkpoint " + checkpoint);
				request.setAttribute("success", "Order is added success. Total price is $" + order.getTotalPrice() + ".");
				request.getRequestDispatcher("/jsp/user_success_action.jsp").forward(request, response);
			}else{
				LOGGER.warn("Mistake in the order add");
				request.setAttribute("error", "Mistake in the car add");
				request.getRequestDispatcher("/jsp/user_error.jsp").forward(request, response);
			}
			}catch (Exception e) {
				LOGGER.error("Mistake in the order add " + e.getMessage());
				throw new IllegalStateException("Unknown error" + e);
			}		
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
