package com.gmail.ubshik.services.model;

import com.gmail.ubshik.repo.model.Cart;
import com.gmail.ubshik.repo.model.Order;
import com.gmail.ubshik.repo.model.OrderStatus;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Acer5740 on 30.07.2017.
 */
public class OrderDTO {

    private Integer orderId;
    private Double totalPrice;
    private OrderStatus orderStatus;
    private Set<Cart> carts = new HashSet<>();

    public OrderDTO(Order order){
        this(
                newBuilder()
                    .orderId(order.getOrderId())
                    .totalPrice(order.getTotalPrice())
                    .orderStatus(order.getOrderStatus())
                    .carts(order.getCarts())
        );
    }

    private OrderDTO(Builder builder){
        setOrderId(builder.orderId);
        setTotalPrice(builder.totalPrice);
        setOrderStatus(builder.orderStatus);
        setCarts(builder.carts);
    }

    public static Builder newBuilder(){
        return new Builder();
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Set<Cart> getCarts() {
        return carts;
    }

    public void setCarts(Set<Cart> carts) {
        this.carts = carts;
    }

    public static final class Builder{
        private Integer orderId;
        private Double totalPrice;
        private OrderStatus orderStatus;
        private Set<Cart> carts = new HashSet<>();

        private Builder(){}

        public Builder orderId(Integer val){
            orderId = val;
            return this;
        }

        public Builder totalPrice (Double val){
            totalPrice = val;
            return this;
        }

        public Builder orderStatus(OrderStatus val){
            orderStatus = val;
            return this;
        }

        public Builder carts (Set<Cart> val){
            carts = val;
            return this;
        }

        public OrderDTO build(){
            return new OrderDTO(this);
        }
    }
}
