package com.gmail.ubshik.services.model;

import com.gmail.ubshik.repo.model.Cart;
import com.gmail.ubshik.repo.model.CartId;
import com.gmail.ubshik.repo.model.Order;

/**
 * Created by Acer5740 on 30.07.2017.
 */
public class CartDTO {
    private Integer cartId;
    private CartId pk = new CartId();
    private Double price;
    private Integer quantity;
    private Double sum;
    private Order order;

    public CartDTO(Cart cart){
        this(
                newBuilder()
                    .cartId(cart.getCartId())
                    .pk(cart.getPk())
                    .price(cart.getPrice())
                    .quantity(cart.getQuantity())
                    .sum(cart.getSum())
                    .order(cart.getOrder())
        );
    }

    private CartDTO(Builder builder){
        setCartId(builder.cartId);
        setPk(builder.pk);
        setPrice(builder.price);
        setQuantity(builder.quantity);
        setSum(builder.sum);
        setOrder(builder.order);
    }

    public static Builder newBuilder(){
        return new Builder();
    }

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public CartId getPk() {
        return pk;
    }

    public void setPk(CartId pk) {
        this.pk = pk;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public static final class Builder{
        private Integer cartId;
        private CartId pk = new CartId();
        private Double price;
        private Integer quantity;
        private Double sum;
        private Order order;

        private Builder(){}

        public Builder cartId(Integer val){
            cartId = val;
            return this;
        }

        public Builder pk(CartId val){
            pk = val;
            return this;
        }

        public Builder price(Double val){
            price = val;
            return this;
        }

        public Builder quantity(Integer val){
            quantity = val;
            return this;
        }

        public Builder sum(Double val){
            sum = val;
            return this;
        }

        public Builder order(Order val){
            order = val;
            return this;
        }

        public CartDTO build(){
            return new CartDTO(this);
        }
    }
}
