package com.gmail.ubshik.services.model;

import com.gmail.ubshik.repo.model.Book;
import com.gmail.ubshik.repo.model.CartId;
import com.gmail.ubshik.repo.model.User;

/**
 * Created by Acer5740 on 30.07.2017.
 */
public class CartIdDTO {
    private User user;
    private Book book;

    public CartIdDTO (CartId cartId){
        this(
                newBuilder()
                    .user(cartId.getUser())
                    .book(cartId.getBook())
        );
    }

    private CartIdDTO(Builder builder){
        setUser(builder.user);
        setBook(builder.book);
    }

    public static Builder newBuilder(){
        return new Builder();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public static final class Builder{
        private User user;
        private Book book;

        private Builder(){}

        public Builder user (User val){
            user = val;
            return this;
        }

        public Builder book(Book val){
            book = val;
            return this;
        }

        public CartIdDTO build(){
            return new CartIdDTO(this);
        }
    }
}
