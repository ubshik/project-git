package com.gmail.ubshik.services;

import com.gmail.ubshik.repo.UserHbnDao;
import com.gmail.ubshik.repo.impl.UserHbnDaoImpl;
import com.gmail.ubshik.repo.model.User;
import com.gmail.ubshik.services.model.UserDTO;
import com.gmail.ubshik.repo.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.sql.SQLException;
import java.util.Date;

/**
 * Created by Acer5740 on 30.07.2017.
 */
public class UserService {
    private static final Logger logger = Logger.getLogger(UserService.class);
    private static UserHbnDao userDao = new UserHbnDaoImpl();
//    private Session getSession = HibernateUtil.getCurrentSession();
    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    private static UserService instance = null;

    public static UserService getInstance(){
        if (instance == null){
            instance = new UserService();
        }
        return instance;
    }

    public UserDTO getUserDTOByEmailAndPassword(String email, String password) {
        logger.info("Trying to get user by email and password.");
        Session getSession = null;
        User user = null;
        try{
            getSession = sessionFactory.getCurrentSession();
            getSession.beginTransaction();
            user = userDao.getUserByEmailAndPassword(email, password);
            getSession.getTransaction().commit();
        }catch (HibernateException e){
            logger.error("User with email " + email + " and password " + password + " don't found. " + e);
            getSession.getTransaction().rollback();
        }
        if (user == null){
            return null;
        }
        return new UserDTO(user);
    }

    public UserDTO getUserDTOByEmail(String email) throws SQLException {
        logger.info("Trying to get user by email.");
        User user = null;
        Session getSession = null;
        try{
            getSession = sessionFactory.getCurrentSession();
            getSession.beginTransaction();
            user = userDao.getUserByEmail(email);
            getSession.getTransaction().commit();
        }catch (HibernateException e){
            logger.error("User with email " + email + " don't found. " + e);
            getSession.getTransaction().rollback();
        }
        if (user == null) {
            return null;
        }
            return new UserDTO(user);
    }

    public UserDTO createUserDTO(String firstname, String lastname, String email, String password,
                                 String phone, String address, String addInfo, Date date){
        logger.info("Trying to create user from UserService.");
        Session getSession = null;
        User user = null;
        Integer count = 1;
        if (addInfo.equals("") || addInfo == null) {
            try {
                getSession = sessionFactory.getCurrentSession();
                Transaction tx = getSession.beginTransaction();
                user = User.newBuilder().firstName(firstname).lastName(lastname)
                        .email(email).password(password).phone(phone).address(address)
                        .dateRegistration(date).build();
                userDao.setSession(getSession);
                count = userDao.save(user);
                logger.info("Count saving user " + count);
                getSession.getTransaction().commit();
            } catch (HibernateException e) {
                logger.error("It is not possible to save user " + firstname + " " + lastname + ". " + e);
                getSession.getTransaction().rollback();
                return null;
            }
        } else {
            try {
                getSession.beginTransaction();
                user = User.newBuilder().firstName(firstname).lastName(lastname)
                        .email(email).password(password).phone(phone).address(address)
                        .addInfo(addInfo).dateRegistration(date).build();
                count = userDao.save(user);
                logger.info("Count saving user " + count);
                getSession.getTransaction().commit();
            } catch (HibernateException e) {
                logger.error("It is not possible to save user " + firstname + " " + lastname + ". " + e);
                getSession.getTransaction().rollback();
            }
        }
        return new UserDTO(user);
    }




}
