package com.gmail.ubshik.services.model;

import com.gmail.ubshik.repo.model.Cart;
import com.gmail.ubshik.repo.model.Role;
import com.gmail.ubshik.repo.model.User;
import com.gmail.ubshik.repo.model.UserStatus;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Acer5740 on 30.07.2017.
 */
public class UserDTO {
    private Integer userId;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String phone;
    private String address;
    private String addInfo;
    private Date dateRegistration;
    private Role role;
    private UserStatus userStatus;
    private Set<CartDTO> carts;

    public UserDTO(User user){
        this(newBuilder()
                .userId(user.getUserId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .password(user.getPassword())
                .phone(user.getPhone())
                .address(user.getAddress())
                .addInfo(user.getAddInfo())
                .dateRegistration(user.getDateRegistration())
                .role(user.getRole())
                .userStatus(user.getUserStatus())
                .carts(user.getCarts())
        );
    }

    private UserDTO(Builder builder){
        setUserId(builder.userId);
        setFirstName(builder.firstName);
        setLastName(builder.lastName);
        setEmail(builder.email);
        setPassword(builder.password);
        setPhone(builder.phone);
        setAddress(builder.address);
        setAddInfo(builder.addInfo);
        setDateRegistration(builder.dateRegistration);
        setRole(builder.role);
        setUserStatus(builder.userStatus);
        interalSetCarts(builder.carts);
    }

    public static Builder newBuilder(){
        return new Builder();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddInfo() {
        return addInfo;
    }

    public void setAddInfo(String addInfo) {
        this.addInfo = addInfo;
    }

    public Date getDateRegistration() {
        return dateRegistration;
    }

    public void setDateRegistration(Date dateRegistration) {
        this.dateRegistration = dateRegistration;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public Set<CartDTO> getCarts() {
        return carts;
    }

    public void SetCarts(Set<CartDTO> carts) {
        this.carts = carts;
    }

    private void interalSetCarts(Set<Cart> carts){
        this.carts = new HashSet<>();
        for(Cart element : carts){
            this.carts.add(new CartDTO(element));
        }
    }

    public static final class Builder{
        private Integer userId;
        private String firstName;
        private String lastName;
        private String email;
        private String password;
        private String phone;
        private String address;
        private String addInfo;
        private Date dateRegistration;
        private Role role;
        private UserStatus userStatus;
        private Set<Cart> carts;

        private Builder(){}

        public Builder userId(Integer val){
            userId = val;
            return this;
        }

        public Builder firstName (String val){
            firstName = val;
            return this;
        }

        public Builder lastName (String val){
            lastName = val;
            return this;
        }

        public Builder email (String val){
            email = val;
            return this;
        }

        public Builder password (String val){
            password = val;
            return this;
        }

        public Builder phone (String val){
            phone = val;
            return this;
        }

        public Builder address (String val){
            address = val;
            return this;
        }

        public Builder addInfo (String val){
            addInfo = val;
            return this;
        }

        public Builder dateRegistration (Date val){
            dateRegistration = val;
            return this;
        }

        public Builder role (Role val){
            role = val;
            return this;
        }

        public Builder userStatus (UserStatus val){
            userStatus = val;
            return this;
        }

        public Builder carts (Set<Cart> val){
            carts = val;
            return this;
        }

        public UserDTO build(){
            return new UserDTO(this);
        }
    }
}
