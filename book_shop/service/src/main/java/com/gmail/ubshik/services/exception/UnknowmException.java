package com.gmail.ubshik.services.exception;

/**
 * Created by Acer5740 on 31.07.2017.
 */
public class UnknowmException extends Exception {
    public UnknowmException() {
    }

    public UnknowmException(String message) {
        super("Unknown exception");
    }

    public UnknowmException(String message, Throwable cause) {
        super("Unknown exception", cause);
    }
}
