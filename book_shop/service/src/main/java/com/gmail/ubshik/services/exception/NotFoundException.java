package com.gmail.ubshik.services.exception;

/**
 * Created by Acer5740 on 30.07.2017.
 */
public class NotFoundException extends Exception {
    private static final long serialVersionUID = 1016392299411667635L;

    public NotFoundException() {
    }

    public NotFoundException(String message) {
        super("Your information are incorrect. System didn't find anything");
    }

    public NotFoundException(String message, Throwable cause) {
        super("Your information are incorrect. System didn't find anything", cause);
    }
}
