package com.gmail.ubshik.services.model;

import com.gmail.ubshik.repo.model.News;
import com.gmail.ubshik.repo.model.User;

import java.util.Date;

/**
 * Created by Acer5740 on 30.07.2017.
 */
public class NewsDTO {
    private Integer newsId;
    private User user;
    private String topic;
    private String content;
    private String pathPhoto;
    private Date datePublication;

    public NewsDTO(News news){
        this(
                newBuilder()
                    .newsId(news.getNewsId())
                    .user(news.getUser())
                    .topic(news.getTopic())
                    .content(news.getContent())
                    .pathPhoto(news.getPathPhoto())
                    .datePublication(news.getDatePublication())
        );
    }

    private NewsDTO(Builder builder){
        setNewsId(builder.newsId);
        setUser(builder.user);
        setTopic(builder.topic);
        setContent(builder.content);
        setPathPhoto(builder.pathPhoto);
        setDatePublication(builder.datePublication);
    }

    public static Builder newBuilder(){
        return new Builder();
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPathPhoto() {
        return pathPhoto;
    }

    public void setPathPhoto(String pathPhoto) {
        this.pathPhoto = pathPhoto;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    public static final class Builder{
        private Integer newsId;
        private User user;
        private String topic;
        private String content;
        private String pathPhoto;
        private Date datePublication;

        private Builder(){}

        public Builder newsId(Integer val){
            newsId = val;
            return this;
        }

        public Builder user(User val){
            user = val;
            return this;
        }

        public Builder topic (String val){
            topic = val;
            return this;
        }

        public Builder content (String val){
            content = val;
            return this;
        }

        public Builder pathPhoto(String val){
            pathPhoto = val;
            return this;
        }

        public Builder datePublication (Date val){
            datePublication = val;
            return  this;
        }

        public NewsDTO build(){
            return new NewsDTO(this);
        }
    }
}
