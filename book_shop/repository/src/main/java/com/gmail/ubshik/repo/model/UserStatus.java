package com.gmail.ubshik.repo.model;

/**
 * Created by Acer5740 on 28.07.2017.
 */
public enum UserStatus {
    ACTIVE,
    BLOCKED
}
