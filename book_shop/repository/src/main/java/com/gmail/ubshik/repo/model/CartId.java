package com.gmail.ubshik.repo.model;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Acer5740 on 30.07.2017.
 */
@Embeddable
public class CartId implements Serializable{
    private static final long serialVersionUID = -2580439319973975691L;

    @ManyToOne
    private User user;
    @ManyToOne
    private Book book;

    private CartId(Builder builder){
        user = builder.user;
        book = builder.book;
    }

    public CartId() {
    }

    public static final Builder newBuilder(){
        return new Builder();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public static final class Builder{
        private User user;
        private Book book;

        private Builder(){}

        public Builder user (User val){
            user = val;
            return this;
        }

        public Builder book(Book val){
            book = val;
            return this;
        }

        public CartId build(){
            return new CartId(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartId cartId = (CartId) o;
        return Objects.equals(user, cartId.user) &&
                Objects.equals(book, cartId.book);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, book);
    }

    @Override
    public String toString() {
        return "CartId{" +
                "user=" + user.toString() +
                ", book=" + book.toString() +
                '}';
    }
}
