package com.gmail.ubshik.repo.model;

/**
 * Created by Acer5740 on 30.07.2017.
 */
public enum OrderStatus {
    NEW,
    REVIEWING,
    IN_PROGRESS,
    DELIVERED
}
