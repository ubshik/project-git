package com.gmail.ubshik.repo.model;

import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Acer5740 on 30.07.2017.
 */
@Entity
@Table(name = "T_CART")
@AssociationOverrides({
        @AssociationOverride(name = "pk.user",
                joinColumns = @JoinColumn(name = "F_USER_ID")),
        @AssociationOverride(name = "pk.book",
                joinColumns = @JoinColumn(name = "F_BOOK_ID"))
})
public class Cart implements Serializable {
    private static final long serialVersionUID = -8159568163059338862L;

    @Id
    @Column(name = "F_CART_ID", nullable = false, unique = true, precision=5)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer cartId;
    @Embedded
    private CartId pk = new CartId();
    @Column(name = "F_PRICE", nullable = false, precision = 5, scale = 2)
    private Double price;
    @Column(name = "F_QUANTITY", nullable = false, precision=4)
    private Integer quantity;
    @Column(name = "F_SUM", nullable = false, precision = 8, scale = 2)
    @Formula("F_PRICE * F_QUANTITY")
    private Double sum;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "F_ORDER_ID", nullable = false)
    private Order order;

    private Cart(Builder builder){
        cartId = builder.cartId;
        pk = builder.pk;
        price = builder.price;
        quantity = builder.quantity;
        sum = builder.sum;
        order = builder.order;
    }

    public Cart() {
    }

    public static Builder newBuilder(){
        return new Builder();
    }

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public CartId getPk() {
        return pk;
    }

    public void setPk(CartId pk) {
        this.pk = pk;
    }

    @Transient
    public User getUser(){
        return getPk().getUser();
    }

    public void setUser(User user){
        getPk().setUser(user);
    }

    @Transient
    public Book getBook(){
        return getPk().getBook();
    }

    public void setBook(Book book){
        getPk().setBook(book);
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Double getPrice() {
        return price;
    }

    public Double getBookPrice() {
        return getBook().getPrice();
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public static final class Builder{
        private Integer cartId;
        private CartId pk = new CartId();
        private Double price;
        private Integer quantity;
        private Double sum;
        private Order order;

        private Builder(){}

        public Builder cartId(Integer val){
            cartId = val;
            return this;
        }

        public Builder pk(CartId val){
            pk = val;
            return this;
        }

        public Builder price(Double val){
            price = val;
            return this;
        }

        public Builder quantity(Integer val){
            quantity = val;
            return this;
        }

        public Builder sum(Double val){
            sum = val;
            return this;
        }

        public Builder order(Order val){
            order = val;
            return this;
        }

        public Cart build(){
            return new Cart(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cart cart = (Cart) o;
        return Objects.equals(cartId, cart.cartId) &&
                Objects.equals(pk, cart.pk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cartId, pk);
    }

    @Override
    public String toString() {
        return "Cart{" +
                "cartId=" + cartId +
                ", pk=" + pk.toString() +
                ", price=" + price +
                ", quantity=" + quantity +
                ", sum=" + sum +
                ", order=" + order.toString() +
                '}';
    }
}
