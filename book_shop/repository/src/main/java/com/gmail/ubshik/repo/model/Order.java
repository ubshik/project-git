package com.gmail.ubshik.repo.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Acer5740 on 30.07.2017.
 */
@Entity
@Table(name = "T_ORDERS")
public class Order implements Serializable {
    private static final long serialVersionUID = 4024645063957024574L;

    @Id
    @Column(name = "F_ORDER_ID", nullable = false, unique = true, precision = 5)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer orderId;
    @Column(name = "F_TOTAL_PRICE", nullable = false, precision=8, scale=2)
    private Double totalPrice;
    @Column(name = "F_ORDER_STATUS", nullable = false, columnDefinition="enum('NEW','REVIEWING','IN_PROGRESS','DELIVERED')")
    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
    private Set<Cart> carts = new HashSet<>();

    private Order(Builder builder){
        orderId = builder.orderId;
        totalPrice = builder.totalPrice;
        orderStatus = builder.orderStatus;
        carts = builder.carts;
    }

    public Order() {
    }

    public static Builder newBuilder(){
        return new Builder();
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Set<Cart> getCarts() {
        return carts;
    }

    public void setCarts(Set<Cart> carts) {
        this.carts = carts;
    }

    public static final class Builder{
        private Integer orderId;
        private Double totalPrice;
        private OrderStatus orderStatus;
        private Set<Cart> carts = new HashSet<>();

        private Builder(){}

        public Builder orderId(Integer val){
            orderId = val;
            return this;
        }

        public Builder totalPrice (Double val){
            totalPrice = val;
            return this;
        }

        public Builder orderStatus(OrderStatus val){
            orderStatus = val;
            return this;
        }

        public Builder carts (Set<Cart> val){
            carts = val;
            return this;
        }

        public Order build(){
            return new Order(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(orderId, order.orderId) &&
                Objects.equals(totalPrice, order.totalPrice) &&
                orderStatus == order.orderStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, totalPrice, orderStatus);
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", totalPrice=" + totalPrice +
                ", orderStatus=" + orderStatus +
                ", carts=" + carts.toString() +
                '}';
    }
}
