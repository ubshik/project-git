package com.gmail.ubshik.repo;

import com.gmail.ubshik.repo.model.User;

import java.sql.SQLException;

/**
 * Created by Acer5740 on 30.07.2017.
 */
public interface UserHbnDao extends GenericHbnDao<User, Integer> {
    User getUserByEmail(String email) throws SQLException;
    User getUserByEmailAndPassword(String email, String password);
}
