package com.gmail.ubshik.repo;

import com.gmail.ubshik.repo.model.Book;

/**
 * Created by Acer5740 on 30.07.2017.
 */
public interface BookHbnDao extends GenericHbnDao<Book, Integer> {
}
