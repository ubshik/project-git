package com.gmail.ubshik.repo.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Acer5740 on 28.07.2017.
 */
@Entity
@Table(name = "T_USERS")
public class User implements Serializable {
    private static final long serialVersionUID = 546025422086706800L;

    @Id
    @Column(name = "F_USER_ID", unique = true, nullable = false, precision=5)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;
    @Column(name = "F_FIRST_NAME", nullable = false, length = 50)
    private String firstName;
    @Column(name = "F_LAST_NAME", nullable = false, length = 50)
    private String lastName;
    @Column(name = "F_EMAIL", unique = true, nullable = false, length = 50)
    private String email;
    @Column(name = "F_PASSWORD", nullable = false, length = 15)
    private String password;
    @Column(name = "F_PHONE", nullable = false, length = 11)
    private String phone;
    @Column(name = "F_ADDRESS", nullable = false, length = 50)
    private String address;
    @Column(name = "F_ADD_INFO", length = 100)
    private String addInfo;
    @Column(name = "F_DATE_REGISTRATION", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dateRegistration;
    @Column(name = "F_ROLE", nullable = false, columnDefinition="enum('SUPERADMIN','ADMIN','USER')")
    @Enumerated(EnumType.STRING)
    private Role role;
    @Column(name = "F_USER_STATUS", nullable = false, columnDefinition="enum('ACTIVE','BLOCKED')")
    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.user", cascade = CascadeType.ALL)
    private Set<Cart> carts = new HashSet<>();
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<News> newsSet = new HashSet<>();

    private User(Builder builder){
        userId = builder.userId;
        firstName = builder.firstName;
        lastName = builder.lastName;
        email = builder.email;
        password = builder.password;
        phone = builder.phone;
        address = builder.address;
        addInfo = builder.addInfo;
        dateRegistration = builder.dateRegistration;
        role = builder.role;
        userStatus = builder.userStatus;
        carts = builder.carts;
    }

    public User() {}

    public static Builder newBuilder(){
        return new Builder();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddInfo() {
        return addInfo;
    }

    public void setAddInfo(String addInfo) {
        this.addInfo = addInfo;
    }

    public Date getDateRegistration() {
        return dateRegistration;
    }

    public void setDateRegistration(Date dateRegistration) {
        this.dateRegistration = dateRegistration;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public Set<Cart> getCarts() {
        return carts;
    }

    public void setCarts(Set<Cart> carts) {
        this.carts = carts;
    }

    public static final class Builder{
        private Integer userId;
        private String firstName;
        private String lastName;
        private String email;
        private String password;
        private String phone;
        private String address;
        private String addInfo;
        private Date dateRegistration;
        private Role role;
        private UserStatus userStatus;
        private Set<Cart> carts  = new HashSet<>();

        private Builder(){};

        public Builder userId (Integer val){
            userId = val;
            return this;
        }

        public Builder firstName (String val){
            firstName = val;
            return this;
        }

        public Builder lastName (String val){
            lastName = val;
            return this;
        }

        public Builder email (String val){
            email = val;
            return this;
        }

        public Builder password (String val){
            password = val;
            return this;
        }

        public Builder phone (String val){
            phone = val;
            return this;
        }

        public Builder address (String val){
            address = val;
            return this;
        }

        public Builder addInfo (String val){
            addInfo = val;
            return this;
        }

        public Builder dateRegistration (Date val){
            dateRegistration = val;
            return this;
        }

        public Builder role (Role val){
            role = val;
            return this;
        }

        public Builder userStatus (UserStatus val){
            userStatus = val;
            return this;
        }

        public Builder carts (Set<Cart> val){
            carts = val;
            return this;
        }

        public User build(){
            return new User(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(userId, user.userId) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(email, user.email) &&
                Objects.equals(phone, user.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, firstName, lastName, email, phone);
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", addInfo='" + addInfo + '\'' +
                ", dateRegistration=" + dateRegistration +
                ", role=" + role +
                ", userStatus=" + userStatus +
                '}';
    }
}
