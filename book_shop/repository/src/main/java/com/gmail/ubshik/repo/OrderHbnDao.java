package com.gmail.ubshik.repo;

import com.gmail.ubshik.repo.model.Order;

/**
 * Created by Acer5740 on 30.07.2017.
 */
public interface OrderHbnDao extends GenericHbnDao<Order, Integer> {
}
