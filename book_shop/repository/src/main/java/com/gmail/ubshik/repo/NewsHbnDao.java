package com.gmail.ubshik.repo;

import com.gmail.ubshik.repo.model.News;

/**
 * Created by Acer5740 on 30.07.2017.
 */
public interface NewsHbnDao extends GenericHbnDao<News, Integer> {
}
