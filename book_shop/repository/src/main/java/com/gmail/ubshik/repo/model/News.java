package com.gmail.ubshik.repo.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Acer5740 on 30.07.2017.
 */
@Entity
@Table(name = "T_NEWS")
public class News implements Serializable {
    private static final long serialVersionUID = 7338356036304027623L;

    @Id
    @Column(name = "F_NEWS_ID", nullable = false, unique = true, precision = 5)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer newsId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "F_USER_ID", nullable = false)
    private User user;
    @Column(name = "F_TOPIC", nullable = false, length = 50)
    private String topic;
    @Column(name = "F_CONTENT", nullable = false, columnDefinition="text")
    private String content;
    @Column(name = "F_URI_PHOTO")
    private String pathPhoto;
    @Column(name = "F_DATE_PUBLICATION", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date datePublication;

    private News(Builder builder){
        newsId = builder.newsId;
        user = builder.user;
        topic = builder.topic;
        content = builder.content;
        pathPhoto = builder.pathPhoto;
        datePublication = builder.datePublication;
    }

    public News() {
    }

    public static final Builder newBuilder(){
        return new Builder();
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPathPhoto() {
        return pathPhoto;
    }

    public void setPathPhoto(String pathPhoto) {
        this.pathPhoto = pathPhoto;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    public static final class Builder{
        private Integer newsId;
        private User user;
        private String topic;
        private String content;
        private String pathPhoto;
        private Date datePublication;

        private Builder(){}

        public Builder newsId(Integer val){
            newsId = val;
            return this;
        }

        public Builder user(User val){
            user = val;
            return this;
        }

        public Builder topic (String val){
            topic = val;
            return this;
        }

        public Builder content (String val){
            content = val;
            return this;
        }

        public Builder pathPhoto(String val){
            pathPhoto = val;
            return this;
        }

        public Builder datePublication (Date val){
            datePublication = val;
            return  this;
        }

        public News build(){
            return new News(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        News news = (News) o;
        return Objects.equals(newsId, news.newsId) &&
                Objects.equals(topic, news.topic) &&
                Objects.equals(content, news.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(newsId, topic, content);
    }

    @Override
    public String toString() {
        return "News{" +
                "newsId=" + newsId +
                ", user=" + user.toString() +
                ", topic='" + topic + '\'' +
                ", content='" + content + '\'' +
                ", pathPhoto='" + pathPhoto + '\'' +
                ", datePublication=" + datePublication +
                '}';
    }
}
