package com.gmail.ubshik.repo.impl;

import com.gmail.ubshik.repo.UserHbnDao;
import com.gmail.ubshik.repo.model.User;
import org.apache.log4j.Logger;
import org.hibernate.Query;


/**
 * Created by Acer5740 on 30.07.2017.
 */
public class UserHbnDaoImpl extends GenericHbnDaoImpl<User, Integer> implements UserHbnDao {
    private static final Logger logger = Logger.getLogger(UserHbnDaoImpl.class);

    @Override
    public User getUserByEmail(String email){
        logger.info("Trying to get user by email");
        User user = null;
        Query query = getSession.createQuery(resourceBundle.getString("getUserByEmail"));
        query.setParameter("email", email);
        user = (User) query.uniqueResult();
        if(user == null){
            logger.warn("Don't find user with email " + email);
        }
        return user;
    }

    @Override
    public User getUserByEmailAndPassword(String email, String password) {
        logger.info("Trying to get user by email and password");
        User user = null;
        Query query = getSession.createQuery(resourceBundle.getString("getUserByEmailAndPassword"));
        query.setParameter("email", email);
        query.setParameter("password", password);
        user = (User) query.uniqueResult();
        if(user == null){
            logger.warn("Don't find user with email " + email + " and password " + password);
        }
        return user;
    }
}
