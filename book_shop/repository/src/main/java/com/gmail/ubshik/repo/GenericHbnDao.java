package com.gmail.ubshik.repo;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Acer5740 on 28.07.2017.
 */
public interface GenericHbnDao<T extends Serializable, ID extends Serializable> {
    void setSession(Session session);
    ID save (T entity);
    void saveOrUpdate(T entity);
    T findById(ID id);
    List<T> findAll();
    void delete(T entity);
    void deleteAll();
}
