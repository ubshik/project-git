package com.gmail.ubshik.repo.impl;

import com.gmail.ubshik.repo.GenericHbnDao;
import com.gmail.ubshik.repo.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.ResourceBundle;


/**
 * Created by Acer5740 on 28.07.2017.
 */
public abstract class GenericHbnDaoImpl<T extends Serializable, ID extends Serializable> implements GenericHbnDao<T, ID> {
   private static final Logger logger = Logger.getLogger(GenericHbnDaoImpl.class);
   protected Session getSession = HibernateUtil.getCurrentSession();
   ResourceBundle resourceBundle = ResourceBundle.getBundle("hql");

   private final Class<T> entityClass;

   public GenericHbnDaoImpl(){
       this.entityClass = (Class<T>) ((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
   }

    @Override
    public ID save(T entity) {
        return (ID) getSession.save(entity);
    }

    @Override
    public void setSession(org.hibernate.Session session){
        getSession = session;
    }


    @Override
    public void saveOrUpdate(T entity) {
        getSession.saveOrUpdate(entity);
    }

    @Override
    public T findById(ID id) {
        return (T) getSession.get(this.entityClass, id);
    }

    @Override
    public List<T> findAll() {
        return getSession.createCriteria(this.entityClass).list();
    }

    @Override
    public void delete(T entity) {
        getSession.delete(entity);
    }

    @Override
    public void deleteAll() {
        List<T> entities = findAll();
        for (T entity : entities){
            getSession.delete(entity);
        }
    }
}
