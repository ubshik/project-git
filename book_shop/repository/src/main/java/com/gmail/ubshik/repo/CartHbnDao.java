package com.gmail.ubshik.repo;

import com.gmail.ubshik.repo.model.Cart;

/**
 * Created by Acer5740 on 30.07.2017.
 */
public interface CartHbnDao extends GenericHbnDao<Cart, Integer> {
}
