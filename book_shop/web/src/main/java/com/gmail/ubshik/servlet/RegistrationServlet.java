package com.gmail.ubshik.servlet;

import com.gmail.ubshik.services.UserService;
import com.gmail.ubshik.services.model.UserDTO;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by Acer5740 on 31.07.2017.
 */
public class RegistrationServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(RegistrationServlet.class);
    private final UserService userService = UserService.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/jsp/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Trying to register from servlet RegisterServlet");
        String firstname = req.getParameter("firstname");
        String lastname = req.getParameter("lastname");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String passwordRepeat = req.getParameter("password_repeat");
        String phone = req.getParameter("phone");
        String address = req.getParameter("address");
        String addInfo = req.getParameter("add_info");
        Date date = new Date();
        UserDTO userDTO = null;
        if(!firstname.equals("") && !lastname.equals("")
                && !email.equals("") && !password.equals("")
                && !phone.equals("") && !address.equals("")){
            if(!password.equals(passwordRepeat)){
                logger.warn("Your fields of password are different.");
                req.getSession().setAttribute("error", "Your fields of password are different. Please fill them the same.");
                resp.sendRedirect(req.getContextPath() + "/registration");
            } else{
                try {
                    userDTO = userService.getUserDTOByEmail(email);
                } catch (SQLException e) {
                    logger.warn("Something wrong with get user by email from RegistrationServlet");
                    e.printStackTrace();
                }
                if (userDTO != null){
                    logger.warn("User with your email already exist.");
                    req.getSession().setAttribute("error", "User with your email already exist. Please fill the form again.");
                    resp.sendRedirect(req.getContextPath() + "/registration");
                }else{
                    try{
                        userDTO = userService.createUserDTO(firstname, lastname, email, password, phone, address, addInfo, date);
                        req.getSession().setAttribute("user", userDTO);
                        req.setAttribute("firstname", userDTO.getFirstName());
                        req.getRequestDispatcher("/jsp/user_enter_success.jsp").forward(req, resp);
                    }catch (Exception e) {
                        logger.error("Mistake" + e.getMessage());
                        throw new IllegalStateException("Unknown error");
                    }
                }
            }
        }else{
            logger.warn("Your fields are free.");
            req.getSession().setAttribute("error", "Your fields are free. Please fill in the form.");
            resp.sendRedirect(req.getContextPath() + "/registration");
        }
    }
}
