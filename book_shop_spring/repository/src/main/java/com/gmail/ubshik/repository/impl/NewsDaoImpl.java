package com.gmail.ubshik.repository.impl;

import com.gmail.ubshik.repository.NewsDao;
import com.gmail.ubshik.repository.model.News;
import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
@Repository
public class NewsDaoImpl extends GenericDaoImpl<News, Integer> implements NewsDao {
    private static final Logger logger = Logger.getLogger(NewsDaoImpl.class);

    @Autowired
    Environment environment;

    @Override
    public List<News> findAll() {
        logger.info("Trying to get all news");
        List<News> newsList;
        Query query = getSession().createQuery(environment.getRequiredProperty("News.All"));
        newsList = query.list();
        if(newsList == null || newsList.size() == 0){
            logger.warn("Don't find any news.");
            return null;
        } else {
            return newsList;
        }
    }

    @Override
    public void deleteAll() {
        List<News> newsList = findAll();
        if(newsList != null){
            for (News news : newsList){
                getSession().delete(news);
            }
        }
    }

    @Override
    public List<News> getListNews10Item() {
        logger.info("Trying to get 10 news.");
        List<News> listNews;
        Query query = getSession().createQuery(environment.getRequiredProperty("News.10pieces"));
        query.setMaxResults(10);
        listNews = (List<News>) query.list();
        if(listNews == null || listNews.size() == 0){
            logger.warn("Don't find any news.");
            return null;
        }
        return listNews;
    }
}
