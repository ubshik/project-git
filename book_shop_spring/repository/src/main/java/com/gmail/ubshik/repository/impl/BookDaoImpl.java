package com.gmail.ubshik.repository.impl;

import com.gmail.ubshik.repository.BookDao;
import com.gmail.ubshik.repository.model.Book;
import com.gmail.ubshik.repository.model.Order;
import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
@Repository
public class BookDaoImpl extends GenericDaoImpl<Book, Integer> implements BookDao {
    private static final Logger logger = Logger.getLogger(BookDaoImpl.class);

    @Autowired
    Environment environment;

    @Override
    public List<Book> findAll() {
        logger.info("Trying to get all books");
        List<Book> bookList;
        Query query = getSession().createQuery(environment.getRequiredProperty("Book.All"));
        bookList = query.list();
        if(bookList == null || bookList.size() == 0){
            logger.warn("Don't find any book.");
            return null;
        } else {
            return bookList;
        }
    }

    @Override
    public void deleteAll() {
        List<Book> bookList = findAll();
        if(bookList != null){
            for (Book book : bookList){
                getSession().delete(book);
            }
        }
    }

    @Override
    public List<Book> getBooksToAddInOrder(Order order) {
        logger.info("Trying to get books for order");
        List<Book> bookList;
        Query query = getSession().createQuery(environment.getRequiredProperty("Order.Book.Add"));
        query.setParameter("order", order);
        bookList = query.list();
        if(bookList == null || bookList.size() == 0){
            logger.warn("Don't find any book.");
            return null;
        } else {
            return bookList;
        }
    }

    @Override
    public Book checkBookInOrder(Book book) {
        logger.info("Trying to check book in order");
        Book bookChecked;
        Query query = getSession().createQuery(environment.getRequiredProperty("Book.In.Order"));
        query.setParameter("book", book);
        bookChecked = (Book) query.uniqueResult();
        if(bookChecked == null){
            logger.warn("Don't find book.");
            return null;
        } else {
            return bookChecked;
        }
    }
}
