package com.gmail.ubshik.repository.model;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Lubov Vol on 25.08.2017.
 */
@Entity
@Table(name = "T_IMAGE")
public class Image implements Serializable {
    private static final long serialVersionUID = 6353626037817083138L;

    @Id
    @GeneratedValue
    @Column(name = "F_NEWS_ID", unique = true, nullable = false, precision = 5)
    private Integer newsId;
    @Column(name = "F_IMAGE_NAME")
    private String imageName;
    @Column(name = "F_LOCATION")
    private String location;

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Image image = (Image) o;
        return Objects.equals(newsId, image.newsId) &&
                Objects.equals(imageName, image.imageName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(newsId, imageName);
    }

    @Override
    public String toString() {
        return "Image{" +
                "newsId=" + newsId +
                ", imageName='" + imageName + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
