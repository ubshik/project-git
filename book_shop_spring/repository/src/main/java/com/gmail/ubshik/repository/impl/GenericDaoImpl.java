package com.gmail.ubshik.repository.impl;

import com.gmail.ubshik.repository.GenericDao;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
@SuppressWarnings("unchecked")
@Repository
public abstract class GenericDaoImpl<T extends Serializable, ID extends Serializable> implements GenericDao<T, ID> {
    private static final Logger logger = Logger.getLogger(GenericDaoImpl.class);
    @Autowired
    private SessionFactory sessionFactory;

    private final Class<T> entityClass;

    protected Session getSession(){
        return this.sessionFactory.getCurrentSession();
    }

    public GenericDaoImpl(){
        this.entityClass = (Class<T>) ((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public ID save(T entity) {
        return (ID) getSession().save(entity);
    }

    @Override
    public void update(T entity) {
        getSession().update(entity);
    }

    @Override
    public T findById(ID id) {
        return (T) getSession().get(this.entityClass, id);
    }

    @Override
    public void delete(T entity) {
        getSession().delete(entity);
    }

}
