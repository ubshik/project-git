package com.gmail.ubshik.repository.impl;

import com.gmail.ubshik.repository.ImageDao;
import com.gmail.ubshik.repository.model.Image;
import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
@Repository
public class ImageDaoImpl extends GenericDaoImpl<Image, Integer> implements ImageDao {
    private static final Logger logger = Logger.getLogger(ImageDaoImpl.class);

    @Autowired
    Environment environment;

    @Override
    public List<Image> findAll() {
        logger.info("Trying to get all images");
        List<Image> imageList;
        Query query = getSession().createQuery(environment.getRequiredProperty("Image.All"));
        imageList = query.list();
        if (imageList == null || imageList.size() == 0) {
            logger.warn("Don't find any image.");
            return null;
        } else {
            return imageList;

        }
    }

    @Override
    public void deleteAll() {
        List<Image> imageList = findAll();
        if(imageList != null){
            for (Image image : imageList){
                getSession().delete(image);
            }
        }
    }
}
