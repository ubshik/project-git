package com.gmail.ubshik.repository.impl;

import com.gmail.ubshik.repository.UserDao;
import com.gmail.ubshik.repository.model.User;
import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
@Repository
public class UserDaoImpl extends GenericDaoImpl<User, Integer> implements UserDao {
    private static final Logger logger = Logger.getLogger(UserDaoImpl.class);

    @Autowired
    Environment environment;

    @Override
    public List<User> findAll() {
        logger.info("Trying to get all users");
        List<User> userList;
        Query query = getSession().createQuery(environment.getRequiredProperty("User.All"));
        userList = query.list();
        if(userList == null || userList.size() == 0){
            logger.warn("Don't find any user.");
            return null;
        } else {
            return userList;
        }
    }

    @Override
    public void deleteAll() {
        List<User> newsList = findAll();
        if(newsList != null){
            for (User user : newsList){
                getSession().delete(user);
            }
        }
    }

    @Override
    public User getUserByEmail(String email) {
        logger.info("Trying to get user by email");
        User user;
        Query query = getSession().createQuery(environment.getRequiredProperty("User.ByEmail"));
        query.setParameter("email", email);
        user = (User) query.uniqueResult();
        if(user == null){
            logger.warn("Don't find user with email " + email);
        }
        return user;
    }

    @Override
    public User getUserByEmailAndPassword(String email, String password) {
        logger.info(org.hibernate.Version.getVersionString());
        logger.info("Trying to get user by email and password");
        User user;
        Query query = getSession().createQuery(environment.getRequiredProperty("User.ByEmail.And.Password"));
        query.setParameter("email", email);
        query.setParameter("password", password);
        user = (User) query.uniqueResult();
        if(user == null){
            logger.warn("Don't find user with email " + email + " and password " + password);
        }
        return user;
    }
}
