package com.gmail.ubshik.repository;

import com.gmail.ubshik.repository.model.Image;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
public interface ImageDao extends GenericDao<Image, Integer> {
}
