package com.gmail.ubshik.repository.impl;

import com.gmail.ubshik.repository.FeedBackDao;
import com.gmail.ubshik.repository.model.FeedBack;
import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
@Repository
public class FeedBackDaoImpl extends GenericDaoImpl<FeedBack, Integer> implements FeedBackDao {
    private static final Logger logger = Logger.getLogger(FeedBackDaoImpl.class);

    @Autowired
    Environment environment;

    @Override
    public List<FeedBack> findAll() {
        logger.info("Trying to get all feedback.");
        List<FeedBack> feedBacks;
        Query query = getSession().createQuery(environment.getRequiredProperty("FeedBack.All"));
        feedBacks = query.list();
        if(feedBacks == null || feedBacks.size() == 0){
            logger.warn("Don't find any feedback.");
            return null;
        } else {
            return feedBacks;
        }
    }

    @Override
    public void deleteAll() {
        List<FeedBack> feedBacks = findAll();
        if(feedBacks != null){
            for (FeedBack feedBack : feedBacks){
                getSession().delete(feedBack);
            }
        }
    }

    @Override
    public List<FeedBack> getFeedBackWithoutAnswer() {
        logger.info("Trying to get feedback without answer.");
        List<FeedBack> feedBacks;
        Query query = getSession().createQuery(environment.getRequiredProperty("FeedBack.WithoutAnswer"));
        feedBacks = query.list();
        if(feedBacks == null || feedBacks.size() == 0){
            logger.warn("Don't find any feedback.");
            return null;
        } else {
            return feedBacks;
        }
    }
}
