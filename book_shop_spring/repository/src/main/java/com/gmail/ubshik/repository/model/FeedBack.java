package com.gmail.ubshik.repository.model;

import com.gmail.ubshik.repository.model.enums.IsAnswered;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Lubov Vol on 31.08.2017.
 */
@Entity
@Table(name = "T_FEEDBACK")
public class FeedBack implements Serializable {
    private static final long serialVersionUID = 5317176162145851250L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_FEEDBACK_ID", nullable = false, precision = 5)
    private Integer feedbackId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "F_USER_ID", nullable = false)
    private User user;
    @Column(name = "F_TOPIC", nullable = false, length = 50)
    private String topic;
    @Column(name = "F_CONTENT", nullable = false)
    private String content;
    @Column(name = "F_EMAIL", nullable = false, length = 50)
    private String email;
    @Column(name = "F_DATE_CREATION", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dateCreation;
    @Column(name = "F_IS_ANSWERED", nullable = false, columnDefinition="enum('YES','NO')")
    @Enumerated(EnumType.STRING)
    private IsAnswered answer;

    public Integer getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(Integer feedbackId) {
        this.feedbackId = feedbackId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public IsAnswered getAnswer() {
        return answer;
    }

    public void setAnswer(IsAnswered answer) {
        this.answer = answer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FeedBack feedBack = (FeedBack) o;
        return Objects.equals(feedbackId, feedBack.feedbackId) &&
                Objects.equals(topic, feedBack.topic) &&
                Objects.equals(dateCreation, feedBack.dateCreation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(feedbackId, topic, dateCreation);
    }

    @Override
    public String toString() {
        return "FeedBack{" +
                "feedbackId=" + feedbackId +
                ", user=" + user.toStringShort() +
                ", topic='" + topic + '\'' +
                ", content='" + content + '\'' +
                ", email='" + email + '\'' +
                ", dateCreation=" + dateCreation +
                ", answer=" + answer +
                '}';
    }
}
