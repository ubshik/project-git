package com.gmail.ubshik.repository;

import com.gmail.ubshik.repository.model.User;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
public interface UserDao extends GenericDao<User, Integer> {
    User getUserByEmail(String email);
    User getUserByEmailAndPassword(String email, String password);
}
