package com.gmail.ubshik.repository.impl;

import com.gmail.ubshik.repository.CartDao;
import com.gmail.ubshik.repository.model.Book;
import com.gmail.ubshik.repository.model.Cart;
import com.gmail.ubshik.repository.model.Order;
import com.gmail.ubshik.repository.model.User;
import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
@Repository
public class CartDaoImpl extends GenericDaoImpl<Cart, Integer> implements CartDao {
    private static final Logger logger = Logger.getLogger(CartDaoImpl.class);

    @Autowired
    Environment environment;

    @Override
    public List<Cart> findAll() {
        logger.info("Trying to get all users");
        List<Cart> cartList;
        Query query = getSession().createQuery(environment.getRequiredProperty("Cart.All"));
        cartList = query.list();
        if(cartList == null || cartList.size() == 0){
            logger.warn("Don't find any cart.");
            return null;
        } else {
            return cartList;
        }
    }

    @Override
    public void deleteAll() {
        List<Cart> cartList = findAll();
        if(cartList != null){
            for (Cart cart : cartList){
                getSession().delete(cart);
            }
        }
    }

    @Override
    public Cart getCartByUserAndBook(User user, Book book) {
        logger.info("Trying to get cart by user and book");
        Cart cart;
        Query query = getSession().createQuery(environment.getRequiredProperty("Cart.ByUser.And.Book"));
        query.setParameter("user", user);
        query.setParameter("book", book);
        cart = (Cart)query.uniqueResult();
        if(cart == null){
            return null;
        } else {
            return cart;
        }
    }

    @Override
    public List<Cart> findCartByUser(User user) {
        logger.info("Trying to get list of cart by user id");
        List<Cart> listCart;
        Query query = getSession().createQuery(environment.getRequiredProperty("Cart.ByUser"));
        query.setParameter("user", user);
        listCart = query.list();
        if(listCart == null || listCart.size() == 0){
            logger.warn("User " + user + " haven't record in cart.");
            return null;
        } else{
            return listCart;
        }
    }

    @Override
    public List<Cart> getCartWithoutOrder() {
        logger.info("Trying to get list of cart without order");
        List<Cart> listCart;
        Query query = getSession().createQuery(environment.getRequiredProperty("Cart.WithoutOrder"));
        listCart = query.list();
        if(listCart == null || listCart.size() == 0){
            logger.warn("Don't find any cart.");
            return null;
        } else {
            return listCart;
        }
    }

    @Override
    public List<Cart> getCartWithOrder(Order order) {
        logger.info("Trying to get list of cart with order");
        List<Cart> listCart;
        Query query = getSession().createQuery(environment.getRequiredProperty("Cart.WithOrder"));
        query.setParameter("order", order);
        listCart = query.list();
        if(listCart == null || listCart.size() == 0){
            logger.warn("Don't find any cart in order.");
            return null;
        } else {
            return listCart;
        }
    }

    @Override
    public Double getSumCart(User user) {
        logger.info("Trying to get sum of cart by user");
        Double sum;
        Query query = getSession().createQuery(environment.getRequiredProperty("Cart.Sum"));
        query.setParameter("user", user);
        sum = (Double) query.uniqueResult();
        if(sum == null || sum == 0){
            logger.warn("User " + user + " haven't record in cart.");
            return 0D;
        } else{
            return sum;
        }
    }

    @Override
    public Double getSumOrder(Order order) {
        logger.info("Trying to get sum of order");
        Double sum;
        Query query = getSession().createQuery(environment.getRequiredProperty("Order.Sum"));
        query.setParameter("order", order);
        sum = (Double) query.uniqueResult();
        if(sum == null || sum == 0){
            return 0D;
        } else{
            return sum;
        }
    }
}
