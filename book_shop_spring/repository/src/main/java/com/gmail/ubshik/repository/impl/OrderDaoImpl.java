package com.gmail.ubshik.repository.impl;

import com.gmail.ubshik.repository.OrderDao;
import com.gmail.ubshik.repository.model.Order;
import com.gmail.ubshik.repository.model.User;
import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
@Repository
public class OrderDaoImpl extends GenericDaoImpl<Order, Integer> implements OrderDao {
    private static final Logger logger = Logger.getLogger(OrderDaoImpl.class);

    @Autowired
    Environment environment;

    @Override
    public List<Order> findAll() {
        logger.info("Trying to get all books");
        List<Order> orderList;
        Query query = getSession().createQuery(environment.getRequiredProperty("Order.All"));
        orderList = query.list();
        if(orderList == null || orderList.size() == 0){
            logger.warn("Don't find any book.");
            return null;
        } else {
            return orderList;
        }
    }

    @Override
    public void deleteAll() {
        List<Order> orderList = findAll();
        if(orderList != null){
            for (Order order : orderList){
                getSession().delete(order);
            }
        }
    }

    @Override
    public List<Order> findListOrderByUserId(User user) {
        logger.info("Trying to get list of cart by user id");
        List<Order> listOrder;
        Query query = getSession().createQuery(environment.getRequiredProperty("Order.ByUser"));
        query.setParameter("user", user);
        listOrder = query.list();
        if(listOrder == null || listOrder.size() == 0){
            logger.warn("User " + user.getEmail() + " haven't order in cart.");
            return null;
        } else {
            return listOrder;
        }
    }
}
