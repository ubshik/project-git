package com.gmail.ubshik.repository;

import com.gmail.ubshik.repository.model.Book;
import com.gmail.ubshik.repository.model.Cart;
import com.gmail.ubshik.repository.model.Order;
import com.gmail.ubshik.repository.model.User;

import java.util.List;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
public interface CartDao extends GenericDao<Cart, Integer> {
    List<Cart> findCartByUser(User user);
    Cart getCartByUserAndBook(User user, Book book);
    List<Cart> getCartWithoutOrder();
    Double getSumCart(User user);
    List<Cart> getCartWithOrder(Order order);
    Double getSumOrder(Order order);
}
