package com.gmail.ubshik.repository;

import com.gmail.ubshik.repository.model.FeedBack;

import java.util.List;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
public interface FeedBackDao extends GenericDao<FeedBack, Integer> {
    public List<FeedBack> getFeedBackWithoutAnswer();
}
