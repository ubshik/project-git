package com.gmail.ubshik.repository.model.enums;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
public enum Role {
    SUPERADMIN,
    ADMIN,
    USER
}
