package com.gmail.ubshik.repository.model;

import com.gmail.ubshik.repository.model.enums.OrderStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
@Entity
@Table(name = "T_ORDER")
public class Order implements Serializable {
    private static final long serialVersionUID = -4581437562868414504L;

    @Id
    @Column(name = "F_ORDER_ID", nullable = false, unique = true, precision = 5)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer orderId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "F_USER_ID", nullable = false)
    private User user;
    @Column(name = "F_TOTAL_PRICE", nullable = false, precision=8, scale=2)
    private Double totalPrice;
    @Column(name = "F_ORDER_STATUS", nullable = false, columnDefinition="enum('NEW','REVIEWING','IN_PROGRESS','DELIVERED')")
    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order", cascade = CascadeType.ALL)
    private Set<Cart> carts = new HashSet<>();

    public Order() {
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Set<Cart> getCarts() {
        return carts;
    }

    public void setCarts(Set<Cart> carts) {
        this.carts = carts;
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, totalPrice, orderStatus);
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", user=" + user.toStringShort() +
                ", totalPrice=" + totalPrice +
                ", orderStatus=" + orderStatus +
                ", carts=" + carts.toString() +
                '}';
    }

    public String toStringShort() {
        return "Order{" +
                "orderId=" + orderId +
                ", user=" + user.toStringShort() +
                ", totalPrice=" + totalPrice +
                ", orderStatus=" + orderStatus +
                '}';
    }
}
