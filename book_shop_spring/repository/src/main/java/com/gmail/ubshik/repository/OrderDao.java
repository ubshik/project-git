package com.gmail.ubshik.repository;

import com.gmail.ubshik.repository.model.Order;
import com.gmail.ubshik.repository.model.User;

import java.util.List;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
public interface OrderDao extends GenericDao<Order, Integer> {
    List<Order> findListOrderByUserId(User user);
}
