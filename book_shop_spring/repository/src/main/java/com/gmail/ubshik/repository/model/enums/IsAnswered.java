package com.gmail.ubshik.repository.model.enums;

/**
 * Created by Lubov Vol on 31.08.2017.
 */
public enum IsAnswered {
    YES,
    NO
}
