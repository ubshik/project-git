package com.gmail.ubshik.repository.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
@Entity
@Table(name = "T_NEWS")
public class News implements Serializable {
    private static final long serialVersionUID = 7082339624541533935L;

    @Id
    @Column(name = "F_NEWS_ID", nullable = false, unique = true, precision = 5)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer newsId;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "F_USER_ID", nullable = false)
    private User user;
    @Column(name = "F_TOPIC", nullable = false, length = 50)
    private String topic;
    @Column(name = "F_CONTENT", nullable = false, columnDefinition="text")
    private String content;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "F_NEWS_ID")
    private Image image;
    @Column(name = "F_DATE_PUBLICATION", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date datePublication;

    public News() {
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        News news = (News) o;
        return Objects.equals(newsId, news.newsId) &&
                Objects.equals(topic, news.topic) &&
                Objects.equals(content, news.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(newsId, topic, content);
    }

    @Override
    public String toString() {
        return "News{" +
                "newsId=" + newsId +
                ", user=" + user.toStringShort() +
                ", topic='" + topic + '\'' +
                ", content='" + content + '\'' +
                ", image=" + image.toString() +
                ", datePublication=" + datePublication +
                '}';
    }
}
