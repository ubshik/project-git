package com.gmail.ubshik.repository;

import com.gmail.ubshik.repository.model.News;

import java.util.List;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
public interface NewsDao extends GenericDao<News, Integer> {
    public List<News> getListNews10Item();
}
