package com.gmail.ubshik.repository.model;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
@Embeddable
public class CartId implements Serializable {
    private static final long serialVersionUID = 5596147983687720245L;

    @ManyToOne
    private User user;
    @ManyToOne
    private Book book;

    public CartId() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartId cartId = (CartId) o;
        return Objects.equals(user, cartId.user) &&
                Objects.equals(book, cartId.book);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, book);
    }

    @Override
    public String toString() {
        return "CartId{" +
                "user=" + user.toStringShort() +
                ", book=" + book.toStringShort() +
                '}';
    }
}
