package com.gmail.ubshik.repository;

import com.gmail.ubshik.repository.model.Book;
import com.gmail.ubshik.repository.model.Order;

import java.util.List;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
public interface BookDao extends GenericDao<Book, Integer> {
    public List<Book> getBooksToAddInOrder(Order order);
    public Book checkBookInOrder(Book book);
}
