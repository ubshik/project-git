package com.gmail.ubshik.service.model.util;

import com.gmail.ubshik.repository.BookDao;
import com.gmail.ubshik.repository.OrderDao;
import com.gmail.ubshik.repository.UserDao;
import com.gmail.ubshik.repository.model.*;
import com.gmail.ubshik.service.model.*;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by Lubov Vol on 08.08.2017.
 */
public class Converter {

    private static UserDao userDao;
    private static BookDao bookDao;
    private static OrderDao orderDao;

    public static void setUserDao(UserDao userDao){
        Converter.userDao = userDao;
    }

    public static void setBookDao(BookDao bookDao){
        Converter.bookDao = bookDao;
    }

    public static void setOrderDao(OrderDao orderDao){
        Converter.orderDao = orderDao;
    }


    public static User convertToUser(UserDTO userDTO){
        User user = new User();
        user.setUserId(userDTO.getUserId());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setPhone(userDTO.getPhone());
        user.setAddress(userDTO.getAddress());
        user.setAddInfo(userDTO.getAddInfo());
        user.setDateRegistration(userDTO.getDateRegistration());
        user.setRole(userDTO.getRole());
        user.setUserStatus(userDTO.getUserStatus());
        return user;
    }

    public static Book convertToBook(BookDTO bookDTO){
        Book book = new Book();
        book.setBookId(bookDTO.getBookId());
        book.setTitle(bookDTO.getTitle());
        book.setAuthor(bookDTO.getAuthor());
        book.setInventoryNumber(bookDTO.getInventoryNumber());
        book.setDescription(bookDTO.getDescription());
        book.setPrice(bookDTO.getPrice());
        book.setDateCreation(bookDTO.getDateCreation());
        return book;
    }

    public static Order convertToOrder(OrderDTO orderDTO){
        if(orderDTO != null){
            Order order = new Order();
            order.setOrderId(orderDTO.getOrderId());
            order.setUser(userDao.findById(orderDTO.getUserId()));
            order.setTotalPrice(orderDTO.getTotalPrice());
            order.setOrderStatus(orderDTO.getOrderStatus());
            return order;
        }else {
            return null;
        }
    };

    public static News convertToNews(NewsDTO newsDTO){
        News news = new News();
        news.setNewsId(newsDTO.getNewsId());
        news.setUser(userDao.findById(getPrincipal().getUserId()));
        news.setTopic(newsDTO.getTopic());
        news.setContent(newsDTO.getContent());
        news.setDatePublication(newsDTO.getDatePublication());
        return news;
    }

    private static CartId convertToCartId(CartDTO cartDTO){
        CartId cartId = new CartId();
        cartId.setUser(userDao.findById(cartDTO.getUserId()));
        cartId.setBook(bookDao.findById(cartDTO.getBookId()));
        return cartId;
    }

    public static Cart convertToCart(CartDTO cartDTO){
        Cart cart = new Cart();
        cart.setCartId(cartDTO.getCartId());
        cart.setPk(convertToCartId(cartDTO));
        cart.setPrice(cartDTO.getPrice());
        cart.setQuantity(cartDTO.getQuantity());
        cart.setSum(cartDTO.getSum());
        cart.setOrder(cartDTO.getOrderId() != null ? orderDao.findById(cartDTO.getOrderId()) : null);
        return cart;
    }

    public static UserDTO convertToUserDTO(AppUserPrincipal appUserPrincipal){
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId(appUserPrincipal.getUserId());
        userDTO.setFirstName(appUserPrincipal.getFirstName());
        userDTO.setLastName(appUserPrincipal.getLastName());
        userDTO.setPassword(null);
        userDTO.setPassword_2(null);
        userDTO.setPhone(appUserPrincipal.getPhone());
        userDTO.setAddress(appUserPrincipal.getAddress());
        userDTO.setAddInfo(appUserPrincipal.getAddInfo());
        return userDTO;
    }

    public static FeedBack convertToFeedBack(FeedBackDTO feedBackDTO){
        FeedBack feedBack = new FeedBack();
        feedBack.setFeedbackId(feedBackDTO.getFeedbackId());
        feedBack.setUser(userDao.findById(getPrincipal().getUserId()));
        feedBack.setTopic(feedBackDTO.getTopic());
        feedBack.setContent(feedBackDTO.getContent());
        feedBack.setEmail(feedBackDTO.getEmail());
        feedBack.setDateCreation(feedBackDTO.getDatePublication());
        feedBack.setAnswer(feedBackDTO.getAnswer());
        return feedBack;
    }

    private static AppUserPrincipal getPrincipal(){
        return (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
    }
}
