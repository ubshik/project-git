package com.gmail.ubshik.service;

import java.io.File;

/**
 * Created by Lubov Vol on 12.08.2017.
 */
public interface IImageService {
    public File getFileById(Integer newsId);
}
