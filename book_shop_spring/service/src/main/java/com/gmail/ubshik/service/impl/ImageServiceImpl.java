package com.gmail.ubshik.service.impl;

import com.gmail.ubshik.repository.ImageDao;
import com.gmail.ubshik.repository.model.Image;
import com.gmail.ubshik.service.IImageService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;

/**
 * Created by Lubov Vol on 12.08.2017.
 */
@Service
public class ImageServiceImpl implements IImageService {
    private static final Logger logger = Logger.getLogger(ImageServiceImpl.class);

    @Autowired
    private ImageDao imageDao;

    public ImageServiceImpl(ImageDao imageDao){
        this.imageDao = imageDao;
    }

    @Override
    @Transactional
    public File getFileById(Integer newsId) {
        Image image = imageDao.findById(newsId);
        return new File(image.getLocation());
    }
}
