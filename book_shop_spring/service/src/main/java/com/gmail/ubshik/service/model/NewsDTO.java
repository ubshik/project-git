package com.gmail.ubshik.service.model;

import com.gmail.ubshik.repository.ImageDao;
import com.gmail.ubshik.repository.model.News;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
public class NewsDTO {
    private Integer newsId;
    private Integer userId;
    @NotEmpty
    @Size(min = 3, max = 50)
    private String topic;
    @NotEmpty
    private String content;
    @NotEmpty
    private MultipartFile image;
    private Date datePublication;

    @Autowired
    private ImageDao imageDao;

    public NewsDTO() {
    }

    public NewsDTO(News news){
        this(
                newBuilder()
                        .newsId(news.getNewsId())
                        .userId(news.getUser().getUserId())
                        .topic(news.getTopic())
                        .content(news.getContent())
                        .datePublication(news.getDatePublication())
        );
    }

    private NewsDTO(Builder builder){
        setNewsId(builder.newsId);
        setUserId(builder.userId);
        setTopic(builder.topic);
        setContent(builder.content);
        setImage(builder.image);
        setDatePublication(builder.datePublication);
    }

    public static Builder newBuilder(){
        return new Builder();
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    public static final class Builder{
        private Integer newsId;
        private Integer userId;
        private String topic;
        private String content;
        private MultipartFile image;;
        private Date datePublication;

        private Builder(){}

        public Builder newsId(Integer val){
            newsId = val;
            return this;
        }

        public Builder userId(Integer val){
            userId = val;
            return this;
        }

        public Builder topic (String val){
            topic = val;
            return this;
        }

        public Builder content (String val){
            content = val;
            return this;
        }

        public Builder image(MultipartFile val){
            image = val;
            return this;
        }

        public Builder datePublication (Date val){
            datePublication = val;
            return  this;
        }

        public NewsDTO build(){
            return new NewsDTO(this);
        }
    }
}
