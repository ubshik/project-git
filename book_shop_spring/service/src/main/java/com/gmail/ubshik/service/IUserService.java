package com.gmail.ubshik.service;

import com.gmail.ubshik.service.model.UserDTO;

import java.util.List;

/**
 * Created by Lubov Vol on 08.08.2017.
 */
public interface IUserService {
    public UserDTO getUserDTOByEmailAndPassword(UserDTO userDTO);
    public UserDTO getUserDTOByEmail(String email);
    public Integer createUserDTO(UserDTO userDTO);
    public void updateUserDateByUser(UserDTO userDTO);
    public void updateUserPasswordByUser(UserDTO userDTO);
    public List<UserDTO> getAll();
    public void deleteUser(Integer userId);
    public UserDTO findUserById(Integer id);
    public void updateUserDateByAdmin(UserDTO userDTO);
}
