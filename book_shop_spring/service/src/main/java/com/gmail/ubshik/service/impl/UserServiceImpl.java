package com.gmail.ubshik.service.impl;

import com.gmail.ubshik.repository.UserDao;
import com.gmail.ubshik.repository.model.User;
import com.gmail.ubshik.repository.model.enums.Role;
import com.gmail.ubshik.repository.model.enums.UserStatus;
import com.gmail.ubshik.service.IUserService;
import com.gmail.ubshik.service.model.UserDTO;
import com.gmail.ubshik.service.model.util.Converter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
@Service
public class UserServiceImpl implements IUserService {
    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private final UserDao userDao;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserServiceImpl(UserDao userDao, BCryptPasswordEncoder bCryptPasswordEncoder){
        this.userDao = userDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        Converter.setUserDao(userDao);
    }

    @Override
    @Transactional
    public UserDTO getUserDTOByEmailAndPassword(UserDTO userDTO) {
        logger.info("Trying to get user by email and password.");
        User user = userDao.getUserByEmailAndPassword(userDTO.getEmail(), userDTO.getPassword());
        if(user != null){
            userDTO = new UserDTO(user);
        }else {
            return null;
        }
        return userDTO;
    }

    @Override
    @Transactional
    public UserDTO getUserDTOByEmail(String email) {
        logger.info("Trying to get user by email.");
        User user = userDao.getUserByEmail(email);
        UserDTO userDTO;
        if(user != null){
            userDTO = new UserDTO(user);
        } else {
            return null;
        }
        return userDTO;
    }

    @Override
    @Transactional
    public Integer createUserDTO(UserDTO userDTO){
        logger.info("Trying to create user from UserServiceImpl.");
        userDTO.setAddInfo(userDTO.getAddInfo() == "" || userDTO.getAddInfo()== null ? null : userDTO.getAddInfo());
        userDTO.setDateRegistration(new Date());
        userDTO.setRole(Role.USER);
        userDTO.setUserStatus(UserStatus.ACTIVE);
        User user = Converter.convertToUser(userDTO);
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        Integer count = userDao.save(user);
        logger.info("User is saved with id " + count);
        return count;
    }

    @Override
    @Transactional
    public void updateUserDateByUser(UserDTO userDTO) {
        logger.info("Trying to update user date by user from UserServiceImpl.");
        User user = userDao.findById(userDTO.getUserId());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setPhone(userDTO.getPhone());
        user.setAddress(userDTO.getAddress());
        user.setAddInfo(userDTO.getAddInfo());
        userDao.update(user);
    }

    @Override
    @Transactional
    public void updateUserPasswordByUser(UserDTO userDTO) {
        logger.info("Trying to update user password by user from UserServiceImpl.");
        User user = userDao.findById(userDTO.getUserId());
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        userDao.update(user);
    }

    @Override
    @Transactional
    public List<UserDTO> getAll() {
        logger.info("Trying to get all users from UserServiceImpl.");
        List<UserDTO> userDTOList = new ArrayList<>();
        List<User> userList = userDao.findAll();
        if(userList == null){
            return null;
        } else {
            for (User user : userList){
                UserDTO userDTO = new UserDTO(user);
                userDTOList.add(userDTO);
            }
            return userDTOList;
        }
    }

    @Override
    @Transactional
    public void deleteUser(Integer userId) {
        logger.info("Trying to delete user by userId from UserServiceImpl.");
        userDao.delete(userDao.findById(userId));
    }

    @Override
    @Transactional
    public UserDTO findUserById(Integer userId) {
        logger.info("Trying to get user by userId from UserServiceImpl.");
        User user = userDao.findById(userId);
        UserDTO userDTO = new UserDTO(user);
        return userDTO;
    }

    @Override
    @Transactional
    public void updateUserDateByAdmin(UserDTO userDTO) {
        logger.info("Trying to update user date by admin from UserServiceImpl.");
        User user = userDao.findById(userDTO.getUserId());
        user.setRole(userDTO.getRole());
        user.setUserStatus(userDTO.getUserStatus());
        userDao.update(user);
    }
}
