package com.gmail.ubshik.service.impl;

import com.gmail.ubshik.repository.FeedBackDao;
import com.gmail.ubshik.repository.UserDao;
import com.gmail.ubshik.repository.model.FeedBack;
import com.gmail.ubshik.repository.model.User;
import com.gmail.ubshik.repository.model.enums.IsAnswered;
import com.gmail.ubshik.service.IFeedBackService;
import com.gmail.ubshik.service.model.AppUserPrincipal;
import com.gmail.ubshik.service.model.FeedBackDTO;
import com.gmail.ubshik.service.model.util.Converter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Lubov Vol on 02.08.2017.
 */
@Service
public class FeedBackImpl implements IFeedBackService {
    private static final Logger logger = Logger.getLogger(FeedBackImpl.class);

    private final FeedBackDao feedBackDao;
    private final UserDao userDao;


    @Autowired
    public FeedBackImpl(FeedBackDao feedBackDao, UserDao userDao){
        this.feedBackDao = feedBackDao;
        this.userDao = userDao;
    }

    @Override
    @Transactional
    public Integer createFeedBack(FeedBackDTO feedBackDTO) {
        logger.info("Trying to save feedback from FeedBackService.");
        FeedBack feedBack = Converter.convertToFeedBack(feedBackDTO);
        feedBack.setDateCreation(new Date());
        feedBack.setAnswer(IsAnswered.NO);
        User user = getUser();
        feedBack.setUser(user);
        user.getFeedBacks().add(feedBack);
        return feedBackDao.save(feedBack);
    }

    @Override
    @Transactional
    public List<FeedBackDTO> getFeedBackWithoutAnswer() {
        logger.info("Trying to get feedback without answer from FeedBackService.");
        List<FeedBack> feedBacks = feedBackDao.getFeedBackWithoutAnswer();
        List<FeedBackDTO> feedBackDTOS;
        if(feedBacks == null || feedBacks.size() == 0){
            return null;
        } else {
            feedBackDTOS = convertToDTO(feedBacks);
            return feedBackDTOS;
        }
    }

    @Override
    @Transactional
    public List<FeedBackDTO> getAllFeedBack() {
        logger.info("Trying to get all feedback from FeedBackService.");
        List<FeedBack> feedBacks = feedBackDao.findAll();
        List<FeedBackDTO> feedBackDTOS;
        if(feedBacks == null || feedBacks.size() == 0){
            return null;
        } else {
            feedBackDTOS = convertToDTO(feedBacks);
            return feedBackDTOS;
        }
    }

    @Override
    @Transactional
    public FeedBackDTO getFeedBackById(Integer feedbackId) {
        logger.info("Trying to get feedback by feedbackId from FeedBackService.");
        FeedBack feedBack = feedBackDao.findById(feedbackId);
        if(feedBack == null){
            return null;
        } else {
            FeedBackDTO feedBackDTO = new FeedBackDTO(feedBack);
            return feedBackDTO;
        }
    }

    @Override
    @Transactional
    public void updateStatusFeedBack(FeedBackDTO feedBackDTO) {
        logger.info("Trying to update feedback status from FeedBackService.");
        FeedBack feedBack = feedBackDao.findById(feedBackDTO.getFeedbackId());
        feedBack.setAnswer(feedBackDTO.getAnswer());
        feedBackDao.update(feedBack);
    }

    private User getUser() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return userDao.findById(principal.getUserId());
    }

    private List<FeedBackDTO> convertToDTO(List<FeedBack> feedBacks){
        List<FeedBackDTO> feedBackDTOS = new ArrayList<>();
        for (FeedBack feedBack : feedBacks) {
            FeedBackDTO feedBackDTO = new FeedBackDTO(feedBack);
            feedBackDTOS.add(feedBackDTO);
        }
        return feedBackDTOS;
    }
}
