package com.gmail.ubshik.service.model.util;

import java.util.ArrayList;

/**
 * Created by Lubov Vol on 31.08.2017.
 */
public class IntegerListWrapper {
    private ArrayList<Integer> integers;

    public ArrayList<Integer> getIntegers() {
        return integers;
    }

    public void setIntegers(ArrayList<Integer> integers) {
        this.integers = integers;
    }
}
