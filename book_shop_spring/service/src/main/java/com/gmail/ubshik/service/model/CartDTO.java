package com.gmail.ubshik.service.model;

import com.gmail.ubshik.repository.model.*;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
public class CartDTO {
    private Integer cartId;
    private Integer userId;
    private Integer bookId;
    private String title;
    private String author;
    private Double price;
    private Integer quantity;
    private Double sum;
    private Integer orderId;

    public CartDTO() {
    }

    public CartDTO(Cart cart){
        this(
                newBuilder()
                        .cartId(cart.getCartId())
                        .userId(cart.getPk().getUser().getUserId())
                        .bookId(cart.getPk().getBook().getBookId())
                        .title(cart.getPk().getBook().getTitle())
                        .author(cart.getPk().getBook().getAuthor())
                        .price(cart.getPrice())
                        .quantity(cart.getQuantity())
                        .sum(cart.getSum())
                        .orderId(cart.getOrder() != null ? cart.getOrder().getOrderId(): null)
        );
    }

    private CartDTO(Builder builder){
        setCartId(builder.cartId);
        setUserId(builder.userId);
        setBookId(builder.bookId);
        setTitle(builder.title);
        setAuthor(builder.author);
        setPrice(builder.price);
        setQuantity(builder.quantity);
        setSum(builder.sum);
        setOrderId(builder.orderId);
    }

    public static Builder newBuilder(){
        return new Builder();
    }

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public static final class Builder{
        private Integer cartId;
        private Integer userId;
        private Integer bookId;
        private String title;
        private String author;
        private Double price;
        private Integer quantity;
        private Double sum;
        private Integer orderId;

        private Builder(){}

        public Builder cartId(Integer val){
            cartId = val;
            return this;
        }

        public Builder userId(Integer val){
            userId = val;
            return this;
        }

        public Builder bookId(Integer val){
            bookId = val;
            return this;
        }

        public Builder title(String val){
            title = val;
            return this;
        }

        public Builder author(String val){
            author = val;
            return this;
        }

        public Builder price(Double val){
            price = val;
            return this;
        }

        public Builder quantity(Integer val){
            quantity = val;
            return this;
        }

        public Builder sum(Double val){
            sum = val;
            return this;
        }

        public Builder orderId(Integer val){
            orderId = val;
            return this;
        }

        public CartDTO build(){
            return new CartDTO(this);
        }
    }
}
