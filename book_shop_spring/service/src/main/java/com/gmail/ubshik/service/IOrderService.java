package com.gmail.ubshik.service;

import com.gmail.ubshik.service.model.CartDTO;
import com.gmail.ubshik.service.model.OrderDTO;

import java.util.List;

/**
 * Created by Lubov Vol on 15.08.2017.
 */
public interface IOrderService {
    public List<OrderDTO> getAllOrderByUserId();
    public Integer createOrder(List<Integer> list);
    public OrderDTO getOrderById(Integer orderId);
    public List<OrderDTO> getAllOrder();
    public void updateOrderStatus(OrderDTO orderDTO);
    public void updateQuantityInOrder(Integer orderId, CartDTO cartDTO);
    public void deleteBookInOrder(Integer orderId, Integer cartId);
    public void addBookToOrder(Integer orderId, Integer bookId);
}
