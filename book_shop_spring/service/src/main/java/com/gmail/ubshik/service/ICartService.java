package com.gmail.ubshik.service;

import com.gmail.ubshik.service.model.CartDTO;

import java.util.List;

/**
 * Created by Lubov Vol on 12.08.2017.
 */
public interface ICartService {
    public void createCart(Integer bookId);
    public List<CartDTO> getCartByUser();
    public CartDTO getCardDTOByUserAndBook(Integer bookId);
    public CartDTO getCartDTOById(Integer id);
    public List<CartDTO> getCart();
    public List<CartDTO> getCartWithoutOrder();
    public void deleteCart(Integer cartId);
    public CartDTO getCartByCartId(Integer cartId);
    public void updateQuantity(CartDTO cartDTO);
    public Double sumOfCart();
    public List<CartDTO> getCartWithOrder(Integer orderId);
    public Double sumOfOrder(Integer orderId);

}
