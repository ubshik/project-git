package com.gmail.ubshik.service;

import com.gmail.ubshik.service.model.BookDTO;

import java.util.List;

/**
 * Created by Lubov Vol on 12.08.2017.
 */
public interface IBookService {
    public Integer createBookDTO(BookDTO bookDTO);
    public List<BookDTO> getAllBook();
    public BookDTO getBookById(Integer id);
    public void deleteBookById(Integer id);
    public void updateBook(BookDTO bookDTO);
    public Integer copyBook(Integer bookId);
    public List<BookDTO> getBookForOrder(Integer orderId);
    public BookDTO checkBookInOrder(Integer bookId);
}
