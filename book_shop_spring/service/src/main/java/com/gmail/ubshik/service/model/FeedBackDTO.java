package com.gmail.ubshik.service.model;

import com.gmail.ubshik.repository.model.FeedBack;
import com.gmail.ubshik.repository.model.enums.IsAnswered;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
public class FeedBackDTO {
    private Integer feedbackId;
    private Integer userId;
    @NotEmpty
    @Size(min = 3, max = 50)
    private String topic;
    @NotEmpty
    private String content;
    @NotEmpty
    @Email
    private String email;
    private Date datePublication;
    private IsAnswered answer;

    public FeedBackDTO() {
    }

    public FeedBackDTO(FeedBack feedBack){
        this(
                newBuilder()
                        .feedbackId(feedBack.getFeedbackId())
                        .userId(feedBack.getUser().getUserId())
                        .topic(feedBack.getTopic())
                        .content(feedBack.getContent())
                        .email(feedBack.getEmail())
                        .datePublication(feedBack.getDateCreation())
                        .answer(feedBack.getAnswer())
        );
    }

    private FeedBackDTO(Builder builder){
        setFeedbackId(builder.feedbackId);
        setUserId(builder.userId);
        setTopic(builder.topic);
        setContent(builder.content);
        setEmail(builder.email);
        setDatePublication(builder.datePublication);
        setAnswer(builder.answer);
    }

    public static Builder newBuilder(){
        return new Builder();
    }

    public Integer getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(Integer feedbackId) {
        this.feedbackId = feedbackId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    public IsAnswered getAnswer() {
        return answer;
    }

    public void setAnswer(IsAnswered answer) {
        this.answer = answer;
    }

    public static final class Builder{
        private Integer feedbackId;
        private Integer userId;
        private String topic;
        private String content;
        private String email;
        private Date datePublication;
        private IsAnswered answer;

        private Builder(){}

        public Builder feedbackId(Integer val){
            feedbackId = val;
            return this;
        }

        public Builder userId(Integer val){
            userId = val;
            return this;
        }

        public Builder topic (String val){
            topic = val;
            return this;
        }

        public Builder content (String val){
            content = val;
            return this;
        }

        public Builder email (String val){
            email = val;
            return this;
        }

        public Builder datePublication (Date val){
            datePublication = val;
            return  this;
        }

        public Builder answer (IsAnswered val){
            answer = val;
            return  this;
        }

        public FeedBackDTO build(){
            return new FeedBackDTO(this);
        }
    }
}
