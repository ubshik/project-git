package com.gmail.ubshik.service;

import com.gmail.ubshik.service.model.NewsDTO;

import java.io.IOException;
import java.util.List;

/**
 * Created by Lubov Vol on 16.08.2017.
 */
public interface INewsService {
    public List<NewsDTO> get10News();
    public Integer createNews(NewsDTO newsDTO) throws IOException;
    public void deleteNews(Integer newsId);
    public NewsDTO getNewsById(Integer newsId);
    public void updateNews(NewsDTO newsDTO);
    public void updateNewsImage(NewsDTO newsDTO) throws IOException;
}
