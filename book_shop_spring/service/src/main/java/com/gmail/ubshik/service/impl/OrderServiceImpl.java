package com.gmail.ubshik.service.impl;

import com.gmail.ubshik.repository.BookDao;
import com.gmail.ubshik.repository.CartDao;
import com.gmail.ubshik.repository.OrderDao;
import com.gmail.ubshik.repository.UserDao;
import com.gmail.ubshik.repository.model.Book;
import com.gmail.ubshik.repository.model.Cart;
import com.gmail.ubshik.repository.model.Order;
import com.gmail.ubshik.repository.model.User;
import com.gmail.ubshik.repository.model.enums.OrderStatus;
import com.gmail.ubshik.service.IOrderService;
import com.gmail.ubshik.service.model.*;
import com.gmail.ubshik.service.model.util.Converter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lubov Vol on 15.08.2017.
 */
@Service
public class OrderServiceImpl implements IOrderService {
    private static final Logger logger = Logger.getLogger(OrderServiceImpl.class);
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private CartDao cartDao;
    @Autowired
    private BookDao bookDao;

    public OrderServiceImpl(OrderDao orderDao, UserDao userDao, CartDao cartDao, BookDao bookDao){
        this.orderDao = orderDao;
        this.userDao = userDao;
        this.cartDao = cartDao;
        this.bookDao = bookDao;
        Converter.setOrderDao(orderDao);
    }

    @Override
    @Transactional
    public List<OrderDTO> getAllOrder() {
        logger.info("Trying to get all orders from OrderService.");
        List<Order> orders = orderDao.findAll();
        List<OrderDTO> orderDTOS;
        if(orders == null || orders.size() == 0 ){
            return null;
        }else {
            orderDTOS = convertToDTO(orders);
            return orderDTOS;
        }
    }

    @Override
    @Transactional
    public List<OrderDTO> getAllOrderByUserId() {
        logger.info("Trying to get all orders by user from OrderService.");
        User user = getUser();
        List<Order> orders = orderDao.findListOrderByUserId(user);
        List<OrderDTO> orderDTOS;
        if(orders == null || orders.size() == 0 ){
            return null;
        }else {
            orderDTOS = convertToDTO(orders);
            return orderDTOS;
        }
    }

    @Override
    @Transactional
    public Integer createOrder(List<Integer> list) {
        logger.info("Trying to create new order from OrderServiceImpl.");
        Order order = new Order();
        order.setOrderStatus(OrderStatus.NEW);
        User user = getUser();
        order.setUser(user);
        List<Cart> carts;
        Double sum = 0D;
        for (Integer integer : list){
            Cart cart = cartDao.findById(integer);
            sum += cart.getSum();
            cart.setOrder(order);
            order.getCarts().add(cart);
        }
        order.setTotalPrice(sum);
        user.getOrders().add(order);
        Integer count = orderDao.save(order);
        logger.info("Order is saved with id " + count);
        return count;
    }

    @Override
    @Transactional
    public OrderDTO getOrderById(Integer orderId) {
        logger.info("Trying to get order by id from OrderService.");
        Order order = orderDao.findById(orderId);
        if(order != null){
            OrderDTO orderDTO = new OrderDTO(order);
            return orderDTO;
        }else {
            return null;
        }
    }

    @Override
    @Transactional
    public void updateOrderStatus(OrderDTO orderDTO) {
        logger.info("Trying to change order status from OrderService.");
        Order order = orderDao.findById(orderDTO.getOrderId());
        order.setOrderStatus(orderDTO.getOrderStatus());
        orderDao.update(order);
    }

    @Override
    @Transactional
    public void updateQuantityInOrder(Integer orderId, CartDTO cartDTO) {
        logger.info("Trying to change quantity in order from OrderService.");
        Order order = orderDao.findById(orderId);
        Cart cart = cartDao.findById(cartDTO.getCartId());
        order.getCarts().remove(cart);
        Double sum = 0D;
        for (Cart cartInOrder : order.getCarts()){
            sum+=cartInOrder.getSum();
        }
        Integer quantity = cartDTO.getQuantity();
        sum += quantity * cart.getPrice();
        cart.setQuantity(quantity);
        cartDao.update(cart);
        order.setTotalPrice(sum);
        order.getCarts().add(cart);
        orderDao.update(order);
    }

    @Override
    @Transactional
    public void deleteBookInOrder(Integer orderId, Integer cartId) {
        logger.info("Trying to delete book in order from OrderService.");
        Order order = orderDao.findById(orderId);
        Cart cart = cartDao.findById(cartId);
        order.getCarts().remove(cart);
        Double sum = 0D;
        for (Cart cartInOrder : order.getCarts()){
            sum+=cartInOrder.getSum();
        }
        order.setTotalPrice(sum);
        cart.setOrder(null);
        orderDao.update(order);
        cartDao.update(cart);
    }

    @Override
    @Transactional
    public void addBookToOrder(Integer orderId, Integer bookId) {
        Book book = bookDao.findById(bookId);
        CartDTO cartDTO = new CartDTO();
        User user = getUser();
        cartDTO.setUserId(user.getUserId());
        cartDTO.setBookId(bookId);
        cartDTO.setTitle(book.getTitle());
        cartDTO.setAuthor(book.getAuthor());
        Double price = book.getPrice();
        cartDTO.setPrice(price);
        Integer quantity = 1;
        cartDTO.setQuantity(quantity);
        cartDTO.setSum(price * quantity);
        Cart cart = Converter.convertToCart(cartDTO);
        Order order = orderDao.findById(orderId);
        Double sum = 0D;
        for (Cart cartInOrder : order.getCarts()){
            sum+=cartInOrder.getSum();
        }
        sum += cart.getSum();
        order.setTotalPrice(sum);
        order.setOrderStatus(OrderStatus.NEW);
        cart.setOrder(order);
        order.getCarts().add(cart);
        user.getCarts().add(cart);
        orderDao.save(order);
        cartDao.save(cart);
    }

    private User getUser() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return userDao.findById(principal.getUserId());
    }

    private List<OrderDTO> convertToDTO(List<Order> orders){
        List<OrderDTO> orderDTOS = new ArrayList<>();
        for (Order order : orders) {
            OrderDTO orderDTO = new OrderDTO(order);
            orderDTOS.add(orderDTO);
        }
        return orderDTOS;
    }
}
