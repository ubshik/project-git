package com.gmail.ubshik.service.impl;

import com.gmail.ubshik.repository.ImageDao;
import com.gmail.ubshik.repository.NewsDao;
import com.gmail.ubshik.repository.UserDao;
import com.gmail.ubshik.repository.model.Image;
import com.gmail.ubshik.repository.model.News;
import com.gmail.ubshik.repository.model.User;
import com.gmail.ubshik.service.INewsService;
import com.gmail.ubshik.service.model.AppUserPrincipal;
import com.gmail.ubshik.service.model.NewsDTO;
import com.gmail.ubshik.service.model.util.Converter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Lubov Vol on 16.08.2017.
 */
@Service
public class NewsServiceImpl implements INewsService {
    private static final Logger logger = Logger.getLogger(NewsServiceImpl.class);

    @Autowired
    private NewsDao newsDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private ImageDao imageDao;

    @Autowired
    Environment environment;

    @Override
    @Transactional
    public List<NewsDTO> get10News() {
        logger.info("Trying to get all news from NewsService.");
        List<News> listNews = newsDao.getListNews10Item();
        List<NewsDTO> listNewsDTO = new ArrayList<>();
        if(listNews == null || listNews.size() == 0 ){
            return null;
        }else {
            for (News news : listNews){
                NewsDTO newsDTO = new NewsDTO(news);
                listNewsDTO.add(newsDTO);
            }
            return listNewsDTO;
        }
    }

    @Override
    @Transactional
    public Integer createNews(NewsDTO newsDTO) throws IOException {
        logger.info("Trying to create news from NewsService.");
        News news = Converter.convertToNews(newsDTO);
        User user = getUser();
        newsDTO.setUserId(user.getUserId());
        Integer count;
        try{
            Image image = getImage(newsDTO);
            news.setImage(image);
            news.setUser(user);
            news.setDatePublication(new Date());
            user.getNewsSet().add(news);
            count = newsDao.save(news);
            logger.info("News is saved with id " + count);
        }catch (IOException e){
            logger.error(e.getMessage(), e);
            throw e;
        }
        return count;
    }

    @Override
    @Transactional
    public void deleteNews(Integer newsId) {
        logger.info("Trying to delete news from NewsService.");
        News news = newsDao.findById(newsId);
        User user = getUser();
        user.getNewsSet().remove(news);
        Image image = imageDao.findById(newsId);
        File file = new File(image.getLocation());
        file.delete();
        newsDao.delete(news);
    }

    @Override
    @Transactional
    public void updateNews(NewsDTO newsDTO) {
        logger.info("Trying to update news from NewsService.");
        News news = newsDao.findById(newsDTO.getNewsId());
        news.setTopic(newsDTO.getTopic());
        news.setContent(newsDTO.getContent());
        newsDao.update(news);
    }

    @Override
    @Transactional
    public void updateNewsImage(NewsDTO newsDTO) throws IOException {
        logger.info("Trying to update news from NewsService.");
        User user = getUser();
        newsDTO.setUserId(user.getUserId());
        Image imageFromDB = imageDao.findById(newsDTO.getNewsId());
        File file = new File(imageFromDB.getLocation());
        file.delete();
        try{
            Image image = getImage(newsDTO);
            imageFromDB.setImageName(image.getImageName());
            imageFromDB.setLocation(image.getLocation());
            imageDao.update(imageFromDB);
            logger.info("Image is updated");
        }catch (IOException e){
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    @Transactional
    public NewsDTO getNewsById(Integer newsId) {
        logger.info("Trying to get news by id from NewsService.");
        News news = newsDao.findById(newsId);
        if(news == null){
            return null;
        } else {
            NewsDTO newsDTO = new NewsDTO(news);
            return newsDTO;
        }
    }

    private User getUser() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return userDao.findById(principal.getUserId());
    }

    private Image getImage(NewsDTO newsDTO) throws IOException{
        DateFormat df = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
        Date today = Calendar.getInstance().getTime();
        String reportDate = df.format(today);
        String imageName = reportDate + "_user_id_" + newsDTO.getUserId();
        String imageLocation = environment.getRequiredProperty("upload.location") + imageName + ".jpg";
        FileCopyUtils.copy(newsDTO.getImage().getBytes(), new File(imageLocation));
        Image image = new Image();
        image.setImageName(imageName);
        image.setLocation(imageLocation);
        return image;
    }
}
