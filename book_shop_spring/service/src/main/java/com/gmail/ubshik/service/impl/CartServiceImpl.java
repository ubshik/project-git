package com.gmail.ubshik.service.impl;

import com.gmail.ubshik.repository.BookDao;
import com.gmail.ubshik.repository.CartDao;
import com.gmail.ubshik.repository.OrderDao;
import com.gmail.ubshik.repository.UserDao;
import com.gmail.ubshik.repository.model.Book;
import com.gmail.ubshik.repository.model.Cart;
import com.gmail.ubshik.repository.model.Order;
import com.gmail.ubshik.repository.model.User;
import com.gmail.ubshik.service.ICartService;
import com.gmail.ubshik.service.model.AppUserPrincipal;
import com.gmail.ubshik.service.model.CartDTO;
import com.gmail.ubshik.service.model.util.Converter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Lubov Vol on 12.08.2017.
 */
@Service
public class CartServiceImpl implements ICartService {
    private static final Logger logger = Logger.getLogger(CartServiceImpl.class);

    @Autowired
    private CartDao cartDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private BookDao bookDao;
    @Autowired
    private OrderDao orderDao;

    @Override
    @Transactional
    public List<CartDTO> getCart() {
        logger.info("Trying to get all cart from CartService.");
        List<Cart> carts = cartDao.findAll();
        List<CartDTO> cartDTOS;
        if(carts == null || carts.size() == 0){
            return null;
        } else {
            cartDTOS = convertToDTO(carts);
            return cartDTOS;
        }
    }

    @Override
    @Transactional
    public List<CartDTO> getCartWithoutOrder() {
        logger.info("Trying to get cart without order from CartService.");
        List<Cart> carts = cartDao.getCartWithoutOrder();
        List<CartDTO> cartDTOS;
        if(carts == null || carts.size() == 0){
            return null;
        } else {
            cartDTOS = convertToDTO(carts);
            return cartDTOS;
        }
    }

    @Override
    @Transactional
    public List<CartDTO> getCartWithOrder(Integer orderId) {
        logger.info("Trying to get cart with order from CartService.");
        Order order = orderDao.findById(orderId);
        List<Cart> carts = cartDao.getCartWithOrder(order);
        List<CartDTO> cartDTOS;
        if(carts == null || carts.size() == 0){
            return null;
        } else {
            cartDTOS = convertToDTO(carts);
            return cartDTOS;
        }
    }

    @Override
    @Transactional
    public List<CartDTO> getCartByUser() {
        logger.info("Trying to get all record by user id from CartService.");
        List<Cart> carts = cartDao.findCartByUser(getUser());
        List<CartDTO> cartDTOS;
        if(carts == null || carts.size() == 0){
            return null;
        } else {
            cartDTOS = convertToDTO(carts);
            return cartDTOS;
        }
    }

    @Override
    @Transactional
    public void createCart(Integer bookId) {
        logger.info("Trying to create new record in cart from CartServiceImpl.");
        Integer quantity = 1;
        User user = getUser();
        Book book = bookDao.findById(bookId);
        Double price = book.getPrice();
        CartDTO cartDTO = new CartDTO();
        cartDTO.setUserId(user.getUserId());
        cartDTO.setBookId(bookId);
        cartDTO.setTitle(book.getTitle());
        cartDTO.setAuthor(book.getAuthor());
        cartDTO.setPrice(price);
        cartDTO.setQuantity(quantity);
        cartDTO.setSum(quantity * price);
        Cart cart = Converter.convertToCart(cartDTO);
        user.getCarts().add(cart);
        book.getCarts().add(cart);
        Integer count = cartDao.save(cart);
        logger.info("Record in cart is saved with id " + count);
    }

    @Override
    @Transactional
    public CartDTO getCardDTOByUserAndBook(Integer bookId) {
        logger.info("Trying to get cartDTO by user and book.");
        User user = getUser();
        Book book = bookDao.findById(bookId);
        Cart cart = cartDao.getCartByUserAndBook(user, book);
        CartDTO cartDTO;
        if(cart != null){
            cartDTO = new CartDTO(cart);
            return cartDTO;
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public CartDTO getCartDTOById(Integer id) {
        logger.info("Trying to get cartDTO by cartId.");
        Cart cart = cartDao.findById(id);
        CartDTO cartDTO = null;
        if(cart != null){
            cartDTO = new CartDTO(cart);
            return cartDTO;
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public void deleteCart(Integer cartId) {
        logger.info("Trying to delete cart by cartId.");
        Cart cart = cartDao.findById(cartId);
        cartDao.delete(cart);
    }

    @Override
    @Transactional
    public CartDTO getCartByCartId(Integer cartId) {
        logger.info("Trying to get cart by cartId from CartService.");
        Cart cart = cartDao.findById(cartId);
        if(cart != null){
            CartDTO cartDTO = new CartDTO(cart);
            return cartDTO;
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public void updateQuantity(CartDTO cartDTO) {
        logger.info("Trying to change quantity from CartService.");
        Cart cart = cartDao.findById(cartDTO.getCartId());
        Integer quantity = cartDTO.getQuantity();
        cart.setQuantity(quantity);
        Double price = cart.getPrice();
        cart.setSum(quantity*price);
        cartDao.update(cart);
    }

    @Override
    @Transactional
    public Double sumOfCart() {
        User user = getUser();
        Double sum = cartDao.getSumCart(user);
        return sum;
    }

    @Override
    @Transactional
    public Double sumOfOrder(Integer orderId) {
        Order order = orderDao.findById(orderId);
        Double sum = cartDao.getSumOrder(order);
        return sum;
    }

    private User getUser() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return userDao.findById(principal.getUserId());
    }

    private List<CartDTO> convertToDTO(List<Cart> carts){
        List<CartDTO> cartDTOS = new ArrayList<>();
        for (Cart cart : carts) {
            CartDTO cartDTO = new CartDTO(cart);
            cartDTOS.add(cartDTO);
        }
        return cartDTOS;
    }
}

