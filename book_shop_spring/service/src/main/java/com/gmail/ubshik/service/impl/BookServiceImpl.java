package com.gmail.ubshik.service.impl;

import com.gmail.ubshik.repository.BookDao;
import com.gmail.ubshik.repository.OrderDao;
import com.gmail.ubshik.repository.model.Book;
import com.gmail.ubshik.repository.model.Order;
import com.gmail.ubshik.service.IBookService;
import com.gmail.ubshik.service.model.BookDTO;
import com.gmail.ubshik.service.model.util.Converter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Lubov Vol on 12.08.2017.
 */
@Service
public class BookServiceImpl implements IBookService {
    private static final Logger logger = Logger.getLogger(BookServiceImpl.class);

    @Autowired
    private BookDao bookDao;
    @Autowired
    private OrderDao orderDao;

    public BookServiceImpl(BookDao bookDao, OrderDao orderDao){
        this.bookDao = bookDao;
        this.orderDao = orderDao;
        Converter.setBookDao(bookDao);
        Converter.setOrderDao(orderDao);
    }


    @Override
    @Transactional
    public List<BookDTO> getAllBook() {
        logger.info("Trying to get all book from BookService.");
        List<Book> listBook = bookDao.findAll();
        List<BookDTO> listBookDTO = new ArrayList<>();
        if(listBook != null){
            for (Book book : listBook){
                BookDTO bookDTO = new BookDTO(book);
                listBookDTO.add(bookDTO);
            }
        }else {
            return null;
        }
        return listBookDTO;
    }

    @Override
    @Transactional
    public Integer createBookDTO(BookDTO bookDTO) {
        logger.info("Trying to create book from BookService.");
        bookDTO.setDateCreation(new Date());
        Book book = Converter.convertToBook(bookDTO);
        Integer count = bookDao.save(book);
        logger.info("Book is saved with id " + count);
        return count;
    }

    @Override
    @Transactional
    public BookDTO getBookById(Integer id) {
        logger.info("Trying to get book by ID from BookService.");
        Book book = bookDao.findById(id);
        BookDTO bookDTO = null;
        if(book != null){
            bookDTO = new BookDTO(book);
        }else {
            return null;
        }
        return bookDTO;
    }

    @Override
    @Transactional
    public void deleteBookById(Integer id) {
        logger.info("Trying to delete book by ID from BookService.");
        Book book = bookDao.findById(id);
        bookDao.delete(book);
    }

    @Override
    @Transactional
    public void updateBook(BookDTO bookDTO) {
        logger.info("Trying to update book by ID from BookService.");//TODO check connect with order
        Book book = bookDao.findById(bookDTO.getBookId());
        book.setTitle(bookDTO.getTitle());
        book.setAuthor(bookDTO.getAuthor());
        book.setInventoryNumber(bookDTO.getInventoryNumber());
        book.setDescription(bookDTO.getDescription());
        book.setPrice(bookDTO.getPrice());
        bookDao.update(book);
    }

    @Override
    @Transactional
    public Integer copyBook(Integer bookId) {
        Book book = bookDao.findById(bookId);
        BookDTO bookDTO = BookDTO.newBuilder().title(book.getTitle())
                .author(book.getAuthor()).inventoryNumber(book.getInventoryNumber())
                .description(book.getDescription()).price(book.getPrice())
                .build();
        bookDTO.setDateCreation(new Date());
        Integer count = bookDao.save(Converter.convertToBook(bookDTO));
        return count;
    }

    @Override
    @Transactional
    public List<BookDTO> getBookForOrder(Integer orderId) {
        Order order = orderDao.findById(orderId);
        List<Book> books = bookDao.getBooksToAddInOrder(order);
        List<BookDTO> bookDTOS = new ArrayList<>();
        if(books == null || books.size() == 0){
            return null;
        }else {
            for (Book book : books){
                BookDTO bookDTO = new BookDTO(book);
                bookDTOS.add(bookDTO);
            }
            return bookDTOS;
        }
    }

    @Override
    @Transactional
    public BookDTO checkBookInOrder(Integer bookId) {
        Book book = bookDao.findById(bookId);
        Book bookChecked = bookDao.checkBookInOrder(book);
        if(bookChecked == null){
            return null;
        }else {
            BookDTO bookDTO = new BookDTO(bookChecked);
            return bookDTO;
        }
    }
}
