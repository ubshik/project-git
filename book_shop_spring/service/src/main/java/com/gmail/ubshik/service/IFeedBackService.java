package com.gmail.ubshik.service;

import com.gmail.ubshik.service.model.FeedBackDTO;

import java.util.List;

/**
 * Created by Lubov Vol on 08.08.2017.
 */
public interface IFeedBackService {
    public Integer createFeedBack(FeedBackDTO feedBackDTO);
    public List<FeedBackDTO> getFeedBackWithoutAnswer();
    public List<FeedBackDTO> getAllFeedBack();
    public FeedBackDTO getFeedBackById(Integer feedbackId);
    public void updateStatusFeedBack(FeedBackDTO feedBackDTO);
}
