package com.gmail.ubshik.service.model;

import com.gmail.ubshik.repository.model.Order;
import com.gmail.ubshik.repository.model.enums.OrderStatus;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
public class OrderDTO {
    private Integer orderId;
    private Integer userId;
    private Double totalPrice;
    private OrderStatus orderStatus;

    public OrderDTO() {
    }

    public OrderDTO(Order order){
        this(
                newBuilder()
                        .orderId(order.getOrderId())
                        .userId(order.getUser().getUserId())
                        .totalPrice(order.getTotalPrice())
                        .orderStatus(order.getOrderStatus())
        );
    }

    private OrderDTO(Builder builder){
        setOrderId(builder.orderId);
        setUserId(builder.userId);
        setTotalPrice(builder.totalPrice);
        setOrderStatus(builder.orderStatus);
    }

    public static Builder newBuilder(){
        return new Builder();
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public static final class Builder{
        private Integer orderId;
        private Integer userId;
        private Double totalPrice;
        private OrderStatus orderStatus;

        private Builder(){}

        public Builder orderId(Integer val){
            orderId = val;
            return this;
        }

        public Builder userId(Integer val){
            userId = val;
            return this;
        }

        public Builder totalPrice (Double val){
            totalPrice = val;
            return this;
        }

        public Builder orderStatus(OrderStatus val){
            orderStatus = val;
            return this;
        }

        public OrderDTO build(){
            return new OrderDTO(this);
        }
    }
}
