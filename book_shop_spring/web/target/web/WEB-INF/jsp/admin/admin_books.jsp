<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<
<!DOCTYPE html >
<html lang="en">
<head>
    <title>Books</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <sec:authorize access="hasAuthority('SUPERADMIN')" >
        <jsp:include page="include_superadmin.jsp"/>
    </sec:authorize>

    <sec:authorize access="hasAuthority('ADMIN')" >
        <jsp:include page="include_admin.jsp"/>
    </sec:authorize>

    <jsp:include page="include_success_error.jsp"/>



    <div class="row">
        <div class="col-md-11">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>Book id</th>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Inventory number</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Date creation</th>
                    <th>Update</th>
                    <th>Copy</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="books" items="${books}">
                    <tr>
                        <td><c:out value="${books.bookId}"/></td>
                        <td><c:out value="${books.title}"/></td>
                        <td><c:out value="${books.author}"/></td>
                        <td><c:out value="${books.inventoryNumber}"/></td>
                        <td><c:out value="${books.description}"/></td>
                        <td><c:out value="${books.price}"/></td>
                        <td><c:out value="${books.dateCreation}"/></td>
                        <td><a href ="/admin/books/update/${books.bookId}">Update</a></td>
                        <td><a href ="/admin/books/copy/${books.bookId}">Copy</a></td>
                        <td><a href ="/admin/books/delete/${books.bookId}">Delete</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="left">
                <button type="button" class="btn-default"><a href ="/admin/books/add">Add book</a></button>
            </div>
        </div>
    </div>

</div>
</body>
</html>