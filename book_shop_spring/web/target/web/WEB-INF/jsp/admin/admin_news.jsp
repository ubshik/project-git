<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<
<!DOCTYPE html >
<html lang="en">
<head>
    <title>News</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <sec:authorize access="hasAuthority('SUPERADMIN')" >
        <jsp:include page="include_superadmin.jsp"/>
    </sec:authorize>

    <sec:authorize access="hasAuthority('ADMIN')" >
        <jsp:include page="include_admin.jsp"/>
    </sec:authorize>

    <jsp:include page="include_success_error.jsp"/>

    <div class="row">
        <div class="col-md-11">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>News id</th>
                    <th>Date</th>
                    <th>Topic</th>
                    <th>Image</th>
                    <th>Content</th>
                    <th>Update</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="news" items="${news}">
                    <tr>
                        <th><c:out value="${news.newsId}"/></th>
                        <th><c:out value="${news.datePublication}"/></th>
                        <td><c:out value="${news.topic}"/></td>
                        <td><img src="/admin/news/download/<c:out value="${news.newsId}"/>" height="42" width="42"></td>
                        <td><c:out value="${news.content}"/></td>
                        <td><a href ="/admin/news/update/${news.newsId}">Update</a></td>
                        <td><a href ="/admin/news/delete/${news.newsId}">Delete</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="right">
                <button class="btn-info"><a href ="/admin/news/add">Add news</a></button>
            </div>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
    </div>

</div>
</body>
</html>