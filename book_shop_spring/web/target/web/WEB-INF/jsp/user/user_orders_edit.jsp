<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<
<!DOCTYPE html >
<html lang="en">
<head>
    <title>Edit order</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <jsp:include page="include_user.jsp"/>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="text-center">
                <h2>Edit order!</h2></div>
        </div>
        <div class="col-md-4"></div>
    </div>

    <jsp:include page="include_success_error.jsp"/>

    <div class="row">
        <div class="col-md-10">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>Serial number</th>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Sum</th>
                    <th>Change quantity</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="cart" items="${cart}" varStatus="loop">
                    <tr>
                        <td>${loop.index + 1}</td>
                        <td><c:out value="${cart.title}"/></td>
                        <td><c:out value="${cart.author}"/></td>
                        <td><c:out value="${cart.price}"/></td>
                        <td><c:out value="${cart.quantity}"/></td>
                        <td><c:out value="${cart.sum}"/></td>
                        <td><a href ="/user/order/${orderId}/cart/${cart.cartId}/quantity">Update</a></td>
                        <td><a href ="/user/order/${orderId}/cart/${cart.cartId}/delete">Delete</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="left">
                <button type="button" class="btn-link"><a href ="/user/order/${orderId}/book/add">Add book</a></button>
            </div>
        </div>
        <div class="col-md-4"/>
        <div class="col-md-4">
            <div class="left">
                <h3>Total sum ${sum}</h3>
            </div>
        </div>
    </div>
</div>
</body>
</html>