<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html >
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4" >
            <div class="bg-success">
                <div class="text-center">
                    <h2><c:out value="${success}"/></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4" >
            <div class="bg-danger">
                <div class="text-center">
                    <h2><c:out value="${error}"/></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
