<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html >
<html>
<head>
    <title>Registration</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid">

    <div class="row">
        <div class="col-md-10">
            <table class="table table-view">
                <thead>
                <tr>
                    <th></th>
                    <th><a href ="/login">LOG IN</a></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4" class="text-center"><h2>Fill up form to your registration!</h2></div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form:form method="post" modelAttribute="user" action="/registration">
                <div class="form-group">
                    <label for="firstName">First name</label>
                    <p class="bg-danger"><form:errors path="firstName"/></p>
                    <form:input id="firstName" cssClass="form-control" path="firstName" type="text"/>
                </div>
                <div class="form-group">
                    <label for="lastName">Last name</label>
                    <p class="bg-danger"><form:errors path="lastName"/></p>
                    <form:input id="lastName" cssClass="form-control" path="lastName" type="text"/>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <p class="bg-danger"><form:errors path="email"/></p>
                    <form:input id="email" cssClass="form-control" path="email" type="text"/>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <p class="bg-danger"><form:errors path="password"/></p>
                    <form:input id="password" path="password" cssClass="form-control" type="password"/>
                </div>
                <div class="form-group">
                    <label for="password_2">Repeat your password</label>
                    <p class="bg-danger"><form:errors path="password_2"/></p>
                    <form:input id="password_2" path="password_2" cssClass="form-control" type="password"/>
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <p class="bg-danger"><form:errors path="phone"/></p>
                    <form:input id="phone" cssClass="form-control" path="phone" type="text"/>
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <p class="bg-danger"><form:errors path="address"/></p>
                    <form:input id="address" path="address" cssClass="form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="addInfo">Added information about you</label>
                    <p class="bg-danger"><form:errors path="addInfo"/></p>
                    <form:input id="addInfo" path="addInfo" cssClass="form-control" type="text"/>
                </div>
                <button class="btn btn-default">Submit</button>
            </form:form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>