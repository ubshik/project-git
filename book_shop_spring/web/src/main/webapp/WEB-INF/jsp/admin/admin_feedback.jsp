<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html >
<html>
<head>
    <title>Feedback without answer</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid">
    <sec:authorize access="hasAuthority('SUPERADMIN')" >
        <jsp:include page="include_superadmin.jsp"/>
    </sec:authorize>

    <sec:authorize access="hasAuthority('ADMIN')" >
        <jsp:include page="include_admin.jsp"/>
    </sec:authorize>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="text-center">
                <h2>Let'a answer our users!</h2></div>
        </div>
        <div class="col-md-4"></div>
    </div>

    <div class="row">
        <div class="col-md-11">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>FeedBack id</th>
                        <th>User id</th>
                        <th>Topic</th>
                        <th>Content</th>
                        <th>Email</th>
                        <th>Date creation</th>
                        <th>Answer</th>
                        <th>Change answer</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="feedback" items="${feedback}">
                        <tr>
                            <td><c:out value="${feedback.feedbackId}"/></td>
                            <td><c:out value="${feedback.userId}"/></td>
                            <td><c:out value="${feedback.topic}"/></td>
                            <td><c:out value="${feedback.content}"/></td>
                            <td><c:out value="${feedback.email}"/></td>
                            <td><c:out value="${feedback.datePublication}"/></td>
                            <td><c:out value="${feedback.answer}"/></td>
                            <td><a href ="/admin/feedback/update/${feedback.feedbackId}">Update</a></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="right">
                <button class="btn-info"><a href ="/admin/feedback/all">All feedback</a></button>
            </div>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>