<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html >
<html>
<head>
    <title>Edit user date by admin</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>
<sec:authorize access="hasAuthority('SUPERADMIN')"></sec:authorize>
<div class="container-fluid">
    <sec:authorize access="hasAuthority('SUPERADMIN')" >
        <jsp:include page="include_superadmin.jsp"/>
    </sec:authorize>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="text-center">
                <h2>Role and status for edit!</h2></div>
        </div>
        <div class="col-md-4"></div>
    </div>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="text-center">
                <h3>User: <c:out value="${user.firstName}"/> <c:out value="${user.lastName}"/> with email "<c:out value="${user.email}"/>"</h3></div>
        </div>
        <div class="col-md-4"></div>
    </div>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form:form method="post" modelAttribute="user" action="/admin/users/update/{userId}">
                <div class="form-group">
                    <form:input id="userId" cssClass="form-control" path="userId" type="hidden" />
                </div>
                <div class="form-group">
                    <label for="role">Role</label>
                    <form:select id="role" cssClass="form-control" path="role" itemValue="${user.role}">
                        <form:option value="USER"/>
                        <form:option value="ADMIN"/>
                        <form:option value="SUPERADMIN"/>
                    </form:select>
                </div>
                <div class="form-group">
                    <label for="userStatus">Status</label>
                    <form:select id="userStatus" cssClass="form-control" path="userStatus" itemValue="${user.userStatus}">
                        <form:option value="ACTIVE"/>
                        <form:option value="BLOCKED"/>
                    </form:select>
                </div>
                <button class="btn btn-default">Submit</button>
            </form:form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>