<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<
<!DOCTYPE html >
<html lang="en">
<head>
    <title>Orders</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <sec:authorize access="hasAuthority('SUPERADMIN')" >
        <jsp:include page="include_superadmin.jsp"/>
    </sec:authorize>

    <sec:authorize access="hasAuthority('ADMIN')" >
        <jsp:include page="include_admin.jsp"/>
    </sec:authorize>

    <jsp:include page="include_success_error.jsp"/>

    <div class="row">
        <div class="col-md-11">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>Order Id</th>
                    <th>User Id</th>
                    <th>Total price</th>
                    <th>Status</th>
                    <th>Details</th>
                    <th>Change status</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="order" items="${order}">
                    <tr>
                        <td><c:out value="${order.orderId}"/></td>
                        <td><c:out value="${order.userId}"/></td>
                        <td><c:out value="${order.totalPrice}"/></td>
                        <td><c:out value="${order.orderStatus}"/></td>
                        <td><a href ="/admin/order/details/${order.orderId}">Details</a></td>
                        <td><a href ="/admin/order/${order.orderId}/status">Change status</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>