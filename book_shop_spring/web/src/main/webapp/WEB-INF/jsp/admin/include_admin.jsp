<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html >
<div class="container-fluid">
    <div class="row">
        <div class="col-md-11">
            <table class="table table-view">
                <thead>
                <tr>
                    <th></th>
                    <th><a href ="/admin/admin">WELCOME</a></th>
                    <th><a href ="/admin/books">BOOKS</a></th>
                    <th><a href ="/admin/cart">CART</a></th>
                    <th><a href ="/admin/orders">ORDERS</a></th>
                    <th><a href ="/admin/news">NEWS</a></th>
                    <th></th>
                    <th></th>
                    <th><a href ="/admin/feedback">FEEDBACK</a></th>
                    <th><a href ="/logout">LOG OUT</a></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
