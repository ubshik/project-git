<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<
<!DOCTYPE html >
<html lang="en">
<head>
    <title>Cart</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <sec:authorize access="hasAuthority('SUPERADMIN')" >
        <jsp:include page="include_superadmin.jsp"/>
    </sec:authorize>

    <sec:authorize access="hasAuthority('ADMIN')" >
        <jsp:include page="include_admin.jsp"/>
    </sec:authorize>

    <jsp:include page="include_success_error.jsp"/>

    <div class="row">
        <div class="col-md-11">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>Cart Id</th>
                    <th>User Id</th>
                    <th>Book Id</th>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Sum</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="cart" items="${cart}">
                    <tr>
                        <td><c:out value="${cart.cartId}"/></td>
                        <td><c:out value="${cart.userId}"/></td>
                        <td><c:out value="${cart.bookId}"/></td>
                        <td><c:out value="${cart.title}"/></td>
                        <td><c:out value="${cart.author}"/></td>
                        <td><c:out value="${cart.price}"/></td>
                        <td><c:out value="${cart.quantity}"/></td>
                        <td><c:out value="${cart.sum}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>