<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html >
<html>
<head>
    <title>Edit book by admin</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid">
    <sec:authorize access="hasAuthority('SUPERADMIN')" >
        <jsp:include page="include_superadmin.jsp"/>
    </sec:authorize>

    <sec:authorize access="hasAuthority('ADMIN')" >
        <jsp:include page="include_admin.jsp"/>
    </sec:authorize>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="text-center">
                <h2>Book for edit!</h2></div>
        </div>
        <div class="col-md-4"></div>
    </div>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form:form method="post" modelAttribute="book" action="/admin/books/update/{bookId}">
                <div class="form-group">
                    <form:input id="bookId" cssClass="form-control" path="bookId" type="hidden" />
                </div>
                <div class="form-group">
                    <label for="title">Title</label>
                    <p class="bg-danger"><form:errors path="title"/></p>
                    <form:input id="title" cssClass="form-control" path="title" type="text"/>
                </div>
                <div class="form-group">
                    <label for="author">Author</label>
                    <p class="bg-danger"><form:errors path="author"/></p>
                    <form:input id="author" cssClass="form-control" path="author" type="text"/>
                </div>
                <div class="form-group">
                    <label for="inventoryNumber">Inventory number</label>
                    <p class="bg-danger"><form:errors path="inventoryNumber"/></p>
                    <form:input id="inventoryNumber" cssClass="form-control" path="inventoryNumber" type="text"/>
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <p class="bg-danger"><form:errors path="description"/></p>
                    <form:input id="description" cssClass="form-control" path="description" type="text"/>
                </div>
                <div class="form-group">
                    <label for="price">Price</label>
                    <p class="bg-danger"><form:errors path="price"/></p>
                    <form:input id="price" cssClass="form-control" path="price" type="text"/>
                </div>
                <button class="btn btn-default">Submit</button>
            </form:form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>