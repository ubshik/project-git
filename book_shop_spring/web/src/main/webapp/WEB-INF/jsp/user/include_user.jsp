<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html >
<div class="container-fluid">
    <div class="row">
        <div class="col-md-11">
            <table class="table table-view">
                <thead>
                <tr>
                    <th></th>
                    <th><a href ="/user/user">WELCOME</a></th>
                    <th><a href ="/user/books">BOOKS</a></th>
                    <th><a href ="/user/cart">CART</a></th>
                    <th><a href ="/user/orders">ORDERS</a></th>
                    <th><a href ="/user/news">NEWS</a></th>
                    <th></th>
                    <th><a href ="/user/feedback">FEEDBACK</a></th>
                    <th></th>
                    <th></th>
                    <th>
                    <div class="dropdown">
                    <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown">PROFILE<span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href ="/user/edit/date">Edit your personal date.</a></li>
                            <li><a href ="/user/edit/password">Edit your password.</a></li>
                            <li class="divider"></li>
                            <li><a href ="/logout">LOG OUT</a></li>
                        </ul>
                    </div>
                    </th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
