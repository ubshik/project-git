<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<
<!DOCTYPE html >
<html lang="en">
<head>
    <title>Books</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <jsp:include page="include_user.jsp"/>
    <jsp:include page="include_success_error.jsp"/>

    <div class="row table-responsive" >
        <div class="col-md-11">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Add to cart</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="books" items="${books}">
                    <tr>
                        <td><c:out value="${books.title}"/></td>
                        <td><c:out value="${books.author}"/></td>
                        <td><c:out value="${books.description}"/></td>
                        <td><c:out value="${books.price}"/></td>
                        <td><button type="submit" class="btn btn-default"><a href ="/user/cart/add/${books.bookId}">Add to cart</a></button></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        </div>
    </div>
</div>
</body>
</html>