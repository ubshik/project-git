<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html >
<html lang="en">
<head>
    <title>User page</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid">
    <jsp:include page="include_user.jsp"/>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4" >
            <div class="text-center">
                <h2>Hello,
                    <sec:authorize access="isAuthenticated()">
                        <strong><sec:authentication property="principal.firstName"/></strong>
                    </sec:authorize>!</h2>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="text-center">
                <h2>Welcome to BOOK SHOP!</h2>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>