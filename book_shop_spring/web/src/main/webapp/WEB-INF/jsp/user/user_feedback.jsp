<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html >
<html>
<head>
    <title>Feedback to add</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid">
    <jsp:include page="include_user.jsp"/>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="text-center">
                <h2>Feedback to add!</h2></div>
            </div>
        <div class="col-md-4"></div>
    </div>

    <jsp:include page="include_success_error.jsp"/>


    <div class="row">
        <div class="col-md-4"></div>
        <sec:authentication property="principal.username" var="existEmail"/>
        <div class="col-md-4">
            <form:form method="post" modelAttribute="feedback" action="/user/feedback">
                <div class="form-group">
                    <label for="topic">Topic</label>
                    <p class="bg-danger"><form:errors path="topic"/></p>
                    <form:input id="topic" cssClass="form-control" path="topic" type="text"/>
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <p class="bg-danger"><form:errors path="content"/></p>
                    <form:input id="content" cssClass="form-control" path="content" type="text"/>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <p class="bg-danger"><form:errors path="email"/></p>
                    <form:input id="email" cssClass="form-control" path="email" type="text" value="${existEmail}"></form:input>
                </div>
                <button class="btn btn-default">Submit</button>
            </form:form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>