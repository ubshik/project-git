<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html >
<html>
<head>
    <title>Edit quantity in order</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid">
    <jsp:include page="include_user.jsp"/>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="text-center">
                <h2>Quantity for edit in order!</h2></div>
        </div>
        <div class="col-md-4"></div>
    </div>

    <jsp:include page="include_success_error.jsp"/>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form:form method="post" modelAttribute="cart" action="/user/order/${orderId}/cart/{cartId}/quantity">
                <div class="form-group">
                    <form:input id="cartId" cssClass="form-control" path="cartId" type="hidden" />
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="text-center">
                            Book: <c:out value="${cart.title}"/> by <c:out value="${cart.author}"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="quantity">Quantity</label>
                    <p class="bg-danger"><form:errors path="quantity"/></p>
                    <form:input id="quantity" cssClass="form-control" path="quantity" type="number" min="1" max="10"/>
                </div>
                <button class="btn btn-default">Submit</button>
            </form:form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>