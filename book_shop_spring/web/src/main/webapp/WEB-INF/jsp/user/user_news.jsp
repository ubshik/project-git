<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<
<!DOCTYPE html >
<html lang="en">
<head>
    <title>News</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <jsp:include page="include_user.jsp"/>

    <div class="row">
        <div class="col-md-11">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Topic</th>
                    <th>Image</th>
                    <th>Content</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="news" items="${news}">
                    <tr>
                        <th><c:out value="${news.datePublication}"/></th>
                        <td><c:out value="${news.topic}"/></td>
                        <td><img src="/user/news/download/<c:out value="${news.newsId}"/>" height="42" width="42"></td>
                        <td><c:out value="${news.content}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>