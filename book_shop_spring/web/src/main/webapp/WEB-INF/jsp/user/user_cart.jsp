<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<
<!DOCTYPE html >
<html lang="en">
<head>
    <title>Cart</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <jsp:include page="include_user.jsp"/>
    <jsp:include page="include_success_error.jsp"/>

    <div class="row">
        <div class="col-md-11">
            <form:form method="post" modelAttribute="integers" action="/user/order/add">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>Serial number</th>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Sum</th>
                        <th>Change quantity</th>
                        <th>Delete</th>
                        <th>Order</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="cart" items="${cart}" varStatus="loop">
                        <tr>
                            <td>${loop.index + 1}</td>
                            <td><c:out value="${cart.title}"/></td>
                            <td><c:out value="${cart.author}"/></td>
                            <td><c:out value="${cart.price}"/></td>
                            <td><c:out value="${cart.quantity}"/></td>
                            <td><c:out value="${cart.sum}"/></td>
                            <td><a href ="/user/cart/${cart.cartId}/quantity">Change quantity</a></td>
                            <td><a href ="/user/cart/delete/${cart.cartId}">Delete</a></td>
                            <td><form:checkbox  path="integers" value="${cart.cartId}"/></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-md-4">
                        <div class="right">
                            <button class="btn btn-primary">Order</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="text-center">
                <h3>Total sum ${sum}</h3>
            </div>
        </div>
    </div>
</div>
</body>
</html>