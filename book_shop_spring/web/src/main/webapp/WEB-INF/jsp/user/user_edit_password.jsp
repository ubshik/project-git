<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html >
<html>
<head>
    <title>Edit user date</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid">
    <jsp:include page="include_user.jsp"/>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="text-center">
                <h2>Your password for edit!</h2>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>

    <jsp:include page="include_success_error.jsp"/>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form:form method="post" modelAttribute="user" action="/user/edit/password">
                <div class="form-group">
                    <form:input id="userId" cssClass="form-control" path="userId" type="hidden" />
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <p class="bg-danger"><form:errors path="password"/></p>
                    <form:input id="password" cssClass="form-control" path="password" type="password"/>
                </div>
                <div class="form-group">
                    <label for="password_2">Repeat your password</label>
                    <p class="bg-danger"><form:errors path="password_2"/></p>
                    <form:input id="password_2" cssClass="form-control" path="password_2" type="password"/>
                </div>
                <button class="btn btn-default">Submit</button>
            </form:form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>