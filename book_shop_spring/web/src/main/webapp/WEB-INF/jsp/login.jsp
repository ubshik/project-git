<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html >
<html lang="en">
<head>
    <title>Login</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4" class="text-center">
            <h2>Log in or register to see site!</h2>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4" class="text-center">
            <h3><c:out value="${success}"></c:out></h3>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form action="login" method='POST'>
                <c:if test="${param['error']}">
                    <p class="bg-danger">Username or password is not valid</p>
                </c:if>
                    <div class="bg-danger"><c:out value="${errorStatus}"></c:out></div>
                <div class="form-group">
                    <label for="username">Email</label>
                    <input class="form-control" id="username" required name="username" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>
                <button class="btn btn-default">Submit</button>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4"><a href ="/registration">Registration</a></div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>