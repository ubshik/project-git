package com.gmail.ubshik.controller;

import com.gmail.ubshik.controller.validator.UserValidator;
import com.gmail.ubshik.service.IUserService;
import com.gmail.ubshik.service.model.AppUserPrincipal;
import com.gmail.ubshik.service.model.UserDTO;
import com.gmail.ubshik.service.model.util.Converter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * Created by Lubov Vol on 21.08.2017.
 */
@Controller
public class UserController {
    private static final Logger logger =  Logger.getLogger(UserController.class);

    private final IUserService userService;
    private final UserValidator userValidator;

    @Autowired
    public UserController(IUserService userService, UserValidator userValidator){
        this.userService = userService;
        this.userValidator = userValidator;
    }

    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public String showWelcomePage() {
        return "login";
    }

    @RequestMapping(value = "/admin/admin", method = RequestMethod.GET)
    public String showAdminMainPage() {
        return "/admin/admin";
    }

    @RequestMapping(value = "/user/user", method = RequestMethod.GET)
    public String showUserMainPage() {
        return "/user/user";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String showRegisterPage(Model model) {
        logger.info("Register page");
        model.addAttribute("user", new UserDTO());
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String saveUser(@ModelAttribute("user") UserDTO user, BindingResult result, RedirectAttributes attributes) {
        userValidator.validate(user, result);
        if (!result.hasErrors()) {
            userService.createUserDTO(user);
            attributes.addFlashAttribute("success", "You registered successful. Please, activate your account.");
            return "redirect:/login";
        } else {
            return "registration";
        }
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return "redirect:/login?logout";
    }

    @RequestMapping(value = "/user/edit", method = RequestMethod.GET)
    public String showUserEditPage() {
        logger.info("Page for edit by user.");
        return "/user/user_edit";
    }

    @RequestMapping(value = "/user/edit/date", method = RequestMethod.GET)
    public String showUserEditDatePage(Model model) {
        logger.info("Page for edit date by user");
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        model.addAttribute("user", userService.findUserById(principal.getUserId()));
        return "/user/user_edit_date";
    }

    @RequestMapping(value = "/user/edit/date", method = RequestMethod.POST)
    public String updateUserDate(UserDTO user, BindingResult result, Model model, RedirectAttributes attributes) {
        userValidator.validateEditDate(user, result);
        if (!result.hasErrors()) {
            userService.updateUserDateByUser(user);
            attributes.addFlashAttribute("success", "Your new date are saved successful.");
            return "redirect:/user/edit";
        } else {
            attributes.addFlashAttribute("error", "Your date wasn't updated. Your date is incorrect.");//todo i want to have field with problem, ignoreDefaultModelOnRedirect = true
            return "redirect:/user/edit/date";
        }
    }

    @RequestMapping(value = "/user/edit/password", method = RequestMethod.GET)
    public String showUserEditPasswordPage(Model model) {
        logger.info("Page for edit password by user");
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        model.addAttribute("user", Converter.convertToUserDTO(principal));
        return "/user/user_edit_password";
    }

    @RequestMapping(value = "/user/edit/password", method = RequestMethod.POST)
    public String updateUserPassword(UserDTO user, BindingResult result, RedirectAttributes attributes) {
        userValidator.validateEditPassword(user, result);
        if (!result.hasErrors()) {
            userService.updateUserPasswordByUser(user);
            attributes.addFlashAttribute("success", "Your new password are saved successful.");
            return "redirect:/user/edit";
        } else {
            attributes.addFlashAttribute("error", "Your password is not correct.");//todo i want to have field with problem, ignoreDefaultModelOnRedirect = true
            return "redirect:/user/edit/password";
        }
    }

    @RequestMapping(value = "/admin/users", method = RequestMethod.GET)
    public String getAllUser(Model model) {
        List<UserDTO> users = userService.getAll();
        model.addAttribute("users", users);
        return "admin/admin_users";
    }

    @RequestMapping(value = "/admin/users/delete/{userId}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable("userId") Integer userId, RedirectAttributes attributes) {
        userService.deleteUser(userId);
        attributes.addFlashAttribute("success", "User was deleted successful.");
        return "redirect:/admin/users";
    }

    @RequestMapping(value = "/admin/users/update/{userId}", method = RequestMethod.GET)
    public String showAdminEditPage(@PathVariable("userId") Integer userId, Model model) {
        logger.info("Page for edit role and status by superadmin");
        model.addAttribute("user", userService.findUserById(userId));
        return "/admin/admin_users_edit_date";
    }

    @RequestMapping(value = "/admin/users/update/{userId}", method = RequestMethod.POST)
    public String updateUserDateByAdmin(UserDTO user, RedirectAttributes attributes) {
        userService.updateUserDateByAdmin(user);
        attributes.addFlashAttribute("success", "User's date are saved successful.");
        return "redirect:/admin/users";
    }

}
