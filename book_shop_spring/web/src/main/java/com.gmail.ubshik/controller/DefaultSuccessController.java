package com.gmail.ubshik.controller;

import com.gmail.ubshik.repository.model.enums.Role;
import com.gmail.ubshik.repository.model.enums.UserStatus;
import com.gmail.ubshik.service.model.AppUserPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Created by Lubov Vol on 02.08.2017.
 */
@Controller
public class DefaultSuccessController {

    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public String filterDirectedUrl(RedirectAttributes attributes) {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        if (principal.getUserStatus() == UserStatus.ACTIVE) {
            if (principal.getRole() == Role.USER) {
                return "user/user";
            } else if (principal.getRole() == Role.ADMIN || principal.getRole() == Role.SUPERADMIN) {
                return "admin/admin";
            } else {
                return "redirect:/login";
            }
        } else {
            attributes.addFlashAttribute("errorStatus", "You are blocked. Connect with administration.");
            return "redirect:/login";
        }
    }
}
