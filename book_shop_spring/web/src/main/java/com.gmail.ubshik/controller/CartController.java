package com.gmail.ubshik.controller;

import com.gmail.ubshik.service.ICartService;
import com.gmail.ubshik.service.model.CartDTO;
import com.gmail.ubshik.service.model.util.IntegerListWrapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lubov Vol on 29.08.2017.
 */
@Controller
public class CartController {
    private static final Logger logger =  Logger.getLogger(CartController.class);

    private final ICartService cartService;

    @Autowired
    public  CartController(ICartService cartService){
        this.cartService = cartService;
    }

    @RequestMapping(value = "/admin/cart", method = RequestMethod.GET)
    public String getCartByAdmin(Model model) {
        List<CartDTO> cartDTOS = cartService.getCartWithoutOrder();
        model.addAttribute("cart", cartDTOS);
        return "admin/admin_cart";
    }

    @RequestMapping(value = "/user/cart", method = RequestMethod.GET)
    public String getCartByUser(Model model) {
        List<CartDTO> cartDTOS = cartService.getCartByUser();
        model.addAttribute("cart", cartDTOS);
        model.addAttribute("sum", cartService.sumOfCart());
        IntegerListWrapper integers = new IntegerListWrapper();
        integers.setIntegers(new ArrayList<Integer>());
        model.addAttribute("integers", integers);
        return "user/user_cart";
    }

    @RequestMapping(value = "/user/cart/add/{bookId}", method = RequestMethod.GET)
    public String addBookToCart(@PathVariable("bookId") Integer bookId, RedirectAttributes attributes) {
        CartDTO cartFromDB = cartService.getCardDTOByUserAndBook(bookId);
        if (cartFromDB == null) {
            cartService.createCart(bookId);
            attributes.addFlashAttribute("success","Book was added to cart successful.");
            return "redirect:/user/books";
        } else {
            attributes.addFlashAttribute("error","This book has already added to cart. Please change quantity if it will need.");
            return "redirect:/user/books";
        }
    }

    @RequestMapping(value = "/user/cart/delete/{cartId}", method = RequestMethod.GET)
    public String deleteCart(@PathVariable("cartId") Integer cartId, RedirectAttributes attributes) {
        cartService.deleteCart(cartId);
        attributes.addFlashAttribute("success", "Cart was deleted successful.");
        return "redirect:/user/cart";
    }

    @RequestMapping(value = "/user/cart/{cartId}/quantity", method = RequestMethod.GET)
    public String showBookQuantityInCart(@PathVariable("cartId") Integer cartId, Model model) {
        logger.info("Page for edit cart.");
        model.addAttribute("cart", cartService.getCartByCartId(cartId));
        return "/user/user_cart_edit_quantity";
    }

    @RequestMapping(value = "/user/cart/{cartId}/quantity", method = RequestMethod.POST)
    public String updateBookQuantityInCart(CartDTO cartDTO, RedirectAttributes attributes) {
        cartService.updateQuantity(cartDTO);
        attributes.addFlashAttribute("success", "Cart are updated successful.");
        return "redirect:/user/cart";
    }

}
