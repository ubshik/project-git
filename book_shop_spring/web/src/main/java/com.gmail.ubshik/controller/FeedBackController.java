package com.gmail.ubshik.controller;

import com.gmail.ubshik.controller.validator.FeedBackValidator;
import com.gmail.ubshik.service.IFeedBackService;
import com.gmail.ubshik.service.model.FeedBackDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * Created by Lubov Vol on 29.08.2017.
 */

@Controller
public class FeedBackController {
    private static final Logger logger =  Logger.getLogger(FeedBackController.class);

    private final IFeedBackService feedBackService;
    private final FeedBackValidator feedBackValidator;

    @Autowired
    public FeedBackController(IFeedBackService feedBackService, FeedBackValidator feedBackValidator){
        this.feedBackService = feedBackService;
        this.feedBackValidator = feedBackValidator;
    }

    @RequestMapping(value = "/user/feedback", method = RequestMethod.GET)
    public String showAddFeedBackPage(@ModelAttribute("feedback")FeedBackDTO feedBackDTO) {
        logger.info("Add feedback page");
        return "user/user_feedback";
    }

    @RequestMapping(value = "/user/feedback", method = RequestMethod.POST)
    public String saveFeedBack(@ModelAttribute("feedback")FeedBackDTO feedBackDTO, BindingResult result,
                           RedirectAttributes attributes) {
        feedBackValidator.validate(feedBackDTO, result);
        if (!result.hasErrors()) {
            feedBackService.createFeedBack(feedBackDTO);
            attributes.addFlashAttribute("success", "Your feedback was added successful.");
            return "redirect:/user/feedback";
        } else {
            return "user/user_feedback";
        }
    }

    @RequestMapping(value = "/admin/feedback", method = RequestMethod.GET)
    public String getFeedBackWithoutAnswer(Model model) {
        List<FeedBackDTO> feedBackDTOS = feedBackService.getFeedBackWithoutAnswer();
        model.addAttribute("feedback", feedBackDTOS);
        return ("admin/admin_feedback");
    }

    @RequestMapping(value = "/admin/feedback/update/{feedbackId}", method = RequestMethod.GET)
    public String showFeedBackToEditPage(@PathVariable("feedbackId") Integer feedbackId, Model model) {
        logger.info("Page for edit feedback.");
        model.addAttribute("feedback", feedBackService.getFeedBackById(feedbackId));
        return "/admin/admin_feedback_edit";
    }

    @RequestMapping(value = "/admin/feedback/update/{feedbackId}", method = RequestMethod.POST)
    public String updateFeedBackStatus(FeedBackDTO feedBackDTO, RedirectAttributes attributes) {
        feedBackService.updateStatusFeedBack(feedBackDTO);
        attributes.addFlashAttribute("success", "Feedback status are updated successful.");
        return "redirect:/admin/feedback/all";
    }

    @RequestMapping(value = "/admin/feedback/all", method = RequestMethod.GET)
    public String getFeedBackAll(Model model) {
        List<FeedBackDTO> feedBackDTOS = feedBackService.getAllFeedBack();
        model.addAttribute("feedback", feedBackDTOS);
        return ("admin/admin_feedback_all");
    }
}

