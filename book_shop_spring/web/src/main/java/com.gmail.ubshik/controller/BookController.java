package com.gmail.ubshik.controller;

import com.gmail.ubshik.controller.validator.BookValidator;
import com.gmail.ubshik.repository.model.enums.Role;
import com.gmail.ubshik.service.IBookService;
import com.gmail.ubshik.service.model.AppUserPrincipal;
import com.gmail.ubshik.service.model.BookDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * Created by Lubov Vol on 29.08.2017.
 */

@Controller
public class BookController {
    private static final Logger logger =  Logger.getLogger(BookController.class);

    private final IBookService bookService;
    private final BookValidator bookValidator;

    @Autowired
    public  BookController(IBookService bookService, BookValidator bookValidator){
        this.bookService = bookService;
        this.bookValidator = bookValidator;
    }

    @RequestMapping(value ={"/admin/books", "/user/books"}, method = RequestMethod.GET)
    public String getBooks(Model model) {
        List<BookDTO> bookDTOS = bookService.getAllBook();
        model.addAttribute("books", bookDTOS);
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        if(principal.getRole().equals(Role.SUPERADMIN) || principal.getRole().equals(Role.ADMIN)){
            return "admin/admin_books";
        }else {
            return "user/user_books";
        }
    }

    @RequestMapping(value = "/admin/books/add", method = RequestMethod.GET)
    public String showAddBookPage(@ModelAttribute("book") BookDTO bookDTO) {
        logger.info("Add book page");
        return "admin/admin_books_add";
    }

    @RequestMapping(value = "/admin/books/add", method = RequestMethod.POST)
    public String saveBook(@ModelAttribute("book") BookDTO bookDTO, BindingResult result,
                           RedirectAttributes attributes) {
        bookValidator.validate(bookDTO, result);
        if (!result.hasErrors()) {
            bookService.createBookDTO(bookDTO);
            attributes.addFlashAttribute("success", "Your book was added successful.");
            return "redirect:/admin/books";
        } else {
            return "admin/admin_books_add";
        }
    }

    @RequestMapping(value = "/admin/books/delete/{bookId}", method = RequestMethod.GET)
    public String deleteBook(@PathVariable("bookId") Integer bookId, RedirectAttributes attributes) {
        bookService.deleteBookById(bookId);
        attributes.addFlashAttribute("success", "Book was deleted successful.");
        return "redirect:/admin/books";
    }

    @RequestMapping(value = "/admin/books/update/{bookId}", method = RequestMethod.GET)
    public String showAdminEditBookPage(@PathVariable("bookId") Integer bookId, Model model, RedirectAttributes attributes) {
        logger.info("Page for edit book.");
        BookDTO bookDTO = bookService.checkBookInOrder(bookId);//TODO - right SQL
        if(bookDTO == null){
            model.addAttribute("book", bookService.getBookById(bookId));
            return "/admin/admin_books_edit";
        }else {
            String message = "Book with id " + bookId + " is in order with status 'NEW'. You couldn't update it.";
            attributes.addFlashAttribute("error", message);
            return "redirect:/admin/books";
        }
    }

    @RequestMapping(value = "/admin/books/update/{bookId}", method = RequestMethod.POST)
    public String updateBookDateByAdmin(BookDTO bookDTO, RedirectAttributes attributes) {
        bookService.updateBook(bookDTO);
        attributes.addFlashAttribute("success", "Book are updated successful.");
        return "redirect:/admin/books";
    }

    @RequestMapping(value = "/admin/books/copy/{bookId}", method = RequestMethod.GET)
    public String showCopyBookPage(@PathVariable("bookId") Integer bookId, RedirectAttributes attributes) {
        logger.info("Page for copy book.");
        bookService.copyBook(bookId);
        attributes.addFlashAttribute("success", "Book are copied successful.");
        return "redirect:/admin/books";
    }


}

