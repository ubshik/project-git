package com.gmail.ubshik.controller;

import com.gmail.ubshik.controller.validator.NewsValidator;
import com.gmail.ubshik.repository.model.enums.Role;
import com.gmail.ubshik.service.IImageService;
import com.gmail.ubshik.service.INewsService;
import com.gmail.ubshik.service.model.AppUserPrincipal;
import com.gmail.ubshik.service.model.NewsDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.List;

/**
 * Created by Lubov Vol on 25.08.2017.
 */
@Controller
public class NewsController {
    private static final Logger logger =  Logger.getLogger(NewsController.class);

    private final INewsService newsService;
    private final NewsValidator newsValidator;
    private final IImageService imageService;

    @Autowired
    public NewsController(INewsService newsService, NewsValidator newsValidator, IImageService imageService){
        this.newsService = newsService;
        this.newsValidator = newsValidator;
        this.imageService = imageService;
    }

    @RequestMapping(value ={"/admin/news", "/user/news"}, method = RequestMethod.GET)
    public String get10LastNews(Model model) {
        List<NewsDTO> news = newsService.get10News();
        model.addAttribute("news", news);
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        if(principal.getRole().equals(Role.SUPERADMIN) || principal.getRole().equals(Role.ADMIN)){
            return "admin/admin_news";
        }else {
            return "user/user_news";
        }
    }

    @RequestMapping(value = {"/admin/news/download/{newsId}", "/user/news/download/{newsId}"}, method = RequestMethod.GET)
    public void downloadImage(HttpServletResponse response, @PathVariable(value = "newsId") Integer newsId) throws IOException{
        File file = imageService.getFileById(newsId);
        if(!file.exists()){
            String errorMessage = "Required image is not exist.";
            logger.warn(errorMessage);
            OutputStream outputStream = response.getOutputStream();
            outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")));
            outputStream.close();
            return;
        }
        String mimeType = URLConnection.guessContentTypeFromName(file.getName());
        if(mimeType == null){
            logger.warn("Mime type is not detectable. There is taken default.");
            mimeType = "application/octet-stream";
        }
        logger.info("Mime type is " + mimeType);
        response.setContentType(mimeType);
        response.setHeader("Content-Disposition", String.format("inline; filename=\"%s\"", file.getName()));
        response.setContentLength((int) file.length());
        try(FileInputStream fis = new FileInputStream(file); InputStream is = new BufferedInputStream(fis)){
            FileCopyUtils.copy(is, response.getOutputStream());
        }
    }

    @RequestMapping(value = "/admin/news/add", method = RequestMethod.GET)
    public String showAddNewsPage(@ModelAttribute("news") NewsDTO newsDTO) {
        logger.info("Add news page");
        return "admin/admin_news_add";
    }

    @RequestMapping(value = "/admin/news/add", method = RequestMethod.POST)
    public String saveNews(@ModelAttribute("news") NewsDTO newsDTO, BindingResult result,
                           RedirectAttributes attributes) throws IOException {
        newsValidator.validate(newsDTO, result);
        if (!result.hasErrors()) {
            newsService.createNews(newsDTO);
            attributes.addFlashAttribute("success", "Your news was added successful.");
            return "redirect:/admin/news";
        } else {
            return "admin/admin_news_add";
        }
    }

    @RequestMapping(value = "/admin/news/delete/{newsId}", method = RequestMethod.GET)
    public String deleteNews(@PathVariable("newsId") Integer newsId, RedirectAttributes attributes) {
        newsService.deleteNews(newsId);
        attributes.addFlashAttribute("success", "News was deleted successful.");
        return "redirect:/admin/news";
    }

    @RequestMapping(value = "/admin/news/update/{newsId}", method = RequestMethod.GET)
    public String showEditNewsPage(@PathVariable("newsId") Integer newsId, Model model, RedirectAttributes attributes) {
        logger.info("Page for edit news.");
        NewsDTO newsDTO = newsService.getNewsById(newsId);
        model.addAttribute("news", newsDTO);
        return "/admin/admin_news_edit";
    }

    @RequestMapping(value = "/admin/news/update/{newsId}", method = RequestMethod.POST)
    public String updateNews(@ModelAttribute(value = "news") NewsDTO newsDTO, RedirectAttributes attributes) {
        newsService.updateNews(newsDTO);
        attributes.addFlashAttribute("success", "News are updated successful.");
        return "redirect:/admin/news";
    }

    @RequestMapping(value = "admin/news/update/{newsId}/image", method = RequestMethod.GET)
    public String showEditNewsImagePage(@PathVariable("newsId") Integer newsId, Model model, RedirectAttributes attributes) {
        logger.info("Page for edit news.");
        NewsDTO newsDTO = newsService.getNewsById(newsId);
        model.addAttribute("news", newsDTO);
        return "/admin/admin_news_edit_image";
    }

    @RequestMapping(value = "admin/news/update/{newsId}/image", method = RequestMethod.POST)
    public String updateNewsImage(@ModelAttribute(value = "news") NewsDTO newsDTO, RedirectAttributes attributes) throws IOException {
        newsService.updateNewsImage(newsDTO);
        attributes.addFlashAttribute("success", "News image is updated successful.");
        return "redirect:/admin/news";
    }


}
