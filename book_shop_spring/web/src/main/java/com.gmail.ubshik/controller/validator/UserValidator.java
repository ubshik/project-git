package com.gmail.ubshik.controller.validator;

import com.gmail.ubshik.service.model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Lubov Vol on 21.08.2017.
 */
@Component
public class UserValidator implements Validator {
    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UserDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDTO user = (UserDTO) o;
        ValidationUtils.rejectIfEmpty(errors, "firstName", "NotEmpty.user.firstName");
        if (user.getFirstName().length() < 3 || user.getFirstName().length() > 50) {
            errors.rejectValue("firstName", "Size.user.firstName.regexp");
        }
        ValidationUtils.rejectIfEmpty(errors, "lastName", "NotEmpty.user.lastName");
        if (user.getLastName().length() < 3 || user.getLastName().length() > 50) {
            errors.rejectValue("lastName", "Size.user.lastName.regexp");
        }
        ValidationUtils.rejectIfEmpty(errors, "email", "NotEmpty.user.email");
        if (user.getEmail().length() < 8 || user.getEmail().length() > 50) {
            errors.rejectValue("email", "Email.user.email");
        }
        ValidationUtils.rejectIfEmpty(errors, "password", "NotEmpty.user.password");
        if (user.getPassword().length() < 1 || user.getPassword().length() > 15) {
            errors.rejectValue("password", "Size.user.password.regexp");
        }
        ValidationUtils.rejectIfEmpty(errors, "password_2", "NotEmpty.user.password");
        if (!user.getPassword().equals(user.getPassword_2())) {
            errors.rejectValue("password_2", "Different.user.password");
        }
        ValidationUtils.rejectIfEmpty(errors, "phone", "NotEmpty.user.phone");
        if (user.getPhone().length() != 11) {
            errors.rejectValue("phone", "Size.user.phone.regexp");
        }
        if (!user.getPhone().matches("[0-9]+")) {
            errors.rejectValue("phone", "Match.user.phone");
        }
        ValidationUtils.rejectIfEmpty(errors, "address", "NotEmpty.user.address");
        if (user.getAddress().length() < 6 || user.getAddress().length() > 100) {
            errors.rejectValue("address", "Size.password.address.regexp");
        }
        UserDetails userDetails = null;
        try {
            userDetails = userDetailsService.loadUserByUsername(user.getEmail());
        } catch (UsernameNotFoundException e) {
            //simple validation
        }
        if (userDetails != null) {
            errors.rejectValue("email", "non.unique.email.regexp");
        }
    }

    public void validateEditDate(Object o, Errors errors) {
        UserDTO user = (UserDTO) o;
        ValidationUtils.rejectIfEmpty(errors, "firstName", "NotEmpty.user.firstName");
        if (user.getFirstName().length() < 3 || user.getFirstName().length() > 50) {
            errors.rejectValue("firstName", "Size.user.firstName.regexp");
        }
        ValidationUtils.rejectIfEmpty(errors, "lastName", "NotEmpty.user.lastName");
        if (user.getLastName().length() < 3 || user.getLastName().length() > 50) {
            errors.rejectValue("lastName", "Size.user.lastName.regexp");
        }
        ValidationUtils.rejectIfEmpty(errors, "phone", "NotEmpty.user.phone");
        if (user.getPhone().length() != 11) {
            errors.rejectValue("phone", "Size.user.phone.regexp");
        }
        if (!user.getPhone().matches("[0-9]+")) {
            errors.rejectValue("phone", "Match.user.phone");
        }
        ValidationUtils.rejectIfEmpty(errors, "address", "NotEmpty.user.address");
        if (user.getAddress().length() < 6 || user.getAddress().length() > 100) {
            errors.rejectValue("address", "Size.password.address.regexp");
        }
    }

    public void validateEditPassword(Object o, Errors errors) {
        UserDTO user = (UserDTO) o;
        ValidationUtils.rejectIfEmpty(errors, "password", "NotEmpty.user.password");
        if (user.getPassword().length() < 1 || user.getPassword().length() > 15) {
            errors.rejectValue("password", "Size.user.password.regexp");
        }
        ValidationUtils.rejectIfEmpty(errors, "password_2", "NotEmpty.user.password");
        if (!user.getPassword().equals(user.getPassword_2())) {
            errors.rejectValue("password_2", "Different.user.password");
        }
    }
}
