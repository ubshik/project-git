package com.gmail.ubshik.controller.validator;

import com.gmail.ubshik.service.model.NewsDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Lubov Vol on 21.08.2017.
 */
@Component
public class NewsValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(NewsDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        NewsDTO news = (NewsDTO) o;
        ValidationUtils.rejectIfEmpty(errors, "topic", "NotEmpty.news.topic");
        if (news.getTopic().length() < 3 && news.getTopic().length() > 50) {
            errors.rejectValue("topic", "Size.news.topic.regexp");
        }
        ValidationUtils.rejectIfEmpty(errors, "content", "NotEmpty.news.content");
        ValidationUtils.rejectIfEmpty(errors, "image", "NotEmpty.news.image");
    }
}
