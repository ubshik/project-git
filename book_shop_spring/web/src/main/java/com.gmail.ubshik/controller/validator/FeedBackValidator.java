package com.gmail.ubshik.controller.validator;

import com.gmail.ubshik.service.model.BookDTO;
import com.gmail.ubshik.service.model.FeedBackDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Lubov Vol on 21.08.2017.
 */
@Component
public class FeedBackValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(FeedBackDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        FeedBackDTO feedBackDTO = (FeedBackDTO) o;
        ValidationUtils.rejectIfEmpty(errors, "topic", "NotEmpty.feedback.topic");
        if (feedBackDTO.getTopic().length() < 3 || feedBackDTO.getTopic().length() > 50) {
            errors.rejectValue("topic", "Size.feedback.topic.regexp");
        }
        ValidationUtils.rejectIfEmpty(errors, "content", "NotEmpty.feedback.content");
        ValidationUtils.rejectIfEmpty(errors, "email", "NotEmpty.feedback.email");
        if (feedBackDTO.getEmail().length() < 8 || feedBackDTO.getEmail().length() > 50) {
            errors.rejectValue("email", "Email.feedback.email");
        }
    }
}
