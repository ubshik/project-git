package com.gmail.ubshik.controller.validator;

import com.gmail.ubshik.service.model.BookDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Lubov Vol on 21.08.2017.
 */
@Component
public class BookValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(BookDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        BookDTO book = (BookDTO) o;
        ValidationUtils.rejectIfEmpty(errors, "title", "NotEmpty.book.title");
        if (book.getTitle().length() > 50) {
            errors.rejectValue("title", "Size.book.title.regexp");
        }
        ValidationUtils.rejectIfEmpty(errors, "author", "NotEmpty.book.author");
        if (book.getAuthor().length() > 50) {
            errors.rejectValue("author", "Size.book.author.regexp");
        }
        ValidationUtils.rejectIfEmpty(errors, "inventoryNumber", "NotEmpty.book.inventoryNumber");
        if (book.getInventoryNumber().length() < 5 && book.getInventoryNumber().length() > 8) {
            errors.rejectValue("inventoryNumber", "Size.book.inventoryNumber.regexp");
        }
        ValidationUtils.rejectIfEmpty(errors, "description", "NotEmpty.book.description");
        ValidationUtils.rejectIfEmpty(errors, "price", "NotEmpty.book.price");
    }
}
