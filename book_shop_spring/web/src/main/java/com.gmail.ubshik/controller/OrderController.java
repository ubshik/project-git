package com.gmail.ubshik.controller;

import com.gmail.ubshik.repository.model.enums.Role;
import com.gmail.ubshik.service.IBookService;
import com.gmail.ubshik.service.ICartService;
import com.gmail.ubshik.service.IOrderService;
import com.gmail.ubshik.service.model.AppUserPrincipal;
import com.gmail.ubshik.service.model.BookDTO;
import com.gmail.ubshik.service.model.CartDTO;
import com.gmail.ubshik.service.model.OrderDTO;
import com.gmail.ubshik.service.model.util.IntegerListWrapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * Created by Lubov Vol on 30.08.2017.
 */
@Controller
public class OrderController {
    private static final Logger logger =  Logger.getLogger(OrderController.class);

    private final IOrderService orderService;
    private final ICartService cartService;
    private final IBookService bookService;

    @Autowired
    public OrderController(IOrderService orderService, ICartService cartService, IBookService bookService){
        this.orderService = orderService;
        this.cartService = cartService;
        this.bookService = bookService;
    }

    @RequestMapping(value = "/admin/orders", method = RequestMethod.GET)
    public String getOrderByAdmin(Model model) {
        List<OrderDTO> orderDTOS = orderService.getAllOrder();
        model.addAttribute("order", orderDTOS);
        return "admin/admin_orders";
    }

    @RequestMapping(value = "/user/orders", method = RequestMethod.GET)
    public String getOrderByUser(Model model) {
        List<OrderDTO> orderDTOS = orderService.getAllOrderByUserId();
        model.addAttribute("order", orderDTOS);
        return "user/user_orders";
    }

    @RequestMapping(value = "/user/order/add", method = RequestMethod.POST)
    public String createOrder(@ModelAttribute(value = "integers")IntegerListWrapper integers, RedirectAttributes attributes) {
        if (integers == null){
            attributes.addFlashAttribute("error", "You don't choose anything to order");
            return "redirect:/user/cart";
        } else {
            orderService.createOrder(integers.getIntegers());
            return "redirect:/user/orders";
        }
    }

    @RequestMapping(value = "/admin/order/{orderId}/status", method = RequestMethod.GET)
    public String showChangeOrderStatusPage(@PathVariable("orderId") Integer orderId, Model model) {
        logger.info("Page for edit order status.");
        model.addAttribute("order", orderService.getOrderById(orderId));
        return "/admin/admin_orders_edit_status";
    }

    @RequestMapping(value = "/admin/order/{orderId}/status", method = RequestMethod.POST)
    public String updateOrderStatus(OrderDTO orderDTO, RedirectAttributes attributes) {
        orderService.updateOrderStatus(orderDTO);
        attributes.addFlashAttribute("success", "Order status are updated successful.");
        return "redirect:/admin/orders";
    }

    @RequestMapping(value = {"/admin/order/details/{orderId}", "/user/order/details/{orderId}"}, method = RequestMethod.GET)
    public String getOrderDetails(@PathVariable("orderId") Integer orderId, Model model, RedirectAttributes attributes) {
        List<CartDTO> cartDTOS = cartService.getCartWithOrder(orderId);
        model.addAttribute("cart", cartDTOS);
        model.addAttribute("sum", cartService.sumOfOrder(orderId));
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        if(principal.getRole().equals(Role.SUPERADMIN) || principal.getRole().equals(Role.ADMIN)){
            return "admin/admin_cart";
        }else {
            return "user/user_orders_details";
        }
    }

    @RequestMapping(value = "/user/order/update/{orderId}", method = RequestMethod.GET)
    public String showUpdateOrderStatusPage(@PathVariable("orderId") Integer orderId, Model model) {
        logger.info("Page for edit order in status NEW.");
        List<CartDTO> cartDTOS = cartService.getCartWithOrder(orderId);
        model.addAttribute("cart", cartDTOS);
        model.addAttribute("orderId", orderId);
        return "/user/user_orders_edit";
    }

    @RequestMapping(value = "/user/order/{orderId}/cart/{cartId}/quantity", method = RequestMethod.GET)
    public String showBookQuantityInOrder(@PathVariable("cartId") Integer cartId, @PathVariable("orderId") Integer orderId, Model model) {
        logger.info("Page for edit cart.");
        model.addAttribute("cart", cartService.getCartByCartId(cartId));
        model.addAttribute("orderId", orderId);
        return "/user/user_orders_edit_quantity";
    }

    @RequestMapping(value = "/user/order/{orderId}/cart/{cartId}/quantity", method = RequestMethod.POST)
    public String updateQuantityInOrder(@ModelAttribute(value = "cart")CartDTO cartDTO, @PathVariable("orderId") Integer orderId, Model model) {
        logger.info("Page for edit quantity in order.");
        orderService.updateQuantityInOrder(orderId, cartDTO);
        return "redirect:/user/orders";
    }

    @RequestMapping(value = "/user/order/{orderId}/cart/{cartId}/delete", method = RequestMethod.GET)
    public String deleteBookInOrder(@PathVariable("cartId") Integer cartId, @PathVariable("orderId") Integer orderId) {
        logger.info("Page for delete cart in order.");
        orderService.deleteBookInOrder(orderId, cartId);
        return "redirect:/user/orders";
    }

    @RequestMapping(value = "/user/order/{orderId}/book/add", method = RequestMethod.GET)
    public String showBookForOrder(@PathVariable("orderId") Integer orderId, Model model, RedirectAttributes attributes) {
        logger.info("Page for order book.");
        List<BookDTO> bookDTOS = bookService.getBookForOrder(orderId);
        if(bookDTOS == null || bookDTOS.size() == 0){
            attributes.addFlashAttribute("error", "All shop book you have had in your order. Please, change quantity if it needs.");
            return "redirect:/user/orders";
        } else {
            model.addAttribute("books", bookDTOS);
            model.addAttribute("orderId", orderId);
            return "/user/user_orders_add_books";
        }
    }

    @RequestMapping(value = "/user/order/{orderId}/book/add/{bookId}", method = RequestMethod.GET)
    public String updateQuantityInOrder(@PathVariable("orderId") Integer orderId, @PathVariable("bookId") Integer bookId) {
        logger.info("Page for add book in order.");
        orderService.addBookToOrder(orderId, bookId);
        return "redirect:/user/orders";
    }

}
