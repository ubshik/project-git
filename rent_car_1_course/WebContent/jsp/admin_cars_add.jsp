<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
 <head>
  <meta charset="utf-8">
  <title>Add car</title>
 </head>
 <body>
 	<h2>ADD A CAR</h2> 
 	<form name="AddCarForm" method="post" action="${pageContext.request.contextPath}/admin/cars/add">
 		<b>Enter Producer : </b>
   		<input name="producer" type="text" size="50"><br><br>
   		<b>Enter Model : </b>
   		<input name="model" type="text" size="50"><br><br>
  		<b>Enter Number of doors : </b>
   		<input name="numderDoors" type="text" size="50"><br><br>
   		<b>Enter Price of one day : </b>
   		<input name="priceOneDay" type="text" size="50"><br><br>
  		<input type="submit" value="Add new car">	
 	</form> <br>
	<%@include file="/jsp/admin_include.jsp"%>
 </body>
</html>