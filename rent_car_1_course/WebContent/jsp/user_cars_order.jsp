<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
 <head>
  <meta charset="utf-8">
  <title>Order a car</title>
 </head>
 <body>
 	<h2>ORDER A CAR</h2>  
 	<h3>Your chosen car is ${producer} ${model} <br>with price of one day $${priceOneDay} </h3>	
 	<form name="OrderCarForm" method="post" action="${pageContext.request.contextPath}/user/orders/add">
 		<input name="id_user" type="hidden" size="50" value="${user.id}">
 		<input name="id_car" type="hidden" size="50" value="${id_car}">
 		<input name="numberDays" type="hidden" size="50" value="${numberDays}">
 		<input name="totalPrice" type="hidden" size="50" value="${totalPrice}"><br>
 		<p>Number days for rent a car : ${numberDays} days.<br></p>
   		<p>Total price for rent a car : $${totalPrice}. <br>
  		<input type="submit" value="Order car">	
 	</form> <br>
 	<%@include file="/jsp/user_include.jsp"%>
 </body>
</html>