<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
 <head>
  <meta charset="utf-8">
  <title>Calculate total price</title>
 </head>
 <body>
 	<h2>ORDER A CAR</h2>  
 	<h3>Your chosen car is ${producer} ${model}  with price of one day $${priceOneDay}.</h3>
 	<form name="CalculateTotalPrice" method="post" action="${pageContext.request.contextPath}/user/orders/add/calculate">
 		<input name="id_car" type="hidden" size="50" value="${id_car}"><br>
 		<b>Enter Number days for rent a car : </b>
   		<input name="numberDays" type="text" size="50" ><br><br>   		
  		<input type="submit" value="Calculate total price">	
 	</form> <br>
 	<%@include file="/jsp/user_include.jsp"%>
 </body>
</html>