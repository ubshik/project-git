<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
 <head>
  <meta charset="utf-8">
  <title>Menu of cars</title>
 </head>
 <body>
 <h2>RENT CARS</h2> 
 	<table>
    	<tr>
            <th>Id order</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Producer</th>
            <th>Model</th>
            <th>Number days to rent</th>
            <th>Total price</th>
            <th>Action</th>
        </tr>
     <c:forEach var="order" items="${order}">
         <tr>
             <td><c:out value="${order.id}"/></td>
             <td><c:out value="${order.user.firstname}"/></td>
             <td><c:out value="${order.user.lastname}"/></td>
             <td><c:out value="${order.car.producer}"/></td>
             <td><c:out value="${order.car.model}"/></td>
             <td><c:out value="${order.numberDaysToRent}"/></td>
             <td><c:out value="${order.totalPrice}"/></td>
             <td><a href="${pageContext.request.contextPath}/admin/oders/delete?id=${order.id}">Delete</a></td>
         </tr>
     </c:forEach>
    </table>
     <form name="CarList" method="post" action="${pageContext.request.contextPath}/admin/cars">
  			<p><input type="submit" value="Car view"></p>
 	</form>
    <form name="Logout" method="get" action="${pageContext.request.contextPath}/logout">
  		<p><input type="submit" value="Logout"></p><br>
 	</form>
 </body>
</html>