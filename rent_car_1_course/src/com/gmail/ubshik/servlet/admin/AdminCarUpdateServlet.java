package com.gmail.ubshik.servlet.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gmail.ubshik.models.Car;
import com.gmail.ubshik.service.Service;

public class AdminCarUpdateServlet extends HttpServlet {
	
	private final Service service = new Service();

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer id = Integer.valueOf(request.getParameter("id"));
		try{
			Car car = service.getCar(id);
			request.setAttribute("id", car.getId()); 
			request.setAttribute("producer", car.getProducer()); 
			request.setAttribute("model", car.getModel()); 
			request.setAttribute("numberDoors", car.getNumberDoors()); 
			request.setAttribute("priceOneDay", car.getPriceOneDay()); 
			request.getRequestDispatcher("/jsp/admin_cars_update.jsp").forward(request, response);
		}catch (Exception e) {
			System.out.println("Mistake to get car by id");
			request.setAttribute("error", "Mistake to get car by id");
			request.getRequestDispatcher("/jsp/admin_error.jsp").forward(request, response);
		}		
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String producer = null;
		String model = null;
		Integer numberDoors = null;
		Integer priceOneDay = null;
		Integer id = null;
		try{
			id = Integer.valueOf(request.getParameter("id")); 
			producer = request.getParameter("producer"); 
			model = request.getParameter("model"); 
			numberDoors = Integer.parseInt(request.getParameter("numberDoors")); 
			priceOneDay = Integer.parseInt(request.getParameter("priceOneDay")); 
		}catch (Exception e) {
			System.out.println("Your date is uncorrect.");
			request.setAttribute("error", "Your date is uncorrect. Please fill its right");
			request.getRequestDispatcher("/jsp/admin_error.jsp").forward(request, response);
			return;
		}
		Car car = null;
		int checkpoint = 1;
		if(!id.equals("") && !producer.equals("") && !model.equals("")
				&& !numberDoors.equals("") && !priceOneDay.equals("")){
			try{
				car = service.createCar(id, producer, model, numberDoors, priceOneDay); 
				checkpoint = service.updateCar(car); 
				if(checkpoint == 1){
					request.setAttribute("success", "Car is updated success");
					request.getRequestDispatcher("/jsp/admin_success_action.jsp").forward(request, response);
				}else{
					System.out.println("Mistake in the car update");
					request.setAttribute("error", "Mistake in the car update");
					request.getRequestDispatcher("/jsp/admin_error.jsp").forward(request, response);
				}
			}catch (Exception e) {
				System.out.println("Mistake" + e.getMessage());
				throw new IllegalStateException("Unknown error");
			}
		}else{
			System.out.println("Your filds are free.");
			request.setAttribute("error", "Your filds are free. Please fill in the form");
			request.getRequestDispatcher("/jsp/admin_error.jsp").forward(request, response);
		}
		
	}
}
