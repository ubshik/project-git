package com.gmail.ubshik.servlet.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gmail.ubshik.models.Order;
import com.gmail.ubshik.service.Service;

public class AdminOrderListServlet extends HttpServlet {
	
	private final Service service = new Service();

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Order> listOrder = null;
		try{
			listOrder = service.geAdminOrdersList();
			request.setAttribute("order", listOrder);
			request.getRequestDispatcher("/jsp/admin_orders.jsp").forward(request, response);
		}catch (Exception e) {
			System.out.println("Mistake of trying to get list of order");
			request.setAttribute("error", e.getMessage());
			request.getRequestDispatcher("/jsp/admin_error.jsp").forward(request, response);
		}		
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
