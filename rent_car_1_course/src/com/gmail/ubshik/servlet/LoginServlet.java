package com.gmail.ubshik.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gmail.ubshik.models.User;
import com.gmail.ubshik.service.Service;

@WebServlet(name="login",urlPatterns={"/login"})
public class LoginServlet extends HttpServlet {
	
	private final Service service = new Service();

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		try {
			User user = service.getUser(email, password);
			if (user != null){
				request.getSession().setAttribute("user", user);
				request.setAttribute("firstname", user.getFirstname());
				switch(user.getUserType()){
					case "user":
						request.getRequestDispatcher("/jsp/user_success.jsp").forward(request, response);
						break;
					case "admin":
						request.getRequestDispatcher("/jsp/admin_success.jsp").forward(request, response);
						break;
					default:
						throw new IllegalArgumentException("Unknown role");
				}
			} else{
				request.getSession().setAttribute("error", "Credentials is not valid");
				request.setAttribute("error", "Incorrect data is entered");
				request.getRequestDispatcher("/jsp/error.jsp").forward(request, response);
			}
		}catch (Exception e) {
			System.out.println("Mistake" + e.getMessage());
			throw new IllegalStateException("Unknown error");
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
