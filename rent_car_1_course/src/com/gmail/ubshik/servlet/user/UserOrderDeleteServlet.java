package com.gmail.ubshik.servlet.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.gmail.ubshik.service.Service;

public class UserOrderDeleteServlet extends HttpServlet {
	
	private final Service service = new Service();

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer id = Integer.valueOf(request.getParameter("id"));
		int checkpoint = 1;			
			try{
				checkpoint = service.orderDelete(id);
				if(checkpoint == 1){
					request.setAttribute("success", "Order is deleted success");
					request.getRequestDispatcher("/jsp/user_success_action.jsp").forward(request, response);
				}else{
					System.out.println("Mistake in the order delete");
					request.setAttribute("error", "Mistake in the order delete");
					request.getRequestDispatcher("/jsp/user_error.jsp").forward(request, response);
				}
			}catch (Exception e) {
				System.out.println("Mistake" + e.getMessage());
				throw new IllegalStateException("Unknown error");
			}		
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
