package com.gmail.ubshik.servlet.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gmail.ubshik.models.Car;
import com.gmail.ubshik.models.Order;
import com.gmail.ubshik.models.User;
import com.gmail.ubshik.service.Service;

public class UserOrderAddServlet extends HttpServlet {
	
	private final Service service = new Service();

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer id_user = Integer.parseInt(request.getParameter("id_user")); 
		Integer id_car = Integer.parseInt(request.getParameter("id_car")); 
		Integer numberDays = Integer.parseInt(request.getParameter("numberDays")); 
		Integer totalPrice = Integer.parseInt(request.getParameter("totalPrice")); 
		Car car = null;
		User user = null;
		int checkpoint = 1;		
		try{
			user = service.getUser(id_user);
			car = service.getCar(id_car);
			Order order = service.createOrder(numberDays, totalPrice);
			checkpoint = service.addOrder(user, car, order);
			if(checkpoint == 1){
				System.out.println( "Checkpoint" + checkpoint);
				request.setAttribute("success", "Order is added success. Total price is $" + order.getTotalPrice() + ".");
				request.getRequestDispatcher("/jsp/user_success_action.jsp").forward(request, response);
			}else{
				System.out.println("Mistake in the order add");
				request.setAttribute("error", "Mistake in the car add");
				request.getRequestDispatcher("/jsp/user_error.jsp").forward(request, response);
			}
			}catch (Exception e) {
				System.out.println("Mistake in the order add " + e.getMessage());
				throw new IllegalStateException("Unknown error" + e);
			}		
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
