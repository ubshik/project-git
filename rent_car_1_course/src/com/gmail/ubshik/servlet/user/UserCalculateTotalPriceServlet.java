package com.gmail.ubshik.servlet.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gmail.ubshik.models.Car;
import com.gmail.ubshik.service.Service;

public class UserCalculateTotalPriceServlet extends HttpServlet {
	private final Service service = new Service();

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer id_car = Integer.parseInt(request.getParameter("id_car"));
		Integer numberDays = null;
		try{
			numberDays = Integer.parseInt(request.getParameter("numberDays"));
		}catch (Exception e) {
			System.out.println("Your date is uncorrect.");
			request.setAttribute("error", "Your date is uncorrect. Please fill its right");
			request.getRequestDispatcher("/jsp/user_error.jsp").forward(request, response);
			return;
		}
		Car car = null;
		int totalPrice = 0;
		if(!numberDays.equals("")){			
			try{
				car = service.getCar(id_car);
				totalPrice = car.getPriceOneDay() * numberDays;
				request.setAttribute("id_car", car.getId());
				request.setAttribute("producer", car.getProducer());
				request.setAttribute("model", car.getModel());
				request.setAttribute("priceOneDay", car.getPriceOneDay());
				request.setAttribute("numberDays", numberDays);
				request.setAttribute("totalPrice", totalPrice);
				request.getRequestDispatcher("/jsp/user_cars_order.jsp").forward(request, response);
				System.out.println("Car from calculation is founded");
			}catch (Exception e) {
				System.out.println("Mistake" + e.getMessage());
				throw new IllegalStateException("Unknown error");
			}
		}else{
			System.out.println("Your filds are free.");
			request.setAttribute("error", "Your filds are free. Please fill in the form");
			request.getRequestDispatcher("/jsp/user_error.jsp").forward(request, response);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
