package roadCamera.service.service;

import roadCamera.repository.domain.Registration;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public interface RegistrationService {
    Serializable save (Registration car);
    List<Registration> getAll();
    List<Date> getByCarNumber(String carNumber);
    long count();
    String checkExistenceCarNumber(String carNumber);
}
