package roadCamera.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import roadCamera.repository.dao.RegistrationDao;
import roadCamera.repository.domain.Registration;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class RegistrationServiceImpl implements RegistrationService {
    @Autowired
    private RegistrationDao registrationDao;

    @Override
    public Serializable save(Registration car) {
        return registrationDao.save(car);
    }

    @Override
    public List<Registration> getAll() {
        return registrationDao.getAll();
    }

    @Override
    public List<Date> getByCarNumber(String carNumber) {
        return registrationDao.getByCarNumber(carNumber);
    }

    @Override
    public long count() {
        return registrationDao.count();
    }

    @Override
    public String checkExistenceCarNumber(String carNumber) {
        return registrationDao.checkExistenceCarNumber(carNumber);
    }
}

