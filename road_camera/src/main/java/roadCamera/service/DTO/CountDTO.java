package roadCamera.service.DTO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import roadCamera.json.CountSerializer;

@JsonSerialize(using = CountSerializer.class)
public class CountDTO {

    private long count;

    public CountDTO() {
    }

    public CountDTO(long count) {
        this.count = count;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
