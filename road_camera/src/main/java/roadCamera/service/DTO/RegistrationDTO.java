package roadCamera.service.DTO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import roadCamera.json.RegistrationDeserializer;
import roadCamera.json.RegistrationSerializer;
import roadCamera.validation.RegistrationSaveConstraint;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

@JsonSerialize(using = RegistrationSerializer.class)
@JsonDeserialize(using = RegistrationDeserializer.class)
@RegistrationSaveConstraint
public class RegistrationDTO implements Serializable{

    Date timestamp;
    @NotEmpty(message = "\"Car number\" mast not be empty.")
    String carNumber;

    public RegistrationDTO() {
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public static RegistrationDTOBuilder builder(){
        return new RegistrationDTOBuilder();
    }

    public static class RegistrationDTOBuilder {
        private RegistrationDTO registrationDTO = new RegistrationDTO();

        public RegistrationDTOBuilder timestamp(Date date){
            registrationDTO.timestamp = date;
            return this;
        }

        public RegistrationDTOBuilder carNumber(String carNumber){
            registrationDTO.carNumber = carNumber;
            return this;
        }

        public RegistrationDTO build(){
            return registrationDTO;
        }
    }
}
