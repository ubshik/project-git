package roadCamera.service.DTO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import roadCamera.json.DateSerializer;

import java.util.Date;

@JsonSerialize(using = DateSerializer.class)
public class DateDTO {

    private Date date;

    public DateDTO() {
    }

    public DateDTO(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
