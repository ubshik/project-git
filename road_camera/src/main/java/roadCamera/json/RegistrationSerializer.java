package roadCamera.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import roadCamera.service.DTO.RegistrationDTO;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RegistrationSerializer extends StdSerializer<RegistrationDTO>{

    public RegistrationSerializer() {
        this(null);
    }

    public RegistrationSerializer(Class<RegistrationDTO> t) {
        super(t);
    }

    @Override
    public void serialize(RegistrationDTO registration, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("date and time", dateToString(registration.getTimestamp()));
        jsonGenerator.writeStringField("car number", registration.getCarNumber());
        jsonGenerator.writeEndObject();
    }

    private String dateToString(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return sdf.format(date);
    }
}
