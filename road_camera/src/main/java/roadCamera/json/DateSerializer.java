package roadCamera.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import roadCamera.service.DTO.DateDTO;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateSerializer extends StdSerializer<DateDTO>{

    public DateSerializer() {
        this(null);
    }

    public DateSerializer(Class<DateDTO> t) {
        super(t);
    }

    @Override
    public void serialize(DateDTO date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("date and time", dateToString(date.getDate()));
        jsonGenerator.writeEndObject();
    }

    private String dateToString(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return sdf.format(date);
    }
}
