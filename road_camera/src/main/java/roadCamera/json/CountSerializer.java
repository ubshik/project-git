package roadCamera.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import roadCamera.service.DTO.CountDTO;

import java.io.IOException;

public class CountSerializer extends StdSerializer<CountDTO>{

    public CountSerializer() {
        this(null);
    }

    public CountSerializer(Class<CountDTO> t) {
        super(t);
    }

    @Override
    public void serialize(CountDTO countDTO, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("Total entries", countDTO.getCount());
        jsonGenerator.writeEndObject();
    }
}
