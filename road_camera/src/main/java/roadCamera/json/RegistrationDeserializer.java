package roadCamera.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import roadCamera.service.DTO.RegistrationDTO;

import java.io.IOException;

public class RegistrationDeserializer extends StdDeserializer<RegistrationDTO>{

    public RegistrationDeserializer() {
        this(null);
    }

    public RegistrationDeserializer(Class<?> t) {
        super(t);
    }

    @Override
    public RegistrationDTO deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException{
        String carNumber = null;
        JsonToken currentToken = null;
        while ((currentToken = jsonParser.nextValue()) != null) {
            switch (currentToken) {
                case VALUE_STRING:
                    switch (jsonParser.getCurrentName()) {
                        case "car number":
                            carNumber = jsonParser.getText();
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        return new RegistrationDTO.RegistrationDTOBuilder().carNumber(carNumber).build();
    }
}
