package roadCamera.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = CarNumberExistenceValidator.class)
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface CarNumberExistenceConstraint {
    String message() default "This car number wasn't registered in system.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
