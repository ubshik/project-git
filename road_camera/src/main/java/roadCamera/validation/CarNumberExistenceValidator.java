package roadCamera.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import roadCamera.service.service.RegistrationService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class CarNumberExistenceValidator implements ConstraintValidator<CarNumberExistenceConstraint, String>{

    @Autowired
    private RegistrationService registrationService;

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return registrationService.checkExistenceCarNumber(s) != null;
    }


}
