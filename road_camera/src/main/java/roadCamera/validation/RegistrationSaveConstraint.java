package roadCamera.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = RegistrationSaveValidator.class)
@Target({java.lang.annotation.ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RegistrationSaveConstraint {
    String message() default "Field values is empty.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
