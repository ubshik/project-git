package roadCamera.validation;

import org.springframework.stereotype.Component;
import roadCamera.service.DTO.RegistrationDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class RegistrationSaveValidator implements ConstraintValidator<RegistrationSaveConstraint, RegistrationDTO>{

    @Override
    public boolean isValid(RegistrationDTO registrationDTO, ConstraintValidatorContext constraintValidatorContext) {
        return registrationDTO != null;
    }
}
