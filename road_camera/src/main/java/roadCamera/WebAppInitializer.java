package roadCamera;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import roadCamera.config.AppConfig;
import roadCamera.config.ControllerConfig;

public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{AppConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{ControllerConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
