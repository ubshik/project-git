package roadCamera.repository.dao;

import roadCamera.repository.domain.Registration;

import java.io.Serializable;
import java.util.List;

public interface RegistrationDao {
    Serializable save (Registration registration);
    List<Registration> getAll();
    List getByCarNumber(String carNumber);
    long count();
    String checkExistenceCarNumber(String carNumber);
}
