package roadCamera.repository.dao;


import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import roadCamera.repository.domain.Registration;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Repository
public class RegistrationDaoImpl implements RegistrationDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private Environment environment;

    @Override
    public Serializable save(Registration registration) {
        return sessionFactory.getCurrentSession().save(registration);
    }

    @Override
    public List<Registration> getAll() {
        return sessionFactory.getCurrentSession()
                .createQuery(environment.getRequiredProperty("getAll"), Registration.class)
                .getResultList();
    }

    @Override
    public List<Date> getByCarNumber(String carNumber) {
        return sessionFactory.getCurrentSession()
                .createQuery(environment.getRequiredProperty("getDatesByCarNumber"))
                .setParameter("carNumber", carNumber)
                .getResultList();
    }

    @Override
    public long count() {
        return (long) sessionFactory.getCurrentSession()
                .createQuery(environment.getRequiredProperty("count"))
                .getSingleResult();
    }

    @Override
    public String checkExistenceCarNumber(String carNumber) {
        return (String) sessionFactory.getCurrentSession()
                .createQuery(environment.getRequiredProperty("checkExistenceCarNumber"))
                .setParameter("carNumber", carNumber)
                .stream().findFirst().orElse(null);
    }
}
