package roadCamera.repository.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import roadCamera.json.RegistrationDeserializer;
import roadCamera.json.RegistrationSerializer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "T_REGISTRATION")
@JsonSerialize(using = RegistrationSerializer.class)
@JsonDeserialize(using = RegistrationDeserializer.class)
public class Registration implements Serializable{

    @Id
    @Column(name = "F_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    Date timestamp;
    @Column(name = "F_CAR_NUMBER", nullable = false)
    String carNumber;

    public Registration() {
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public static RegistrationBuilder builder(){
        return new RegistrationBuilder();
    }

    public static class RegistrationBuilder{
        private Registration registration = new Registration();

        public RegistrationBuilder timestamp(){
            registration.timestamp = new Date();
            return this;
        }

        public RegistrationBuilder carNumber(String carNumber){
            registration.carNumber = carNumber;
            return this;
        }

        public Registration build(){
            return registration;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Registration that = (Registration) o;
        return Objects.equals(timestamp, that.timestamp) &&
                Objects.equals(carNumber, that.carNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, carNumber);
    }
}
