package roadCamera.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "roadCamera.service")
public class ServiceConfig {
}
