-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.7.17-log - MySQL Community Server (GPL)
-- Операционная система:         Win32
-- HeidiSQL Версия:              9.4.0.5151
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных car_rent
CREATE DATABASE IF NOT EXISTS `car_rent` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `car_rent`;

-- Дамп структуры для таблица car_rent.cars
CREATE TABLE IF NOT EXISTS `cars` (
  `id_car` int(11) NOT NULL AUTO_INCREMENT,
  `producer` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `number_doors` int(11) NOT NULL,
  `price_one_day` int(11) NOT NULL,
  PRIMARY KEY (`id_car`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы car_rent.cars: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
REPLACE INTO `cars` (`id_car`, `producer`, `model`, `number_doors`, `price_one_day`) VALUES
	(1, 'BMW', 'X5', 5, 155),
	(2, 'KIA', 'RIO', 5, 30),
	(3, 'TOYOTA', 'CAROLLA', 5, 40),
	(4, 'Ferrari', '488GTB', 3, 1500),
	(5, 'Renault', 'Logan', 5, 25),
	(6, 'Fiat', 'Bravo', 5, 30),
	(8, 'Opel', 'Astra', 5, 35),
	(9, 'KIA', 'Ceed', 5, 55);
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;

-- Дамп структуры для таблица car_rent.order
CREATE TABLE IF NOT EXISTS `order` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_car` int(11) NOT NULL,
  `number_day_rent` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  PRIMARY KEY (`id_order`),
  KEY `FK_order_users` (`id_user`),
  KEY `FK_order_cars` (`id_car`),
  CONSTRAINT `FK_order_cars` FOREIGN KEY (`id_car`) REFERENCES `cars` (`id_car`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_order_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы car_rent.order: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
REPLACE INTO `order` (`id_order`, `id_user`, `id_car`, `number_day_rent`, `total_price`) VALUES
	(1, 2, 6, 4, 120),
	(4, 5, 5, 4, 250),
	(6, 4, 8, 7, 245),
	(8, 2, 6, 9, 270),
	(9, 5, 4, 2, 3000),
	(10, 2, 9, 2, 110);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

-- Дамп структуры для таблица car_rent.users
CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(15) NOT NULL,
  `type` varchar(5) NOT NULL DEFAULT 'user',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы car_rent.users: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id_user`, `firstname`, `lastname`, `email`, `password`, `type`) VALUES
	(1, 'Luba', 'Vol', 'ubshik@gmail.com', 'admin', 'admin'),
	(2, 'Olya', 'Swistynova', 'olga@tut.by', '123123', 'user'),
	(3, 'Oleg', 'Vinnikov', 'oleg@tut.by', '0000', 'user'),
	(4, 'Petr', 'Petrivich', 'petya@tut.by', '112233', 'user'),
	(5, 'Ivan', 'Yrgant', 'yrgant@mail.ru', '141414', 'user'),
	(6, 'Matvei', 'Van', 'motya@mail.ru', '1111', 'user'),
	(8, 'Lena', 'Rizhik', 'rizhik@tut.by', '000', 'user'),
	(9, 'Vova', 'Lesnoi', 'les@rm.by', '111', 'user'),
	(10, 'Igor', 'Livak', 'livak@it.com', '222', 'user');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
