Social Feedback Platform Project Implementation
=========


Applying database scripts with Liquibase
------------------

#### <i class="icon-file"></i> Step 1: Create a Changelog File
The database changelog file is where all database changes are listed. It is [XML based](http://www.liquibase.org/documentation/xml_format.html), so start with an empty XML file which called *'master.xml'*:
```
<?xml version="1.0" encoding="UTF-8"?>
<databaseChangeLog
    xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext"
    xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog
        http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.1.xsd
        http://www.liquibase.org/xml/ns/dbchangelog-ext
        http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd">
        
</databaseChangeLog>
```
This is main changelog file that includes all changeset files.


#### <i class="icon-file"></i> Step 2: Add a ChangeSet
Each [change set](http://www.liquibase.org/documentation/changeset.html) is uniquely identified by an “id” attribute and an “author” attribute. These two tags, along with the name and package of the changelog file uniquely identify the change. If only an “id” needed to be specified, it would be too easy to accidentally duplicate them, especially when dealing with multiple developers and code branches. Including an “author” attribute minimizes the chances of duplications.

Think of each change set as an atomic change that you want to apply to your database. It’s usually best to include just one change in your change set, but more are allowed and can make sense if you are inserting multiple rows that should be added as a single transaction. Liquibase will attempt to run each change set as a single transaction, but many databases will silently commit and resume transactions for certain commands (create table, drop table, etc.)
```
<?xml version="1.0" encoding="UTF-8"?>

<databaseChangeLog
    xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext"
    xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog
        http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.1.xsd
        http://www.liquibase.org/xml/ns/dbchangelog-ext
        http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd">
         
    <changeSet id="1" author="BOB">
        <createTable tableName="DEPARTMENT">
            <column name="ID" type="bigint">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="NAME" type="varchar(50)">
                <constraints nullable="false"/>
            </column>
            <column name="ACTIVE" type="boolean" defaultValueBoolean="true"/>
        </createTable>
        <rollback>
            <dropTable tableName="DEPARTMENT"/>
        </rollback>
    </changeSet>
    
</databaseChangeLog>
```
For adding this changeset we need to create subfolder called *'release_X'* where 'X'
 is release number (starts with 1 and increments by 1, for example '*release_1*'). 
Next step is adding new changelog file called *'Y.Z-Creating_Table_Department.xml'* where 'Y' is release numbers (equals to number on folder name), 'Z' - latest script's version (on every new release starts with 1 and increments by 1 after each db schema's change, for example *'1.1-Creating_Table_Department.xml'*).
After that, we need include our changelog file to master changelog file:
```
<?xml version="1.0" encoding="UTF-8"?>

<databaseChangeLog 
    xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext"
    xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog
        http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.1.xsd
        http://www.liquibase.org/xml/ns/dbchangelog-ext
        http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd">
    
    <include file="liquibase/changelog/release_1/1.1-Creating-Table_Department.xml" />

</databaseChangeLog>
```
Also we can creating changesets in different formats like [JSON](http://www.liquibase.org/documentation/json_format.html), [YAML](http://www.liquibase.org/documentation/yaml_format.html), [SQL](http://www.liquibase.org/documentation/sql_format.html).


#### <i class="icon-refresh"></i> Step 3: Run the ChangeSet
  
 There are many ways to execute your change log including via [command line](http://www.liquibase.org/documentation/command_line.html), [Maven](http://www.liquibase.org/documentation/maven/index.html), [Spring](http://www.liquibase.org/documentation/spring.html) and other ways.

We are using [Maven Liquibase Plugin](http://www.liquibase.org/documentation/maven/index.html) for manually case executing (command: `$ mvn liquibase:update`) and [Spring](http://www.liquibase.org/documentation/spring.html) for automatically case.

#### <i class="icon-check"></i> Step 4: Check Your Database
You will see that your database now contains a table called “department”. Two other tables are created as well: *“[databasechangelog](http://www.liquibase.org/documentation/databasechangelog_table.html)”* and *“[databasechangeloglock](http://www.liquibase.org/documentation/databasechangeloglock_table.html)”*. The *databasechangelog* table contains a list of all the statements that have been run against the database. The *databasechangeloglock* table is used to make sure two machines don’t attempt to modify the database at the same time.
