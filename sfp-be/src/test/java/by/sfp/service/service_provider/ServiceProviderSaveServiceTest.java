package by.sfp.service.service_provider;

import by.sfp.config.RunnableTestConfiguration;
import by.sfp.domain.ClassCategory;
import by.sfp.domain.DomainCategory;
import by.sfp.domain.ServiceProvider;
import by.sfp.service.domain_category.DomainCategorySaveService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunnableTestConfiguration
@RunWith(SpringRunner.class)
public class ServiceProviderSaveServiceTest {
    @Autowired
    private ServiceProviderSaveService serviceProviderSave;

    @Autowired
    private DomainCategorySaveService domainCategorySave;
    
    @Autowired
    private ServiceProviderGetAllService serviceProviderGetAll;

    @Test
    public void saveServiceProvider() {
        DomainCategory entertainment = DomainCategory.builder().name("Развлечения")
                .addClassCategory(ClassCategory.builder().name("Аттракционы").build())
                .addClassCategory(ClassCategory.builder().name("Караоке").build())
                .build();
        domainCategorySave.execute(entertainment);

        ServiceProvider dreamland = ServiceProvider.builder()
                .name("Дримлэнд").city("Минск").street("Орловская").building("80")
                .addClassCategorySet(entertainment.getClassCategories())
                .build();
        serviceProviderSave.execute(dreamland);

        List<ServiceProvider> serviceProviders = serviceProviderGetAll.execute();
        assertNotNull(serviceProviders);
        assertEquals(1, serviceProviders.size());
        assertTrue(serviceProviders.contains(dreamland));
    }
}
