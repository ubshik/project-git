package by.sfp.service.service_provider;

import by.sfp.config.RunnableTestConfiguration;
import by.sfp.domain.ClassCategory;
import by.sfp.domain.DomainCategory;
import by.sfp.domain.ServiceProvider;
import by.sfp.service.domain_category.DomainCategorySaveService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunnableTestConfiguration
@RunWith(SpringRunner.class)
public class ServiceProviderGetByIdServiceTest {
    @Autowired
    private ServiceProviderGetByIdService serviceProviderGetById;

    @Autowired
    private DomainCategorySaveService domainCategorySave;

    @Autowired
    private ServiceProviderSaveService serviceProviderSave;

    @Test
    public void getServiceProviderById() {
        DomainCategory entertainment = DomainCategory.builder().name("Развлечения")
                .addClassCategory(ClassCategory.builder().name("Аттракционы").build())
                .addClassCategory(ClassCategory.builder().name("Караоке").build())
                .build();
        domainCategorySave.execute(entertainment);

        ServiceProvider dreamland = ServiceProvider.builder()
                .name("Дримлэнд").city("Минск").street("Орловская").building("80")
                .addClassCategorySet(entertainment.getClassCategories())
                .build();
        serviceProviderSave.execute(dreamland);

        ServiceProvider savedServiceProvider = serviceProviderGetById.execute(dreamland.getId());

        assertNotNull(savedServiceProvider);
        assertEquals(2, savedServiceProvider.getClassCategories().size());
        assertEquals("Дримлэнд", savedServiceProvider.getName());
    }
}
