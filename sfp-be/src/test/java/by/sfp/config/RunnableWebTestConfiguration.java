package by.sfp.config;

import by.sfp.config.hibernate.HibernateConfig;
import by.sfp.config.mapping.MappingConfig;
import by.sfp.config.service.ServiceConfig;
import by.sfp.config.util.UtilConfig;
import by.sfp.config.validation.ValidationConfig;
import by.sfp.config.web.ControllerConfig;

import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@WebAppConfiguration
@ContextConfiguration(classes = {HibernateConfig.class, ServiceConfig.class, MappingConfig.class, ControllerConfig.class,
        ValidationConfig.class, UtilConfig.class})
@PropertySource("classpath:hibernate/datasource.${spring.profiles.active}.properties")
@Transactional
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RunnableWebTestConfiguration {
}
