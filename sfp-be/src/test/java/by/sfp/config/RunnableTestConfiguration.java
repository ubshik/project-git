package by.sfp.config;

import by.sfp.config.hibernate.HibernateConfig;
import by.sfp.config.mapping.MappingConfig;
import by.sfp.config.service.ServiceConfig;

import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;

import javax.transaction.Transactional;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@ContextConfiguration(classes = {HibernateConfig.class, ServiceConfig.class, MappingConfig.class})
@PropertySource("classpath:hibernate/datasource.${spring.profiles.active}.properties")
@Transactional
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RunnableTestConfiguration {
}
