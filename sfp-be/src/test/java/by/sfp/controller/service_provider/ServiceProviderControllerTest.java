package by.sfp.controller.service_provider;

import by.sfp.config.RunnableWebTestConfiguration;
import by.sfp.controller.context.ControllerTestContext;
import by.sfp.domain.ClassCategory;
import by.sfp.domain.DomainCategory;
import by.sfp.domain.ServiceProvider;
import by.sfp.mapping.service_provider.mapper.ServiceProviderSaveMapper;
import by.sfp.service.domain_category.DomainCategorySaveService;
import by.sfp.service.service_provider.ServiceProviderSaveService;
import by.sfp.util.JsonConverter;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;

@RunWith(SpringRunner.class)
@RunnableWebTestConfiguration
public class ServiceProviderControllerTest extends ControllerTestContext {
    @Autowired
    private DomainCategorySaveService domainCategorySave;

    @Autowired
    private ServiceProviderSaveService serviceProviderSave;

    @Autowired
    private JsonConverter converter;

    @Test
    public void saveServiceProvider() throws Exception {
        ClassCategory cinema = ClassCategory.builder().name("Cinema").build();
        ClassCategory restaurant = ClassCategory.builder().name("Restaurants").build();
        DomainCategory entertainment = DomainCategory.builder()
                .name("Entertainments")
                .addClassCategory(cinema)
                .addClassCategory(restaurant)
                .build();
        domainCategorySave.execute(entertainment);

        ServiceProviderSaveMapper serviceProvider = ServiceProviderSaveMapper.builder()
                .name("Dreamland")
                .city("Minsk").street("Orlovskaya").building("80").block("A")
                .mobile("+375291150176").phone("+375173697401")
                .email("dreamlandpark@tut.by").site("http://dreamland.by")
                .classCategories(Arrays.asList(cinema.getId(), restaurant.getId()))
                .build();

        getMockMvc().perform(post("/service_providers")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(converter.toJson(serviceProvider)))
                .andExpect(status().isCreated())
                .andDo(getDocumentHandler()
                        .document(
                                requestFields(
                                        fieldWithPath("name").description(""),
                                        fieldWithPath("city").description(""),
                                        fieldWithPath("street").description(""),
                                        fieldWithPath("building").description(""),
                                        fieldWithPath("block").description(""),
                                        fieldWithPath("mobile").description(""),
                                        fieldWithPath("phone").description(""),
                                        fieldWithPath("email").description(""),
                                        fieldWithPath("site").description(""),
                                        fieldWithPath("classCategories").type(JsonFieldType.ARRAY).description("")
                                )));
    }

    @Test
    public void saveServiceProviderWithIncorrectValues() throws Exception {
        ServiceProviderSaveMapper serviceProvider = ServiceProviderSaveMapper.builder()
                .name("DreamlandDreamlandDreamlandDreamland")
                .city("MinskMinskMinskMinskMinskMinsk1")
                .street("OrlovskayaOrlovskayaOrlovskaya1")
                .building("80888")
                .block("AAAA")
                .mobile("notMobile").phone("notPhone")
                .email("dreamlandparktut.by")
                .site("http://dreamlandby")
                .classCategories(Arrays.asList(9999999L))
                .build();

        getMockMvc().perform(post("/service_providers")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(converter.toJson(serviceProvider)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.fields.name", containsInAnyOrder("\'Name\' must has length less or equal 30")))
                .andExpect(jsonPath("$.fields.city", containsInAnyOrder("\'City\' must has length less or equal 30")))
                .andExpect(jsonPath("$.fields.street", containsInAnyOrder("\'Street\' must has length less or equal 30")))
                .andExpect(jsonPath("$.fields.building", containsInAnyOrder("\'Building\' must has length less or equal 4")))
                .andExpect(jsonPath("$.fields.block", containsInAnyOrder("\'Block\' must has length less or equal 3")))
                .andExpect(jsonPath("$.fields.mobile", containsInAnyOrder("\'Mobile\' must be a well-formed")))
                .andExpect(jsonPath("$.fields.phone", containsInAnyOrder("\'Phone\' must be a well-formed")))
                .andExpect(jsonPath("$.fields.email", containsInAnyOrder("\'Email\' must be a well-formed email address")))
                .andExpect(jsonPath("$.fields.site", containsInAnyOrder("\'Site\' must be a well-formed URL")))
                .andExpect(jsonPath("$.fields.classCategories", containsInAnyOrder("Class categories aren't valid")));
    }

    @Test
    public void saveServiceProviderWithEmptyValues() throws Exception {
        ServiceProviderSaveMapper serviceProvider = ServiceProviderSaveMapper.builder()
                .name("")
                .city("")
                .street("")
                .building("")
                .block("")
                .mobile("").phone("")
                .email("")
                .site("")
                .classCategories(Arrays.asList())
                .build();

        getMockMvc().perform(post("/service_providers")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(converter.toJson(serviceProvider)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.fields.name", containsInAnyOrder("\'Name\' must not be empty")))
                .andExpect(jsonPath("$.fields.city", containsInAnyOrder("\'City\' must not be empty")))
                .andExpect(jsonPath("$.fields.classCategories", containsInAnyOrder("Class categories aren't valid")))
                .andExpect(jsonPath("$.common", containsInAnyOrder("Fields values aren't match")));
    }

    @Test
    public void getServiceProviderById() throws Exception {
        DomainCategory entertainment = DomainCategory.builder()
                .name("Entertainments")
                .addClassCategory(ClassCategory.builder().name("Cinema").build())
                .addClassCategory(ClassCategory.builder().name("Restaurants").build())
                .build();
        domainCategorySave.execute(entertainment);

        ServiceProvider savedServiceProvider = ServiceProvider.builder()
                .name("Dreamland")
                .city("Minsk").street("Orlovskaya").building("80").block("A")
                .mobile("+375291150176").phone("+375173697401")
                .email("dreamlandpark@tut.by").site("http://dreamland.by")
                .addClassCategorySet(entertainment.getClassCategories())
                .build();
        serviceProviderSave.execute(savedServiceProvider);

        getMockMvc().perform(get("/service_providers/{id}", savedServiceProvider.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(converter.toJson(savedServiceProvider)))
                .andDo(getDocumentHandler().document(
                        pathParameters(
                                parameterWithName("id").description("Service provider id")
                        ),
                        responseFields(
                                fieldWithPath("id").description(""),
                                fieldWithPath("name").description(""),
                                fieldWithPath("city").description(""),
                                fieldWithPath("street").description(""),
                                fieldWithPath("building").description(""),
                                fieldWithPath("block").description(""),
                                fieldWithPath("mobile").description(""),
                                fieldWithPath("phone").description(""),
                                fieldWithPath("email").description(""),
                                fieldWithPath("site").description(""),
                                fieldWithPath("classCategories").description(""),
                                fieldWithPath("classCategories.[].id").description(""),
                                fieldWithPath("classCategories.[].name").description(""),
                                fieldWithPath("classCategories.[].domainCategory").description(""),
                                fieldWithPath("classCategories.[].domainCategory.id").description(""),
                                fieldWithPath("classCategories.[].domainCategory.name").description("")
                        )
                ));
    }

    @Test
    public void getServiceProviderByIncorrectId() throws Exception {
        getMockMvc().perform(get("/service_providers/{id}", 0)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.common", containsInAnyOrder("Service provider's id doesn't exist")));
    }
}
