package by.sfp.service.service_provider;

import by.sfp.domain.ServiceProvider;
import by.sfp.service.SaveService;

public interface ServiceProviderSaveService extends SaveService<ServiceProvider> {
}
