package by.sfp.service.service_provider;

import by.sfp.domain.ServiceProvider;
import by.sfp.service.GetByIdService;

public interface ServiceProviderGetByIdService extends GetByIdService<ServiceProvider> {
}
