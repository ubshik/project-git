package by.sfp.service.service_provider.impl;

import by.sfp.dao.service_provider.ServiceProviderGetByIdDao;
import by.sfp.domain.ServiceProvider;
import by.sfp.service.service_provider.ServiceProviderGetByIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ServiceProviderGetByIdServiceImpl implements ServiceProviderGetByIdService {
    @Autowired
    private ServiceProviderGetByIdDao serviceProviderGetByIdDao;

    @Override
    public ServiceProvider execute(Long id) {
        return serviceProviderGetByIdDao.execute(id);
    }
}
