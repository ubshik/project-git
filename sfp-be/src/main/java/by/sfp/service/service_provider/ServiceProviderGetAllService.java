package by.sfp.service.service_provider;

import by.sfp.domain.ServiceProvider;
import by.sfp.service.GetAllService;

public interface ServiceProviderGetAllService extends GetAllService<ServiceProvider> {
}
