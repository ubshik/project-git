package by.sfp.service.service_provider.impl;

import by.sfp.dao.service_provider.ServiceProviderSaveDao;
import by.sfp.domain.ServiceProvider;
import by.sfp.service.service_provider.ServiceProviderSaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ServiceProviderSaveServiceImpl implements ServiceProviderSaveService {
    @Autowired
    private ServiceProviderSaveDao serviceProviderSaveDao;

    @Override
    public void execute(ServiceProvider serviceProvider) {
        serviceProviderSaveDao.execute(serviceProvider);
    }
}
