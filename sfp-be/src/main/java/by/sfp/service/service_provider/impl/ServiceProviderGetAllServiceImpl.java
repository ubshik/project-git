package by.sfp.service.service_provider.impl;

import by.sfp.dao.service_provider.ServiceProviderGetAllDao;
import by.sfp.domain.ServiceProvider;
import by.sfp.service.service_provider.ServiceProviderGetAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ServiceProviderGetAllServiceImpl implements ServiceProviderGetAllService {
    @Autowired
    private ServiceProviderGetAllDao serviceProviderGetAllDao;

    @Override
    public List<ServiceProvider> execute() {
        return serviceProviderGetAllDao.execute();
    }
}
