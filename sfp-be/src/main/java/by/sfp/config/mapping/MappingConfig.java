package by.sfp.config.mapping;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"by.sfp.mapping"})
public class MappingConfig {
}
