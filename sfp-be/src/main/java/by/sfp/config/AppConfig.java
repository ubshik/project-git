package by.sfp.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "by.sfp.config", excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = "by.sfp.config.web.*"))
public class AppConfig {
}