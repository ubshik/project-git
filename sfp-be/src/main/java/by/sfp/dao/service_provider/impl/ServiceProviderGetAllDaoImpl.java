package by.sfp.dao.service_provider.impl;

import by.sfp.dao.service_provider.ServiceProviderGetAllDao;
import by.sfp.domain.ServiceProvider;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ServiceProviderGetAllDaoImpl implements ServiceProviderGetAllDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<ServiceProvider> execute() {
        return sessionFactory.getCurrentSession()
                .createQuery("select sp from ServiceProvider sp", ServiceProvider.class)
                .getResultList();
    }
}
