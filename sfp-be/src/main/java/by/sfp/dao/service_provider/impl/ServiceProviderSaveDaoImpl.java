package by.sfp.dao.service_provider.impl;

import by.sfp.dao.service_provider.ServiceProviderSaveDao;
import by.sfp.domain.ServiceProvider;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ServiceProviderSaveDaoImpl implements ServiceProviderSaveDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void execute(ServiceProvider serviceProvider) {
        sessionFactory.getCurrentSession().saveOrUpdate(serviceProvider);
    }
}
