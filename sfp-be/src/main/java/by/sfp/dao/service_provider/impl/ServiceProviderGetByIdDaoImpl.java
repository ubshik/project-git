package by.sfp.dao.service_provider.impl;

import by.sfp.dao.service_provider.ServiceProviderGetByIdDao;
import by.sfp.domain.ServiceProvider;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ServiceProviderGetByIdDaoImpl implements ServiceProviderGetByIdDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public ServiceProvider execute(Long id) {
        return sessionFactory.getCurrentSession()
                .createQuery("select sp from ServiceProvider sp where sp.id = :id", ServiceProvider.class)
                .setParameter("id", id)
                .stream().findFirst().orElse(null);
    }
}
