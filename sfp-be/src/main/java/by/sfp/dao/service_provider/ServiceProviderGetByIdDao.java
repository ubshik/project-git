package by.sfp.dao.service_provider;

import by.sfp.dao.GetByIdDao;
import by.sfp.domain.ServiceProvider;

public interface ServiceProviderGetByIdDao extends GetByIdDao<ServiceProvider> {
}
