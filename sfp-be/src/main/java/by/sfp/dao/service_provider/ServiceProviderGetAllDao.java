package by.sfp.dao.service_provider;

import by.sfp.dao.GetAllDao;
import by.sfp.domain.ServiceProvider;

public interface ServiceProviderGetAllDao extends GetAllDao<ServiceProvider> {
}
