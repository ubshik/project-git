package by.sfp.dao.service_provider;

import by.sfp.dao.SaveDao;
import by.sfp.domain.ServiceProvider;

public interface ServiceProviderSaveDao extends SaveDao<ServiceProvider> {
}
