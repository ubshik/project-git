package by.sfp.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="SERVICE_PROVIDER")
@Data
@ToString(includeFieldNames = true)
@NoArgsConstructor
public class ServiceProvider {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SERVICE_PROVIDER_ID_SEQ")
    @SequenceGenerator(name = "SERVICE_PROVIDER_ID_SEQ", sequenceName = "SERVICE_PROVIDER_ID_SEQ", allocationSize = 0)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;
    @Column(name = "CITY")
    private String city;
    @Column(name = "STREET")
    private String street;
    @Column(name = "BUILDING")
    private String building;
    @Column(name = "BLOCK")
    private String block;
    @Column(name = "MOBILE")
    private String mobile;
    @Column(name = "PHONE")
    private String phone;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "SITE")
    private String site;

    @ManyToMany(mappedBy = "serviceProviders", fetch = FetchType.EAGER)
    @JsonManagedReference
    private Set<ClassCategory> classCategories = new HashSet<>();

    public static ServiceProviderBuilder builder(){
        return new ServiceProviderBuilder();
    }

    public static class ServiceProviderBuilder {
        private ServiceProvider serviceProvider = new ServiceProvider();

        public ServiceProviderBuilder name(String name){
            serviceProvider.name = name;
            return this;
        }

        public ServiceProviderBuilder city(String city){
            serviceProvider.city = city;
            return this;
        }

        public ServiceProviderBuilder street(String street){
            serviceProvider.street = street;
            return this;
        }

        public ServiceProviderBuilder building(String building){
            serviceProvider.building = building;
            return this;
        }

        public ServiceProviderBuilder block(String block){
            serviceProvider.block = block;
            return this;
        }

        public ServiceProviderBuilder mobile(String mobile){
            serviceProvider.mobile = mobile;
            return this;
        }

        public ServiceProviderBuilder phone(String phone){
            serviceProvider.phone = phone;
            return this;
        }

        public ServiceProviderBuilder email(String email){
            serviceProvider.email = email;
            return this;
        }

        public ServiceProviderBuilder site(String site){
            serviceProvider.site = site;
            return this;
        }

        public ServiceProviderBuilder addClassCategorySet(Set<ClassCategory> classCategorySet){
            classCategorySet.forEach(classCategory -> classCategory.getServiceProviders().add(serviceProvider));
            serviceProvider.getClassCategories().addAll(classCategorySet);
            return this;
        }

        public ServiceProvider build(){
            return serviceProvider;
        }
    }
}
