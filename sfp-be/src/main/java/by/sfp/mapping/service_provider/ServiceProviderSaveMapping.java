package by.sfp.mapping.service_provider;

import by.sfp.domain.ServiceProvider;
import by.sfp.mapping.service_provider.mapper.ServiceProviderSaveMapper;
import by.sfp.mapping.Mapping;

public interface ServiceProviderSaveMapping extends Mapping<ServiceProvider, ServiceProviderSaveMapper> {
}
