package by.sfp.mapping.service_provider.impl;

import by.sfp.domain.ClassCategory;
import by.sfp.domain.ServiceProvider;
import by.sfp.service.class_category.ClassCategoryGetAllByIdsService;
import by.sfp.mapping.service_provider.mapper.ServiceProviderSaveMapper;
import by.sfp.mapping.service_provider.ServiceProviderSaveMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
@Transactional
public class ServiceProviderSaveMappingImpl implements ServiceProviderSaveMapping {
    @Autowired
    private ClassCategoryGetAllByIdsService classCategoryGetAllByIds;

    @Override
    public ServiceProvider toObject(ServiceProviderSaveMapper mapper) {
        return ServiceProvider.builder()
                .name(mapper.getName())
                .city(mapper.getCity())
                .street(mapper.getStreet())
                .building(mapper.getBuilding())
                .block(mapper.getBlock())
                .mobile(mapper.getMobile())
                .phone(mapper.getPhone())
                .email(mapper.getEmail())
                .site(mapper.getSite())
                .addClassCategorySet(getClassCategories(mapper.getClassCategories()))
                .build();
    }

    private Set<ClassCategory> getClassCategories(List<Long> ids) {
        return classCategoryGetAllByIds.execute(ids);
    }
}
