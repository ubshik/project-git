package by.sfp.mapping.service_provider.mapper;

import by.sfp.validation.class_category.ClassCategoriesIdsConstraint;
import by.sfp.validation.service_provider.ServiceProviderSaveConstraint;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ServiceProviderSaveConstraint
public class ServiceProviderSaveMapper implements Serializable {
    @NotEmpty(message = "\'Name\' must not be empty")
    @Size(max = 30, message = "\'Name\' must has length less or equal 30")
    private String name;

    @NotEmpty(message = "\'City\' must not be empty")
    @Size(max = 30, message = "\'City\' must has length less or equal 30")
    private String city;

    @Size(max = 30, message = "\'Street\' must has length less or equal 30")
    private String street;

    @Size(max = 4, message = "\'Building\' must has length less or equal 4")
    private String building;

    @Size(max = 3, message = "\'Block\' must has length less or equal 3")
    private String block;

    @Pattern(regexp = "\\+[0-9]{12}|^\\s*$", message = "\'Mobile\' must be a well-formed")
    private String mobile;

    @Pattern(regexp = "\\+[0-9]{12}|^\\s*$", message = "\'Phone\' must be a well-formed")
    private String phone;

    @Email(message = "\'Email\' must be a well-formed email address")
    @Size(max = 30, message = "\'Email\' mush has length less or equal 30")
    private String email;

    @Pattern(regexp = "((http(s)?://.)?(www\\.)?[-a-zA-Z0-9@:%._+~#=]{2,27}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_+.~#?&//=]*)|^\\s*$)",
            message = "\'Site\' must be a well-formed URL")
    @Size(max = 30, message = "\'Site\' mush has length less or equal 30")
    private String site;

    @ClassCategoriesIdsConstraint
    private List<Long> classCategories = new ArrayList<>();

    public static ServiceProviderSaveMapperBuilder builder(){
        return new ServiceProviderSaveMapperBuilder();
    }

    public static class ServiceProviderSaveMapperBuilder {
        private ServiceProviderSaveMapper mapper = new ServiceProviderSaveMapper();

        public ServiceProviderSaveMapperBuilder name(String name) {
            mapper.name = name;
            return this;
        }

        public ServiceProviderSaveMapperBuilder city(String city) {
            mapper.city = city;
            return this;
        }

        public ServiceProviderSaveMapperBuilder street(String street) {
            mapper.street = street;
            return this;
        }

        public ServiceProviderSaveMapperBuilder building(String building) {
            mapper.building = building;
            return this;
        }

        public ServiceProviderSaveMapperBuilder block(String block) {
            mapper.block = block;
            return this;
        }

        public ServiceProviderSaveMapperBuilder mobile(String mobile) {
            mapper.mobile = mobile;
            return this;
        }

        public ServiceProviderSaveMapperBuilder phone(String phone) {
            mapper.phone = phone;
            return this;
        }

        public ServiceProviderSaveMapperBuilder email(String email) {
            mapper.email = email;
            return this;
        }

        public ServiceProviderSaveMapperBuilder site(String site) {
            mapper.site = site;
            return this;
        }

        public ServiceProviderSaveMapperBuilder classCategories(List<Long> classCategories) {
            mapper.classCategories.addAll(classCategories);
            return this;
        }

        public ServiceProviderSaveMapper build(){
            return mapper;
        }
    }
}
