package by.sfp.validation.class_category;

import by.sfp.service.class_category.ClassCategoryGetAllByIdsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.Objects;

@Component
public class ClassCategoriesIdsValidator implements ConstraintValidator<ClassCategoriesIdsConstraint, List<Long>> {
    public static final int MAX_NUMBER_OF_CLASS_CATEGORIES = 15;

    @Autowired
    private ClassCategoryGetAllByIdsService classCategoryGetAllByIds;

    @Override
    public boolean isValid(List<Long> categoriesIds, ConstraintValidatorContext context) {
        return checkNotNullReference(categoriesIds) &&
                checkNotNullElements(categoriesIds) &&
                checkNumberInputClassCategories(categoriesIds) &&
                checkNumberRetrievedClassCategories(categoriesIds);
    }

    private boolean checkNotNullReference(List<Long> ids) {
        return ids != null;
    }

    private boolean checkNotNullElements(List<Long> ids) {
        return ids.stream().allMatch(Objects::nonNull);
    }

    private boolean checkNumberInputClassCategories(List<Long> ids) {
        return ids.size() != 0 && ids.size() <= MAX_NUMBER_OF_CLASS_CATEGORIES;
    }

    private boolean checkNumberRetrievedClassCategories(List<Long> ids) {
        int executesIdsSize = classCategoryGetAllByIds.execute(ids).size();
        return executesIdsSize == ids.size();

    }
}
