package by.sfp.validation.class_category;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = ClassCategoriesIdsValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ClassCategoriesIdsConstraint {
    String message() default "Class categories aren't valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
