package by.sfp.validation.service_provider;

import by.sfp.mapping.service_provider.mapper.ServiceProviderSaveMapper;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class ServiceProviderSaveValidator implements ConstraintValidator<ServiceProviderSaveConstraint, ServiceProviderSaveMapper> {
    @Override
    public boolean isValid(ServiceProviderSaveMapper mapper, ConstraintValidatorContext context) {
        return checkNotNullReference(mapper) && checkNotEmptyContactInfo(mapper);
    }

    private boolean checkNotEmptyContactInfo(ServiceProviderSaveMapper mapper) {
        return checkNotEmptyAddress(mapper.getStreet(), mapper.getBuilding()) ||
                checkNotEmptyMobile(mapper.getMobile()) || checkNotEmptyPhone(mapper.getPhone()) ||
                checkNotEmptyEmail(mapper.getEmail()) || checkNotEmptySite(mapper.getSite());
    }

    private boolean checkNotEmptyAddress(String street, String building) {
        return checkNotEmptyStreet(street) && checkNotEmptyBuilding(building);
    }

    private boolean checkNotEmptyStreet(String street) {
        return street != null && !street.isEmpty();
    }

    private boolean checkNotEmptyBuilding(String building) {
        return building != null && !building.isEmpty();
    }

    private boolean checkNotEmptyMobile(String mobile) {
        return mobile != null && !mobile.isEmpty();
    }

    private boolean checkNotEmptyPhone(String phone) {
        return phone != null && !phone.isEmpty();
    }

    private boolean checkNotEmptyEmail(String email) {
        return email != null && !email.isEmpty();
    }

    private boolean checkNotEmptySite(String site) {
        return site != null && !site.isEmpty();
    }

    private boolean checkNotNullReference(ServiceProviderSaveMapper mapper) {
        return mapper != null;
    }

}
