package by.sfp.validation.service_provider;

import by.sfp.service.service_provider.ServiceProviderGetByIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class ServiceProviderIdExistenceValidator implements ConstraintValidator<ServiceProviderIdExistenceConstraint, Long> {
    @Autowired
    private ServiceProviderGetByIdService serviceProviderGetById;

    @Override
    public boolean isValid(Long id, ConstraintValidatorContext context) {
        return serviceProviderGetById.execute(id) != null;
    }
}
