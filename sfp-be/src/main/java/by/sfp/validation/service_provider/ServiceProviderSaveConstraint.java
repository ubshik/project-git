package by.sfp.validation.service_provider;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = ServiceProviderSaveValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ServiceProviderSaveConstraint {
    String message() default "Fields values aren't match";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
