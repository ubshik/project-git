package by.sfp.controller.service_provider;

import by.sfp.domain.ServiceProvider;
import by.sfp.service.service_provider.ServiceProviderGetByIdService;
import by.sfp.service.service_provider.ServiceProviderSaveService;
import by.sfp.mapping.service_provider.mapper.ServiceProviderSaveMapper;
import by.sfp.mapping.service_provider.ServiceProviderSaveMapping;
import by.sfp.validation.service_provider.ServiceProviderIdExistenceConstraint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/service_providers", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Validated
public class ServiceProviderController {
    @Autowired
    private ServiceProviderSaveMapping serviceProviderSaveMapping;

    @Autowired
    private ServiceProviderSaveService serviceProviderSave;

    @Autowired
    private ServiceProviderGetByIdService serviceProviderGetById;

    @PostMapping
    public ResponseEntity saveServiceProvider(@Valid @RequestBody ServiceProviderSaveMapper serviceProviderMapper) {
        ServiceProvider serviceProvider = serviceProviderSaveMapping.toObject(serviceProviderMapper);
        serviceProviderSave.execute(serviceProvider);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}", consumes = MediaType.ALL_VALUE)
    public ResponseEntity getServiceProviderById(@ServiceProviderIdExistenceConstraint @PathVariable("id") Long serviceProviderId) {
       ServiceProvider serviceProvider = serviceProviderGetById.execute(serviceProviderId);
       return new ResponseEntity<>(serviceProvider, HttpStatus.OK);
    }
}
